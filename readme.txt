To compile, please run g++ *.cpp and call the generated a.out file.

Background:

Process industries are adopting wireless sensor-actuator networks
(WSANs) as the communication infrastructure.
The dynamics of industrial environments and stringent reliability
requirements necessitate high degrees of fault tolerance in routing.
WirelessHART is an open industrial standard for WSANs that have seen
world-wide deployments. WirelessHART employs graph routing schemes
to achieve network reliability through multiple paths. Since many
industrial devices operate on batteries in harsh environments where
changing batteries are prohibitively labor-intensive, WSANs need to
achieve long network lifetime. To meet industrial demand for
long-term reliable communication, this project studies the problem of
maximizing network lifetime for WSANs under graph routing.

This project formulates the network lifetime maximization problem for
WirelessHART networks under graph routing. Then, it propose a series
of efficient routing algorithms to prolong the network lifetime of
WSANs. Experiments in a physical testbed and simulations show our
routing algorithms can improve the network lifetime by up to 50%
while preserving the reliability benefits of graph routing.

Project summary:

The code in this repository is used to generate 
routes based on collected link qualities from the testbed. 

The problem is about wireless network design.
There are 63 nodes in the network. Each of them has a battery capacity.
Also, there are multiple flows, each with a source, a destination, and a period.
We build a route for each flow.
Each route has a primary path from the source to the
destination. And for each node on the primary path, there is a backup
path from the node to the destination. The problem is how to build the
routes for flows such that every flow has a valid route.
We also generate schedules for each flow.

We use different classes to present nodes, flows, and schedule entries
in the network.

Related infomation:

The network link qualities are stored in 
/topology/profile_april21.txt.

Flows are printed in /Outputs/LogFlow.txt.

Routes of flows are stored in /Outputs/LogFlowRouting.txt

This repository is part of the real-time wireless control network project:
http://cps.cse.wustl.edu/index.php/Real-Time_Wireless_Control_Networks.

My personal website: 
http://www.cse.wustl.edu/~wuchengjie/.

My LinkedIn profile:
https://www.linkedin.com/pub/chengjie-wu/28/30b/836.