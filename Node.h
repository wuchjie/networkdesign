//
//  Node.h
//  ReliableScheduler
//
//  Created by Chengjie Wu on 1/6/14.
//
//

#ifndef ReliableScheduler_Node_h
#define ReliableScheduler_Node_h

#include <iostream>
#include "ScheduleEntry.h"
using namespace std;


class Node
{
	int iID;					    //Node id
    float iAggRate;
    float iLoad;
    float iBattery;
    int iResidualBattery;
public:
	Node();
	Node(int ID)
	{
        iID= ID;
        iLoad=0;
        iAggRate=0;
        iBattery=0;
    };
    int rate;
    //int battery;
    //float load;
    //int PowerRate[5];
    map<ROUTETYPE, int> PowerRate;
    map<int, float> mConnectedNeighbors;
    map<int, vector<int> > mvHanDownLinkGraph;
    map<int, vector<int> > SourceRoute;
    map<int, map<int, vector<int> > > mpDijkstraUplinkRoute;           //[routeid][hop count][sender, receiver]
    map<int, map<int, vector<int> > > mpDijkstraDownlinkRoute;           //[routeid][hop count][sender, receiver]
    map<int, map<int, vector<int> > > mpUplinkDisjointRoutes;
    map<int, map<int, vector<int> > > mpDownlinkDisjointRoutes;
    map<int, vector<int> > mpDisjontPaths;
    map<int, map<int, vector<int> > > mmFaultTolerantUplinkRoute;
    map<int, map<int, vector<int> > > mmFaultTolerantDownlinkRoute;
    map<int, map<int, map<int, float> > > mmPowerUplinkRoute;
    map<int, map<int, map<int, float> > > mmPowerDownlinkRoute;
    map<int, vector<int> > mvGLPKRoute;
    map<int, map<int, vector<int> > > mpGLPKUplinkRoute;
    map<int, map<int, vector<int> > > mpGLPKDownlinkRoute;
    
    map<int, map<int, vector<int> > > mpHanUplinkRoute;
    map<int, map<int, vector<int> > > mpHanDownlinkRoute;
    vector<vector<int> > HanDownlinkGraph;
    
	int GetNodeID(){return iID;};
    float GetAggRate(){return iAggRate;}
    void SetAggRate(float rate){iAggRate=rate;}
    float GetLoad(){return iLoad;}
    void SetLoad(float cload){iLoad=cload;}
    float GetBattery(){return iBattery;}
    void SetBattery(float b){iBattery=b;}
    int GetResidualBattery(){return iResidualBattery;}
    void SetResidualBattery(int re){iResidualBattery=re;}
    void ReduceResidualBattery(int reduce){iResidualBattery-=reduce;}
	map<int, map<int, vector<int> > > getGraphRoute(ROUTETYPE route, DIRECTIONTYPE dirt){
        if (route==DIJKSTRA){
            if (dirt==UP) {return mpDijkstraUplinkRoute;}
            else if (dirt==DOWN) {return mpDijkstraDownlinkRoute;}
            else {exit(11);}
        }
        else if (route==GLPK){
            if (dirt==UP) {return mpGLPKUplinkRoute;}
            else if (dirt==DOWN) {return mpGLPKDownlinkRoute;}
            else {exit(11);}
        }
        else {
            exit(11);
        }
    }
	   
	~Node();
};

#endif
