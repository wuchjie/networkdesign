#include <iostream>
#include <fstream>
#include "conio.h"
#include "ReliableScheduler.h"

using namespace std;
int main (int argc, char * const argv[]) {
    try
	{
        ofstream LogLifetime("Outputs/LogLifeTimeEXP.txt", ios::app);
        ofstream LogTime("Outputs/LogTime.txt", ios::app);
        LogLifetime<<"\n\n== new run n==\n\n";
        LogTime<<"\n\n== new run n==\n\n";
        LogLifetime.close();
        LogTime.close();
        for (int numflows=10; numflows<=10; numflows+=2) {
            ReliableScheduler *g=new ReliableScheduler(numflows, 1);
            cout<<"\n ----------------------- End of the simulation ---------------------";
            cout<<"\n ----------------------- Record performance parameters ---------------------";
            delete g;
        }
	}
	catch (int n)
	{
		
	};

	cout<<"\n ----------------------- Press any key to leave the program ---------------------";
	return 0;     
}