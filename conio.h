/*
 *  conio.h
 *  
 *
 */


#ifndef _CONIO_H_
#define _CONIO_H_


#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
	

	
    typedef enum
    {
        DEDICATED,
        SHARED,
        PRIMARY,
        SECONDRY,
        EACK
    } ACCESSTYPE;
    
    typedef enum
    {
        Oldtrace,
        IWQoSpaper,
        MoSha
    } ApplicationTYPE;
    
    typedef enum
    {
        SENSING,
        CONTROL,
        ACKNOW
    } PHASETYPE;
    
    typedef enum
    {
        BACKUP,   //All flows use graph routing; Links on backup paths are flat sharing
        POLLING,  //All flows use graph routing; Every link is dedicated
        SOURCE,   //All flows use source routing; Every link is dedicated
        DIVIDE,   //All flows use graph routing; Every link is dedicated (links on primary path are schedule only once)
        MCDEDI,   //Low critical flows use source routing, High critical flows use graph routing; Every link is dedicated
        MCFLAT,   //Low critical flows use source routing, High critical flows use graph routing; Links on backup paths are flat sharing
        MCPRIO,   //Low critical flows use source routing, High critical flows use graph routing; Links on backup paths of high critical flows prioritized share slots with the primary path of regular flow
        SHARBACKUP,  //Polling serve with shared backup paths and stealing dedicated path (Prioritized sharing on primary path and flat sharing on backup path)
        SHARING,  //Polling server with slot sharing (Prioritized sharing on all paths)
        ACKALL,    //Polling server with slot sharing with scheduled ACK
        ACKREV,  //Polling server with slot sharing with immediate one time ACK on the reverse route
        SHARACKALL,
        SHARACKREV,
        SITTING,   //Emergency control loop sitting on regular control loop
        SITACK
    } APPROACH;
    
    typedef enum
    {
        IDLE,
        ACTIVE,
        DELIVERED,
        LOST,
        ALARM,
        ALARMACK,
        CLEAR,
        CLEARACK
    } STATUSTYPE;
	
    typedef enum
    {
        HIGH,
        LOW
    } CRITTYPE;
    
    typedef enum
    {
        UP,
        DOWN
    } DIRECTIONTYPE;
    
    typedef enum
    {
        HAN,
        DIJKSTRA,
        GLPK,
        MINLOAD,
        MIPK
    } ROUTETYPE;
    
#define cgets	_cgets
#define cprintf	_cprintf
#define cputs	_cputs
#define cscanf	_cscanf
#define ScreenClear clrscr
	
	/* blinkvideo */
	
	void clreol (void);
	void clrscr (void);
	
	int _conio_gettext (int left, int top, int right, int bottom,
						char *str);
	/* _conio_kbhit */
	
	void delline (void);
	
	/* gettextinfo */
	void gotoxy(int x, int y);
	/*
	 highvideo
	 insline
	 intensevideo
	 lowvideo
	 movetext
	 normvideo
	 */
	
	void gotoxy(int x, int y);
	
	void puttext (int left, int top, int right, int bottom, char *str);
	
	// Screen Variables
	
	/* ScreenCols
	 ScreenGetChar
	 ScreenGetCursor
	 ScreenMode
	 ScreenPutChar
	 ScreenPutString
	 ScreenRetrieve
	 ScreenRows
	 ScreenSetCursor
	 ScreenUpdate
	 ScreenUpdateLine
	 ScreenVisualBell
	 _set_screen_lines */
	
	void _setcursortype (int type);
	
	void textattr (int _attr);
	
	void textbackground (int color);
	
	void textcolor (int color);
	
	/* textmode */
	
	int wherex (void);
	
	int wherey (void);
	
	/* window */
	
	
	
	/*  The code below was part of Mingw's conio.h  */
	/*
	 * conio.h
	 *
	 * Low level console I/O functions. Pretty please try to use the ANSI
	 * standard ones if you are writing new code.
	 *
	 * This file is part of the Mingw32 package.
	 *
	 * Contributors:
	 *  Created by Colin Peters <colin@bird.fu.is.saga-u.ac.jp>
	 *
	 *  THIS SOFTWARE IS NOT COPYRIGHTED
	 *
	 *  This source code is offered for use in the public domain. You may
	 *  use, modify or distribute it freely.
	 *
	 *  This code is distributed in the hope that it will be useful but
	 *  WITHOUT ANY WARRANTY. ALL WARRANTIES, EXPRESS OR IMPLIED ARE HEREBY
	 *  DISCLAMED. This includes but is not limited to warranties of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	 *
	 * $Revision: 1.4 $
	 * $Author: hongli $
	 * $Date: 2002/04/26 19:31:25 $
	 *
	 */
	
	char*	_cgets (char*);
	int	_cprintf (const char*, ...);
	int	_cputs (const char*);
	int	_cscanf (char*, ...);
	
	int	_getch (void);
	int	_getche (void);
	int	_kbhit (void);
	int	_putch (int);
	int	_ungetch (int);
	
	
	int	getch (void);
	int	getche (void);
	int	kbhit (void);
	int	putch (int);
	int	ungetch (int);
	
	
#ifdef __cplusplus
}
#endif

#endif /* _CONIO_H_ */
