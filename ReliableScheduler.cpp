/*
 *  ReliableScheduler.cpp
 *  ReliableScheduler
 *
 *  
 *
 */
#include "ReliableScheduler.h"

#include <iostream>
using namespace std;

//fix the han routing approach
ReliableScheduler:: ReliableScheduler(int numflows, int numruns)
{
//    map<int, int> NetworkOutput;
    //RandomSimulator();
    
    whethersingleperiod=false;
    srand (time(NULL));
    
	/****************parameters***************/
    iGateway=0;
	iRuns=1;				//total runs
	exp1=5; exp2=9;		//harmonic period ranges. 5--12 is the final range
	iTotalFlows=40;			//maximum total flows
    inumflows=numflows;
    n=inumflows;
    BigBattery=1000000000;
    SABattery=500;
	/************************************************/
	deadlinedivisor=1;
    //m= 3;					// total channels
    SIMStart=1;
    SIMEnd=40000;
    EMStart=200;
    EMEnd=39000;
    LOSTDELAY=50;
    PowerTrans=2;
    PowerReceive=2;
    PowerWait=1;
    inumtrace=100;
    inumlinks=0;
    
    numAccessPoints=2;
    
    emergencyperiod=1024;
    regularperiod=2048;
    
    
    setIOFiles();
    
    vAPPROACH.clear();
    APPtype.clear();
    vRoutes.push_back(DIJKSTRA);
    vRoutes.push_back(HAN);

    
    RouteName[DIJKSTRA]="DIJ";
    RouteName[HAN]="HAN";
    RouteName[GLPK]="LPR";
    RouteName[MINLOAD]="GHS";
    RouteName[MIPK]="IPM";
    
    vDirects.push_back(UP);
    //vDirects.push_back(DOWN);
    
    DirectionName[UP]="UP";
    DirectionName[DOWN]="DOWN";
    
    vAPPROACH.push_back(BACKUP);

    APPtype[BACKUP]="BACKUP";

    
    
    //iaccesstype=DEDICATED;
	
    
    Etype.push_back('R');
    Etype.push_back('E');
    
    Atype[DEDICATED]='D';
    Atype[SHARED]='S';
    Atype[PRIMARY]='P';
    Atype[SECONDRY]='N';
    Atype[EACK]='E';

    
    Ptype.push_back('S');
    Ptype.push_back('C');
    //Ptype.push_back('A');
    
    Btype.push_back('N');
    Btype.push_back('Y');
    
    vCritType.push_back('H');
    vCritType.push_back('L');
    
    
    
	
	numflows_for_display=20;
    n=10;
    
    PRRBound=0.9;
    bool TestCase=false;
    LogLifetimeEXP<<"\n"<<inumflows<<" ";
    LogLifetimeRatio<<"\n"<<inumflows<<" ";
    LogLifetimeSim<<"\n"<<inumflows<<" ";
    LogLifetimeSimRatio<<"\n"<<inumflows<<" ";
    LogTime<<"\n"<<inumflows<<" ";
    for (int runindx=0; runindx<numruns; runindx++) {
        LogTime.flush();
        LogLifetimeSim.flush();
        LogLifetimeRatio.flush();
        LogLifetimeSimRatio.flush();
        LogLifetimeEXP.flush();
        cout<<runindx<<"\n\n";
        
        CollectTopology(TestCase);
        SelectChannel(true, TestCase);
        SelectLinks();
        //GetAccessPoints_Paths(4, true, TestCase);
        //PrintNodes();
        cout<<"\n\n=================== "<<inumflows<<" ==========================\n\n";
        
        mpFlows.clear();
        GenerateFlows(inumflows, whethersingleperiod, TestCase);
        // if testcase, generate 1 flow, and use the test topology in topology folder
        
        SetNodeBattery(TestCase);
        //CollectHanRoutingTime();
        PrintTestbedGraphTrace();
        CreateAllGraphRouting();
        PrintFlowGraphRouting();
        VerifyAllGraphRouting();
        
        //PrintGraphRouting();
        //cout<<"PrintGraphRouteing Done";
        
        //cout<<"CalculatePowerRate done";
        bool simulate=false;
        if (simulate) {
            f.clear();
            AssignRMPriority(f);
            
            for (int i=0; i<vRoutes.size(); i++) {
                iapproach=POLLING;
                ROUTETYPE iroute=vRoutes[i];
                bool wpass=GenerateSchedule(true, iapproach, iroute);
                if (wpass==true) {
                    cout<<"Schedulable under "<<vRoutes[iroute]<< "\n";
                }
                else {
                    cout<<"Not Schedulable under "<<vRoutes[iroute]<< "\n";
                    exit(22);
                }
                VerifySchedule(iapproach);
                PrintSchedule(iapproach, iroute);
                //PrintWCPSSchedule(iapproach);
                PrintFlowSchedule(iapproach, iroute);
                CreateFlowRouteSchedule(iroute);
                
            }
            PrintGraphRouteSchedule();
            CalculatePowerRate();
            cout<<"Num of links "<<inumlinks<<" \n";
        }
        
    }
    closeIOFiles();
    cout<<"\n\nRun finished\n\n";
    //exit(1);
}

void ReliableScheduler:: setIOFiles()
{
    TraceReader.open("Results/OutputFinal.txt", ios::in);
    HanTimeReader.open("GraphRoutes/Han_time.txt", ios::in);
    
    Log.open("Outputs/LogFlow.txt", ios::out);	//customize the working directory from executables
    FlowLog.open("Outputs/FlowLog.txt", ios::out);	//customize the working directory from executables
	
    LogFlowSchedule.open("Outputs/LogFlowSchedule.txt", ios::out);	//customize the working directory from executables
    
    LogTopology.open("Outputs/LogTopology.txt", ios::out);
    LogLinks.open("Outputs/LogLinks", ios::out);
    //LogChannelDiversity.open("Outputs/LogChannelDiversity.txt", ios::out);
    LogSourceRouting.open("Outputs/LogRouting.txt", ios::out);
    
    LogFlowRouting.open("Outputs/LogFlowRouting.txt", ios::out);
    
    LogLinkQuality.open("Outputs/LogLinkQuality.txt", ios::out);
    
    LogPowerRate.open("Outputs/LogPowerRate.txt", ios::out);
    TestbedTraceReader.open("GraphRoutes/testbed_traces.txt", ios::in);
    
    LogTime.open("Outputs/LogTime.txt", ios::app);
    LogLifetimeEXP.open("Outputs/LogLifetimeEXP.txt", ios::app);
    LogLifetimeRatio.open("Outputs/LogLifetimeRatio.txt", ios::app);
    LogLifetimeSim.open("Outputs/LogLifetimeSim.txt", ios::app);
    LogLifetimeSimRatio.open("Outputs/LogLifetimeSimRatio.txt", ios::app);
    LogHanGraph.open("Outputs/LogHanGraph.txt", ios::out);
}

void ReliableScheduler:: closeIOFiles()
{
    TraceReader.close(); TestbedTraceReader.close();
	LogAllRuns.close(); Log.close(); Schedule.close();
    LogSchedule.close();LogFlowSchedule.close(); LogWCPSSchedule.close(); LogWCPSFlowSchedule.close();
    LogTestbedSchedule.close(); LogTestbedFlowSchedule.close();
    LogDebug.close(); FlowLog.close(); LogACC.close();LogWCDelay.close();LogTransmissions.close();
    LogDelay.close(); LogFirstDelay.close();LogTopology.close(); LogChannelDiversity.close(); LogLinks.close();
    LogSourceRouting.close(); LogFlowRouting.close();
    LogEdgeDisjointPaths.close();
    LogShortSchedule.close(); LogDeliveryRatio.close();LogLinkQuality.close(); LogTestDelayVector.close();
    LogFaultTolerantRoute.close();LogPowerRate.close();
    LogGraphSchedule.close(); LogTestbedGraphTrace.close(); LogTransmissionTraces.close();
    LogEnergySimulation.close();
    LogEnergy.close();
    LogTime.close(); HanTimeReader.close();
    LogLifetimeEXP.close(); LogHanGraph.close();LogLifetimeRatio.close();LogLifetimeSim.close(); LogLifetimeSimRatio.close();
}



void ReliableScheduler::PrintTransmissionTraces()
{
    
}


void ReliableScheduler::CollectHanRoutingTime()
{
    mpHanTime.clear();

    if (HanTimeReader.is_open())
    {
        cout<<"Han Time reader is working \n";
        while(!HanTimeReader.eof())
        {
            int node, runtime;
            HanTimeReader>>node>>runtime;
            mpHanTime[node]=runtime;
        }
        cout<<"\n Han Time reader  Done.";
        
        HanTimeReader.close();
    }
}

void ReliableScheduler::PrintTestbedGraphTrace()
{
    map<int, map<int, map<int, vector<int> > > >::iterator itmmmTrace;
    map<int, map<int, vector<int> > >::iterator itmmTrace;
    map<int, vector<int> > ::iterator itmTrace;
    for (itmmmTrace=mmPRRTrace.begin(); itmmmTrace!=mmPRRTrace.end(); itmmmTrace++) {
        for (itmmTrace=itmmmTrace->second.begin(); itmmTrace!=itmmmTrace->second.end(); itmmTrace++) {
            for (itmTrace=itmmTrace->second.begin(); itmTrace!=itmmTrace->second.end(); itmTrace++) {
                LogTestbedGraphTrace<<"s "<<itmmmTrace->first<<" r "<<itmmTrace->first<<" c "<<itmTrace->first<<" size "<<itmTrace->second.size()<<" i "<<mmPRRIndex[itmmmTrace->first][itmmTrace->first][itmTrace->first]<<" real "<<mmPRRReal[itmmmTrace->first][itmmTrace->first][itmTrace->first]<<"\n";
                for (int index=0; index<itmTrace->second.size(); index++) {
                    LogTestbedGraphTrace<<itmTrace->second.at(index)<<" ";
                }
                LogTestbedGraphTrace<<"\n";
            }
        }
    }
}




void ReliableScheduler::CreateAllGraphRouting()
{
    struct timeval startTime;
    struct timeval endTime;
    struct rusage ru;
    float tS, tE, fTime;
    //LogTime<<inumflows<<" ";
    for (int i=0; i<vRoutes.size(); i++) {
        cout<<"\n\n Generating "<<" "<<PrintRouteType(vRoutes[i])<<" Graph Routing\n";
        if (vRoutes[i]==HAN) {
            getrusage(RUSAGE_SELF, &ru);
            startTime = ru.ru_utime;
            //int hantime=0;
            //hantime+=mpHanTime[iGateway];
            for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
                vector<vector<int> > HanGraph;
                bool findhangraph=GenerateHanGraphRoute(itmpflow->second->GetSource(), itmpflow->second->GetDestination(), HanGraph);
                if(!findhangraph) LogHanGraph<<"\n\n Didn't find graph route\n\n";
                LogHanGraph<<"\n\nHanGraph from "<<itmpflow->second->GetSource()<<" to "<<itmpflow->second->GetDestination()<<":\n";
                for (int i=0; i<HanGraph.size(); i++) {
                    for (int j=0; j<HanGraph[i].size(); j++) {
                        if (j==0) {
                            LogHanGraph<<HanGraph[i][j]<<": ";
                        }
                        else{
                            LogHanGraph<<HanGraph[i][j]<<" ";
                        }
                    }
                    LogHanGraph<<"\n";
                }
                LogHanGraph.flush();
                map<int, map<int, vector<int> > > Groute;
                FindHanGraphRoute(itmpflow->second->GetSource(), itmpflow->second->GetDestination(), HanGraph, Groute);
                itmpflow->second->mpGraphRoute[HAN][UP]=Groute;
                LogHanGraph.flush();
            }
            getrusage(RUSAGE_SELF, &ru);
            endTime = ru.ru_utime;
            // calculate time in microseconds
            tS = startTime.tv_sec*1000000 + (startTime.tv_usec);
            tE = endTime.tv_sec*1000000  + (endTime.tv_usec);
            fTime=tE-tS;
            LogTime<<fTime<<" ";
        }
        else if (vRoutes[i]==DIJKSTRA){
            getrusage(RUSAGE_SELF, &ru);
            startTime = ru.ru_utime;
            CreateFlowDijkstraGraphRouting();
            getrusage(RUSAGE_SELF, &ru);
            endTime = ru.ru_utime;
            // calculate time in microseconds
            tS = startTime.tv_sec*1000000 + (startTime.tv_usec);
            tE = endTime.tv_sec*1000000  + (endTime.tv_usec);
            fTime=tE-tS;
            LogTime<<fTime<<" ";
        }
        else {
            cout<<"!"<<PrintRouteType(vRoutes[i])<<"!";
            cout<<"Wrong Route option\n\n";
            exit(13);
        }
    }
    //LogTime<<"\n";
    LogTime.flush();
}

void ReliableScheduler::VerifyAllGraphRouting()
{
    for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
        //map<int, map<int, vector<int> > >
        for (int i=0; i<vRoutes.size(); i++) {
            for (int j=0; j<vDirects.size(); j++) {
                map<int, map<int, vector<int> > > mpRoute=itmpflow->second->mpGraphRoute[vRoutes[i]][vDirects[j]];
                map<int, map<int, vector<int> > >::iterator itroute;
                map<int, vector<int> >::iterator ithop;
                for (itroute=mpRoute.begin(); itroute!=mpRoute.end(); itroute++) {
                    int lasthop=-1;
                    for (ithop=itroute->second.begin(); ithop!=itroute->second.end(); ithop++) {
                        if (itroute->first==0) {
                            if (mpRoute.count(ithop->first)==1) {
                                if (mpRoute[ithop->first].begin()->second[0]!=ithop->second[0]) {
                                    if (ithop->first!=1 || vDirects[j]!=DOWN) {
                                        cout<<"Wrong with route";
                                        cout<<"Flow "<<itmpflow->first<<" Route type "<<vRoutes[i]<<" phase "<<vDirects[j]<<" Routeid "<<itroute->first<<" Hop count "<<ithop->first<<" \n";
                                        //exit(13);
                                    }
                                    
                                }
                            }
                        }
                        if (ithop->first!=1) {
                            if (ithop->second[0]!=lasthop) {
                                cout<<"Wrong last hop!";
                                cout<<"Flow "<<itmpflow->first<<" Route type "<<RouteName[vRoutes[i]]<<" phase "<<vDirects[j]<<" Routeid "<<itroute->first<<" Hop count "<<ithop->first<<" \n";
                                //exit(14);
                            }
                        }
                        lasthop=ithop->second[1];
                    }
                }
            }
        }
    }
}

void ReliableScheduler::PrintGraphRouteSchedule()
{
    map<int, Flow*>::iterator itflow;
    map<ROUTETYPE, map<DIRECTIONTYPE, map<int, map<int, vector<ScheduleEntry> > > > >::iterator itmmmmv;
    map<DIRECTIONTYPE, map<int, map<int, vector<ScheduleEntry> > > >::iterator itmmmv;
    map<int, map<int, vector<ScheduleEntry> > >::iterator itmmv;
    map<int, vector<ScheduleEntry> >::iterator itmv;
    
    for (itflow=mpFlows.begin(); itflow!=mpFlows.end(); itflow++) {
        LogGraphSchedule<<"\n\nFlow "<<itflow->first<<"\n";
        for (itmmmmv=itflow->second->mpRouteSchedule.begin(); itmmmmv!=itflow->second->mpRouteSchedule.end(); itmmmmv++) {
            LogGraphSchedule<<"\n Route Approach "<<itmmmmv->first;
            for (itmmmv=itmmmmv->second.begin(); itmmmv!=itmmmmv->second.end(); itmmmv++) {
                LogGraphSchedule<<"\n  Direction "<<itmmmv->first;
                for (itmmv=itmmmv->second.begin(); itmmv!=itmmmv->second.end(); itmmv++) {
                    LogGraphSchedule<<"\nRoute "<<itmmv->first<<": ";
                    for (itmv=itmmv->second.begin(); itmv!=itmmv->second.end(); itmv++) {
                        //LogGraphSchedule<<"\nHop "<<itmv->first<<": ";
                        for (int i=0; i<itmv->second.size(); i++) {
                            LogGraphSchedule<<"("<<itmv->first<<" "<<itmv->second[i].iSender<<"->"<<itmv->second[i].iReceiver<<" "<<itmv->second[i].iTimeSlot<<" "<<itmv->second[i].iChannel<<")  \n";
//                            int sender=itmv->second[i].iSender;
//                            int receiver=itmv->second[i].iReceiver;
//                            int channel=itmv->second[i].iChannel;
//                            int numtry=inumtrace;
//                            while (numtry>0) {
//                                float prr=mmPRR[sender][receiver][channel];
//                                
//                                int PRRT=int(prr*100);
//                                int result=0;
//                                int chance=rand() % 101;
//                                if (chance<PRRT) {
//                                    result=1;
//                                }
//                                //cout<<prr<<" "<<PRRT<<" "<<chance<<" "<<result<<"\n";
//                                itflow->second->mpRouteTrace[itmmmmv->first][itmmmv->first][itmmv->first][itmv->first][i].push_back(result);
//                                numtry--;
//                                LogGraphSchedule<<result<<" ";
//                            }
                            LogGraphSchedule<<"\n";
                        }
                    }
                    LogGraphSchedule<<"\n";
                }
            }
        }
    }
}

void ReliableScheduler:: CalculatePowerRate()
{
    LogPowerRate<<"Node Battery:\n";
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        LogPowerRate<<itnodes->first<<" "<<itnodes->second->GetBattery()<<"\n";
    }
    LogPowerRate<<"\n";
    for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
        LogPowerRate<<"\n\nFlow "<<itmpflow->first<<":\n";
        for (int i=0; i<vRoutes.size(); i++) {
            for (int j=0; j<vDirects.size(); j++) {
                map<int, map<int, vector<int> > > GraphRoute;
                map<int, map<int, vector<int> > >::iterator itgroute;
                map<int, vector<int> >::iterator itroute;
                ROUTETYPE route=vRoutes[i];
                DIRECTIONTYPE dirt=vDirects[j];
                GraphRoute=itmpflow->second->mpGraphRoute[route][dirt];
                LogPowerRate<<"\n\nRoute route "<<route<<" direction "<<dirt<<":\n";
                for (itgroute=GraphRoute.begin(); itgroute!=GraphRoute.end(); itgroute++) {
                    LogPowerRate<<"\nRoute "<<itgroute->first<<" : ";
                    for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
                        //LogPowerRate<<"("<<itroute->first<<" : "<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<") , ";
                        if (itgroute->first==0) {
                            mpNodes[itroute->second.at(0)]->PowerRate[route]+=PowerTrans*itmpflow->second->GetRate();
                            mpNodes[itroute->second.at(1)]->PowerRate[route]+=PowerReceive*itmpflow->second->GetRate();
                        }
                        else {
                            mpNodes[itroute->second.at(1)]->PowerRate[route]+=PowerWait*itmpflow->second->GetRate();
                            
                        }
                        LogPowerRate<<itroute->second.at(0)<<"->"<<itroute->second.at(1)<<", "<<mpNodes[itroute->second.at(0)]->PowerRate[route]<<"->"<<mpNodes[itroute->second.at(1)]->PowerRate[route]<<"; ";
                        
                    }
                }
            }
        }
        
    }
    LogPowerRate<<"\n\n\n";
    //vector<float> MaxRate(vRoutes.size(),0);
    map<ROUTETYPE, float> MaxRate;
    //float MaxRate[vRoutes.size()]={0,0};
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        map<ROUTETYPE, float> PowerRate;
        map<ROUTETYPE, int> RawRate;
        //vector<float> PowerRate(vRoutes.size(),0);
        //vector<int> RawRate(vRoutes.size(),0);
        LogPowerRate<<itnodes->first<<" ";
        for (int i=0; i<vRoutes.size(); i++) {
            RawRate[vRoutes[i]]=itnodes->second->PowerRate[vRoutes[i]];
            PowerRate[vRoutes[i]]=float(itnodes->second->PowerRate[vRoutes[i]])/float(itnodes->second->GetBattery());
            LogPowerRate<<PowerRate[vRoutes[i]]<<" ";
        }
        LogPowerRate<<"\n Load "<<itnodes->second->GetLoad()<<"\n";
        //LogPowerRate<<itnodes->second->battery<<"\n";
        //LogPowerRate<<itnodes->first<<" "<<RawRate[DIJKSTRA]<<" "<<RawRate[GLPK]<<" "<<PowerRate[DIJKSTRA]<<" "<<PowerRate[GLPK]<<" "<<itnodes->second->battery<<"\n";
        for (int i=0; i<vRoutes.size(); i++) {
            if (PowerRate[vRoutes[i]]>MaxRate[vRoutes[i]]) {
                MaxRate[vRoutes[i]]=PowerRate[vRoutes[i]];
            }
        }
    }
    
    LogPowerRate<<"\n\nMaximum Power Rate:";
    for (int i=0; i<vRoutes.size(); i++) {
        LogPowerRate<<PrintRouteType(vRoutes[i])<<", "<<PrintRouteType(vRoutes[i])<<"/"<<PrintRouteType(vRoutes[0])<<", ";
    }
    LogPowerRate<<"\n";
    
    for (int i=0; i<vRoutes.size(); i++) {
        LogPowerRate<<MaxRate[vRoutes[i]]<<" ";
        LogLifetimeEXP<<1/MaxRate[vRoutes[i]]<<" ";
        LogLifetimeRatio<<MaxRate[DIJKSTRA]/MaxRate[vRoutes[i]]<<" ";
        if (MaxRate[vRoutes[i]]>0) {
            LogPowerRate<<MaxRate[vRoutes[0]]/MaxRate[vRoutes[i]]<<" ";
        }
        else {
            LogPowerRate<<"0 ";
        }
    }
    
    //LogLifetimeEXP<<"\n";
    LogPowerRate<<"\n";
}



bool GenerateBFSTopology(map<int, map<int, float> > mmTraffic, float threshold, set<int> &sNode, map<int, vector<int> > &mvtop){
    sNode.clear();
    mvtop.clear();
	map<int, map<int, float> >::iterator itmm;
	map<int, float>::iterator itm;
    for (itmm=mmTraffic.begin(); itmm!=mmTraffic.end(); itmm++) {
        for (itm=itmm->second.begin(); itm!=itmm->second.end(); itm++) {
            if (itm->second>=threshold) {
                sNode.insert(itmm->first);
                sNode.insert(itm->first);
                mvtop[itmm->first].push_back(itm->first);
            }
        }
    }
    if (sNode.size()>0) {
        return true;
    }
    else {
        return false;
    }
}



string ReliableScheduler::PrintRouteType(ROUTETYPE iroute)
{
    if (iroute==DIJKSTRA) {
        return "DIJ";
    }
    else if (iroute==GLPK){
        return "LPR";
    }
    else if (iroute==MINLOAD){
        return "GHS";
    }
    else if (iroute==HAN){
        return "HAN";
    }
    else if (iroute==MIPK){
        return "IPM";
    }
    else {
        cout<<"Wrong Route Type in PrintRouteType";
        exit(22);
    }
}


int ReliableScheduler::Shortestpath(int source)
{
    map<int, int> mDist;
    map<int, int> mLasthop;
    map<int, int> mQ;
    map<int, int>::iterator itmq;
    map<int, int> mConnected;
    int Bigdistance=500;
    
    map<int, map<int, map<int, float> > >::iterator itprr;
    map<int, map<int, float> >::iterator itneighbors;
    
    if (mmPRR.count(source)==0) {
        cout<<"source is not in the topology\n";
        exit(12);
    }
    
    mDist[source]=0;
    mLasthop[source]=0;
    mQ.insert(pair<int, int>(source,0));
    
    
    
    for (itprr=mmPRR.begin(); itprr!=mmPRR.end(); itprr++) {
        if (itprr->first!=source) {
            mQ.insert(pair<int, int>(itprr->first, Bigdistance));
        }
    }
    
    while (mQ.size()>0) {
        int cdist=Bigdistance;
        int cnode;
        for (itmq=mQ.begin(); itmq!=mQ.end(); itmq++) {
            if (cdist>itmq->second) {
                cdist=itmq->second;
                cnode=itmq->first;
            }
        }
        
        if (cdist==Bigdistance) {
            break;
        }
        
        mDist[cnode]=cdist;
        mQ.erase(cnode);
        mConnected.insert(pair<int, int>(cnode,0));
        
        
        for (itmmPRR=mmPRR[cnode].begin(); itmmPRR!=mmPRR[cnode].end(); itmmPRR++  ) {
            if (mConnected.count(itmmPRR->first)==0) {
                int alt=mDist[cnode]+1;
                if (alt<mQ[itmmPRR->first]) {
                    mQ[itmmPRR->first]=alt;
                    mLasthop[itmmPRR->first]=cnode;
                }
            }
            
        }
        
    }
    
    
    return mConnected.size();
    
    
    
    
}

bool ReliableScheduler:: FindBFSRoute(int source, int destination, set<int> sNode, map<int, vector<int> > mvtop, map<int, vector<int> > &SourceRoute)
{
    SourceRoute.clear();
    map<int, int> mLasthop;
    set<int> sConnected;
    if (source==destination) {
        cout<<"\n\n Source and destination are same\n\n";
        return false;
    }
    
    if (sNode.count(source)==0 || sNode.count(destination)==0) {
        cout<<"source or destination is not in the topology\n";
        return false;
    }
    
    queue<int> Q;
    Q.push(source);
    sConnected.insert(source);
    
    
    while (!Q.empty()) {
        int cnode=Q.front();
        
        
        Q.pop();
        if (mvtop.count(cnode)>0) {
            for (int nindex=0; nindex<mvtop[cnode].size(); nindex++) {
                int nnode=mvtop[cnode][nindex];
                if (sConnected.count(nnode)==0) {
                    Q.push(nnode);
                    sConnected.insert(nnode);
                    mLasthop[nnode]=cnode;
                    if (nnode==destination) {
                        break;
                    }
                }
            }
        }
    }
    if (sConnected.count(destination)==0) {
        return false;
    }
    
    vector<int> vroute;
    vroute.push_back(destination);
    int currentnode=destination;
    while (currentnode!=source) {
        currentnode=mLasthop[currentnode];
        vroute.push_back(currentnode);
    }
    reverse(vroute.begin(), vroute.end());
    
    for (int index=0; index<vroute.size()-1; index++) {
        SourceRoute[index+1].push_back(vroute.at(index));
        SourceRoute[index+1].push_back(vroute.at(index+1));
    }
    return true;
    
}

void ReliableScheduler::CreateDijkstraSourceRouting()
{
    
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        map<int, vector<int> > mRoute;
        bool WhetherRoute= FindDijkstraRoute(itnodes->first, iGateway, mmTopology, mRoute);
        if (WhetherRoute==true) {
            itnodes->second->SourceRoute=mRoute;
        }
        else {
            cout<<"No source route exits between "<<itnodes->first<<" and "<<iGateway<<"\n";
        }
    }
}

void ReliableScheduler::CreateDijkstraGraphRouting()
{
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        map<int, map<int, vector<int> > > mpRoute;
        FindDijkstraGraphRoute(itnodes->first, iGateway, mmTopology, mpRoute);
        itnodes->second->mpDijkstraUplinkRoute=mpRoute;
        mpRoute.clear();
        FindDijkstraGraphRoute(iGateway, itnodes->first, mmTopology, mpRoute);
        itnodes->second->mpDijkstraDownlinkRoute=mpRoute;
    }
}

void ReliableScheduler::CreateFlowDijkstraGraphRouting(){
    for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
        map<int, map<int, vector<int> > > mpRoute;
        FindDijkstraGraphRoute(itmpflow->second->GetSource(), itmpflow->second->GetDestination(), mmTopology, mpRoute);
        itmpflow->second->mpGraphRoute[DIJKSTRA][UP]=mpRoute;
    }
}

bool ReliableScheduler::FindDijkstraRoute(int source, int destination, map<int, map<int, int> > mmtop, map<int, vector<int> > &SourceRoute)
{
    map<int, int> mDist;
    map<int, int> mLasthop;
    map<int, int> mQ;
    map<int, int>::iterator itmq;
    map<int, int> mConnected;
    int Bigdistance=500;
    
    map<int, map<int, int > >::iterator itTopology;
    map<int, int>::iterator ittop;
    map<int, map<int, float> >::iterator itneighbors;
    
    if (source==destination) {
        cout<<"\n\n Source and destination are same\n\n";
        exit(11);
    }
    
    if (mmTopology.count(source)==0) {
        cout<<"source is not in the topology\n";
        exit(12);
    }
    
    mDist[source]=0;
    mLasthop[source]=0;
    mQ.insert(pair<int, int>(source,0));
    
    
    
    for (itTopology=mmtop.begin(); itTopology!=mmtop.end(); itTopology++) {
        if (itTopology->first!=source) {
            mQ.insert(pair<int, int>(itTopology->first, Bigdistance));
        }
    }
    
    while (mQ.size()>0) {
        int cdist=Bigdistance;
        int cnode;
        for (itmq=mQ.begin(); itmq!=mQ.end(); itmq++) {
            if (cdist>itmq->second) {
                cdist=itmq->second;
                cnode=itmq->first;
            }
        }
        
        if (cdist==Bigdistance) {
            cout<<"\n\n There is no Dijkstra route from "<<source<<" to "<<destination<<"\n\n";
            return false;
        }
        
        //        if (cnode==destination) {
        //            break;
        //        }
        
        mDist[cnode]=cdist;
        mQ.erase(cnode);
        mConnected.insert(pair<int, int>(cnode,0));
        
        
        for (ittop=mmtop[cnode].begin(); ittop!=mmtop[cnode].end(); ittop++  ) {
            if (mConnected.count(ittop->first)==0) {
                int alt=mDist[cnode]+1;
                if (alt<mQ[ittop->first]) {
                    mQ[ittop->first]=alt;
                    mLasthop[ittop->first]=cnode;
                }
            }
            
        }
        
    }
    
    
    vector<int> vroute;
    vector<int>::iterator itvroute;
    vroute.push_back(destination);
    int currentnode=destination;
    while (currentnode!=source) {
        currentnode=mLasthop[currentnode];
        itvroute=vroute.begin();
        vroute.insert(itvroute, currentnode);
        
    }
    map<int, vector<int> > mRoute;
    if (destination==iGateway) {
        for (int index=0; index<vroute.size()-2; index++) {
            mRoute[index+1].push_back(vroute.at(index));
            mRoute[index+1].push_back(vroute.at(index+1));
        }
    }
    else if (source==iGateway){
        int hopindex=1;
        for (int index=1; index<vroute.size()-1; index++) {
            mRoute[hopindex].push_back(vroute.at(index));
            mRoute[hopindex].push_back(vroute.at(index+1));
            hopindex++;
        }
    }
    else {
        for (int index=0; index<vroute.size()-1; index++) {
            mRoute[index+1].push_back(vroute.at(index));
            mRoute[index+1].push_back(vroute.at(index+1));
        }
    }
    
    
    SourceRoute=mRoute;
    return true;
    
}

bool ReliableScheduler::FindDijkstraGraphRoute(int source, int destination, map<int, map<int, int> > mmtop, map<int, map<int, vector<int> > > &GraphRoute)
{
    map<int, map<int, vector<int> > > mpRoute;
    map<int, vector<int> > PrimaryRoute;
    map<int, vector<int> >::iterator itroute;
    bool ExistRoute = FindDijkstraRoute(source, destination, mmtop, PrimaryRoute);
    if (ExistRoute==false) {
        cout<<"Dijkstra, No primary path from "<<source<< " to "<<destination<<"\n\n";
        exit(12);
        return false;
    }
    int routeindex=0;
    mpRoute[routeindex]=PrimaryRoute;
    for (itroute=PrimaryRoute.begin(); itroute!=PrimaryRoute.end(); itroute++) {
        map<int, map<int, int> > mmTempTop=mmtop;
        int sender=itroute->second.at(0);
        int receiver=itroute->second.at(1);
        mmTempTop[sender].erase(receiver);
        mmTempTop[receiver].erase(sender);
        map<int, vector<int> > SecondryRoute;
        bool isRoute=FindDijkstraRoute(sender, destination, mmTempTop, SecondryRoute);
        if (isRoute==true) {
            routeindex=itroute->first;
            mpRoute[routeindex]=SecondryRoute;
        }
    }
    GraphRoute=mpRoute;
    return true;
}



void ReliableScheduler::PrintNodes()
{
    cout<<"\n\nNodes:\n";
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        cout<<itnodes->first<<" "<<itnodes->second->GetLoad()<<"\n";
    }
    cout<<"\n\n";
}


void ReliableScheduler::PrintSourceRouting()
{
    LogSourceRouting<<"Source Routing:\n\n";
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        LogSourceRouting<<"\n"<<itnodes->first<<" : ";
        map<int, vector<int> >::iterator itroute;
        for (itroute=itnodes->second->SourceRoute.begin(); itroute!=itnodes->second->SourceRoute.end(); itroute++) {
            LogSourceRouting<<"("<<itroute->first<<" : "<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<") , ";
        }
    }
}



void ReliableScheduler::PrintGraphRouting()
{
    LogSourceRouting<<"\n\nGraph Routing:\n\n";
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        if (IsInVector(vAccessPoints, itnodes->first)==true) {
            continue;
        }
        LogSourceRouting<<"\n\n\n"<<itnodes->first<<" : ";
        
        ROUTETYPE route;
        DIRECTIONTYPE dirt;
        
        
        ROUTETYPE aroute[]={DIJKSTRA, GLPK};
        DIRECTIONTYPE adirt[]={UP, DOWN};
        for (int i=0; i<2; i++){
            for (int j=0; j<2; j++) {
                route=aroute[i];
                dirt=adirt[j];
                map<int, map<int, vector<int> > > GraphRoute;
                map<int, map<int, vector<int> > >::iterator itgroute;
                map<int, vector<int> >::iterator itroute;
                GraphRoute=itnodes->second->getGraphRoute(route, dirt);
                if (route==DIJKSTRA && dirt==UP) {LogSourceRouting<<"\n\n Dijkstra Uplink : \n";}
                if (route==DIJKSTRA && dirt==DOWN) {LogSourceRouting<<"\n\n Dijkstra Downlink : \n";}
                if (route==GLPK && dirt==UP) {LogSourceRouting<<"\n\n GLPK Uplink : \n";}
                if (route==GLPK && dirt==DOWN) {LogSourceRouting<<"\n\n GLPK Downlink : \n";}
                
                for (itgroute=GraphRoute.begin(); itgroute!=GraphRoute.end(); itgroute++) {
                    LogSourceRouting<<"\nRoute "<<itgroute->first<<" : ";
                    for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
                        LogSourceRouting<<"("<<itroute->first<<" : "<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<") , ";
                    }
                }
            }
            
        }
    }
}

void ReliableScheduler::CollectTopology(bool TestCase)
{
    mmPRR.clear();
    if (TestCase) {
        TopologyReader.open("topology/testcase.txt", ios::in);
    }
    else {
        TopologyReader.open("topology/profile_april21.txt", ios::in);
    }
    cout<<"start to read topology\n";
    if (TopologyReader.is_open())
    {
        cout<<"Topology reader is working \n";
        while(!TopologyReader.eof())
        {
            int node1, node2, channel, numpackets;
            float prr;
            TopologyReader>>node1>>node2>>numpackets>>channel;
            
            prr=float(numpackets)/float(100);
            
            if (prr >=PRRBound) {
                mmPRR[node1][node2][channel]=prr;
            }
            
            //cout<<"a new line added\n";
        }
        cout<<"\n Topology read  Done.";
        
        TopologyReader.close();
    }
    else cout << "Unable to open file";
    
    //TopologyReader.close();
    
    for (itmmmPRR=mmPRR.begin(); itmmmPRR!=mmPRR.end(); itmmmPRR++) {
        for (itmmPRR=itmmmPRR->second.begin(); itmmPRR!=itmmmPRR->second.end(); itmmPRR++) {
            LogTopology<<"\n"<<itmmmPRR->first<<" "<<itmmPRR->first<<" "<<itmmPRR->second.size()<<": ";
            for (itmPRR=itmmPRR->second.begin(); itmPRR!=itmmPRR->second.end(); itmPRR++) {
                LogTopology<<itmPRR->first<<" , "<<itmPRR->second<<"; ";
            }
        }
    }
    
    for (itmmmPRR=mmPRR.begin(); itmmmPRR!=mmPRR.end(); itmmmPRR++) {
        for (itmmPRR=itmmmPRR->second.begin(); itmmPRR!=itmmmPRR->second.end(); itmmPRR++) {
            //LogChannelDiversity<<itmmPRR->second.size()<<"\n";
        }
    }
    
    
}
 
void ReliableScheduler::pickup(int offset, int k) {
    if (k == 0) {
        static int comindex=0;
        comindex++;
        //pretty_print(combination);
        mpChannels[comindex]=combination;
        return;
    }
    for (int i = offset; i <= vChannels.size() - k; ++i) {
        combination.push_back(vChannels[i]);
        pickup(i+1, k-1);
        combination.pop_back();
    }
}

void ReliableScheduler::pickup_copy(int offset, int k, vector<int> pool, map<int, vector<int> > &mpCandidate, vector<int> &Candidate)
{
    if (k == 0) {
        static int comindex=0;
        comindex++;
        //pretty_print(combination);
        mpCandidate[comindex]=Candidate;
        return;
    }
    for (int i = offset; i <= pool.size() - k; ++i) {
        Candidate.push_back(pool[i]);
        pickup_copy(i+1, k-1, pool, mpCandidate, Candidate);
        Candidate.pop_back();
    }
}

void ReliableScheduler::SelectChannel(bool preset, bool TestCase)
{
    if (preset) {
        //int aChannels [2]={11,  12};
        vChannels.clear();
        if (TestCase) {
            int aChannels []={11};
            int nchannels=sizeof(aChannels)/sizeof(*aChannels);
            
            for (int index=0; index<nchannels; index++) {
                vChannels.push_back(aChannels[index]);
            }
        }
        else{
            int aChannels []={11,  12,  13,  14};
            int nchannels=sizeof(aChannels)/sizeof(*aChannels);
            
            for (int index=0; index<nchannels; index++) {
                vChannels.push_back(aChannels[index]);
            }
        }
        m=vChannels.size();
        iTotalChannels=vChannels.size();
        return;
    }
    
    vChannels.clear();
    for (int index=11; index<27; index++) {
        vChannels.push_back(index);
    }
    
    map<int, int> mpChannelLinks;
    map<int, vector<int> > mBestChannelSets;
    map<int, vector<int> >::iterator itbestchannelset;
    map<int, int>::iterator itchlinks;
    
    for (int numchannels=1; numchannels<17; numchannels++) {
        cout<<"numchannels: "<<numchannels<<"\n";
        mpChannels.clear();
        combination.clear();
        pickup(0, numchannels);
        
        map<int, vector<int> >::iterator itmpvector;
        
        
        map<int, int> mpNumLinks;
        map<int, int>::iterator itnumlinks;
        
        // To check how many links satisify the reliability requirement in this part.
        
        for (itmpvector=mpChannels.begin(); itmpvector!=mpChannels.end(); itmpvector++) {
            vector<int> CurrentChannels;
            for (int index=0; index<itmpvector->second.size(); index++) {
                CurrentChannels.push_back(itmpvector->second.at(index));
            }
            mpNumLinks[itmpvector->first]=0;
            for (itmmmPRR=mmPRR.begin(); itmmmPRR!=mmPRR.end(); itmmmPRR++) {
                for (itmmPRR=itmmmPRR->second.begin(); itmmPRR!=itmmmPRR->second.end(); itmmPRR++) {
                    bool qulify=true;
                    map<int, float> reverseprr;
                    if (mmPRR.count(itmmPRR->first)==1) {
                        if (mmPRR[itmmPRR->first].count(itmmmPRR->first)==1) {
                            reverseprr=mmPRR[itmmPRR->first][itmmmPRR->first];
                        }
                        else {
                            qulify=false;
                        }
                    }
                    else {
                        qulify=false;
                    }
                    for (int index=0; index<CurrentChannels.size(); index++) {
                        if (reverseprr.count(CurrentChannels.at(index))==0) {
                            qulify=false;
                        }
                    }
                    
                    for (int index=0; index<CurrentChannels.size(); index++) {
                        if (itmmPRR->second.count(CurrentChannels.at(index))==0) {
                            qulify=false;
                        }
                    }
                    
                    if (qulify==true) {
                        mpNumLinks[itmpvector->first]++;
                    }
                }
            }
        }
        vector<int> bestchannleset;
        int maxnumlinks=0;
        for (itnumlinks=mpNumLinks.begin(); itnumlinks!=mpNumLinks.end(); itnumlinks++) {
            if (itnumlinks->second>maxnumlinks) {
                maxnumlinks=itnumlinks->second;
                bestchannleset.clear();
                bestchannleset=mpChannels[itnumlinks->first];
            }
        }
        
        mpChannelLinks[numchannels]=maxnumlinks;
        mBestChannelSets[numchannels]=bestchannleset;
        
        
        
        
        //LogChannelDiversity<<maxnumlinks<<"\n";
        
        for (itmpvector=mpChannels.begin(); itmpvector!=mpChannels.end(); itmpvector++) {
            
            for (int index=0; index<itmpvector->second.size(); index++) {
                //LogChannelDiversity<<itmpvector->second.at(index)<<" ";
            }
            //LogChannelDiversity<<" : "<<mpNumLinks[itmpvector->first]<<"\n";
            
        }
        
    }
    int bestnumchannels;
    double maxbandwidth=0;
    for (itchlinks=mpChannelLinks.begin(); itchlinks!=mpChannelLinks.end(); itchlinks++) {
        double bandwidth=pow(itchlinks->first, 1.3) * itchlinks->second;
        //double bandwidth=itchlinks->first * itchlinks->second;
        LogChannelDiversity<<itchlinks->first<<"  "<<itchlinks->second<<"  "<<bandwidth<<"\n";
        if (maxbandwidth<bandwidth) {
            bestnumchannels=itchlinks->first;
            maxbandwidth=bandwidth;
        }
    }
    
    //bestnumchannels=5;
    //maxbandwidth=mpChannelLinks[bestnumchannels];
    cout<<"\n Best num channels: "<<bestnumchannels<<"; Max Bandwidth: "<<maxbandwidth<<"\n";
    
    for (itbestchannelset=mBestChannelSets.begin(); itbestchannelset!=mBestChannelSets.end(); itbestchannelset++) {
        for (int index=0; index<itbestchannelset->second.size(); index++) {
            LogChannelDiversity<<itbestchannelset->second.at(index)<<"  ";
        }
        LogChannelDiversity<<"\n";
    }
    
    
    vChannels.clear();
    vChannels=mBestChannelSets[bestnumchannels];
    
    
        
    
}

void ReliableScheduler::SelectLinks()
{

    for (int index=0; index<vChannels.size(); index++) {
        cout<<vChannels.at(index)<<"\t";
    }
    
    map<int, map<int, map<int, float > > > mmPRRTemp;   //key1, node id; key2, node id; key3, channel; value, prr
    
    mmPRRTemp=mmPRR;
    mmPRR.clear();
    
    for (itmmmPRR=mmPRRTemp.begin(); itmmmPRR!=mmPRRTemp.end(); itmmmPRR++) {
        for (itmmPRR=itmmmPRR->second.begin(); itmmPRR!=itmmmPRR->second.end(); itmmPRR++) {
            bool qulify=true;
            map<int, float> reverseprr;
            if (mmPRRTemp.count(itmmPRR->first)==1) {
                if (mmPRRTemp[itmmPRR->first].count(itmmmPRR->first)==1) {
                    reverseprr=mmPRRTemp[itmmPRR->first][itmmmPRR->first];
                }
                else {
                    qulify=false;
                }
            }
            else {
                qulify=false;
            }
            for (int index=0; index<vChannels.size(); index++) {
                if (reverseprr.count(vChannels.at(index))==0) {
                    qulify=false;
                }
            }
            for (int index=0; index<vChannels.size(); index++) {
                if (itmmPRR->second.count(vChannels.at(index))==0) {
                    qulify=false;
                }
            }
            if (qulify==true) {
                LogTopology<<itmmmPRR->first<<"  "<<itmmPRR->first<<": ";
                LogLinks<<itmmmPRR->first<<"  "<<itmmPRR->first<<"  ";
                float minprr=2;
                for (int index=0; index<vChannels.size(); index++) {
                    float currentprr=itmmPRR->second.at(vChannels.at(index));
                    mmPRR[itmmmPRR->first][itmmPRR->first][vChannels.at(index)]=itmmPRR->second.at(vChannels.at(index));
                    LogTopology<<vChannels.at(index)<<" , "<<itmmPRR->second.at(vChannels.at(index))<<"; ";
                    if (minprr>currentprr) {
                        minprr=currentprr;
                    }
                    
                }
                LogLinks<<minprr<<"\n";
                mvNeighbor[itmmmPRR->first].push_back(itmmPRR->first);
                LogTopology<<"\n";
            }
        }
    }
    
    LogTopology<<"\n mvNeighbor: \n";
    
    map<int, vector<int> >::iterator itmvNeighbors;
    for (itmvNeighbors=mvNeighbor.begin(); itmvNeighbors!=mvNeighbor.end(); itmvNeighbors++) {
        LogTopology<<"\n"<<itmvNeighbors->first<<":  ";
        for (int index=0; index<itmvNeighbors->second.size(); index++) {
            LogTopology<<itmvNeighbors->second.at(index)<<"  ";
        }
    }
    
    mmTopology.clear();
    for (itmmmPRR=mmPRR.begin(); itmmmPRR!=mmPRR.end(); itmmmPRR++) {
        for (itmmPRR=itmmmPRR->second.begin(); itmmPRR!=itmmmPRR->second.end(); itmmPRR++) {
            mmTopology[itmmmPRR->first][itmmPRR->first]=0;
        }
    }
    
    
    
    map<int, map<int, int> >::iterator ittop;
    
    mpNodes.clear();
    for (ittop=mmTopology.begin(); ittop!=mmTopology.end(); ittop++) {
        Node* node;
        int nodeid=ittop->first;
        node = new Node(nodeid);
        mpNodes.insert(pair<int, Node*>(nodeid, node));
    }
    for (itmmmPRR=mmPRR.begin(); itmmmPRR!=mmPRR.end(); itmmmPRR++) {
        if (IsInVector(vAccessPoints, itmmmPRR->first)==true) {
            continue;
        }
        Node* node;
        int nodeid=itmmmPRR->first;
        node = new Node(nodeid);
        if (mpNodes.count(nodeid)==0) {
            mpNodes.insert(pair<int, Node*>(nodeid, node));
        }
        
    }
    


}

void ReliableScheduler::CheckLinkConnectivity()
{
    // run after selectlinks();
    map<int, map<int, int> > mmNumPaths;
    map<int, Node*>::iterator itnodes2;
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        for (itnodes2=itnodes; itnodes2!=mpNodes.end(); itnodes2++) {
            int maxflow;
            map<int, map<int, int> > CurrentFlow;
            if (itnodes2->first==itnodes->first) {
                maxflow=0;
            }
            else {
                maxflow=EdgeDisjointPaths(itnodes->first, itnodes2->first, mmTopology, CurrentFlow);
            }
            mmNumPaths[itnodes->first][itnodes2->first]=maxflow;
            mmNumPaths[itnodes2->first][itnodes->first]=maxflow;
        }
    }
    
    map<int, map<int, int> >::iterator itmpNumPaths;
    map<int, int>::iterator itNumPaths;
    
    LogTopology<<"Number of paths between two nodes: \n\n  ";
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        LogTopology<<itnodes->first<<" ";
    }
    for (itmpNumPaths=mmNumPaths.begin(); itmpNumPaths!=mmNumPaths.end(); itmpNumPaths++) {
        LogTopology<<"\n"<<itmpNumPaths->first<<" ";
        for (itNumPaths=itmpNumPaths->second.begin(); itNumPaths!=itmpNumPaths->second.end(); itNumPaths++) {
            LogTopology<<itNumPaths->second<<" ";
        }
    }
}

void ReliableScheduler:: GetAccessPoints_Neighbors(int numpoints, bool preset)
{
    if (preset) {
        vAccessPoints.clear();
        int aAccessPoints [3]={154, 162, 173};
        if (sizeof(aAccessPoints)/sizeof(*aAccessPoints)!=numpoints) {
            cout<<"Wrong number of access points\n\n";
            exit(12);
        }
        for (int index=0; index<numpoints; index++) {
            vAccessPoints.push_back(aAccessPoints[index]);
        }
        
        for (int indexap=0; indexap<vAccessPoints.size(); indexap++) {
            mmTopology[iGateway][vAccessPoints.at(indexap)]=0;
            mmTopology[vAccessPoints.at(indexap)][iGateway]=0;
        }
        
        return;
    }
    
    vAccessPoints.clear();
    LogTopology<<"\n\n Number of nodes: "<<mmPRR.size()<<"\n";
    
    map<int, int> mNumCon;
    map<int, int> mNumNeighbor;
    
    int MaxNumCon=0;
    vector<int> vMaxCon;
    for (itmmmPRR=mmPRR.begin(); itmmmPRR!=mmPRR.end(); itmmmPRR++) {
        int inodeid=itmmmPRR->first;
        cout<<"Node id: "<<inodeid<<"\n";
        int inumcon=NumConnectedNode(inodeid);
        mNumCon[inodeid]=inumcon;
        mNumNeighbor.insert(pair<int, int>(inodeid, itmmmPRR->second.size()));
        LogTopology<<inodeid<<" : "<<itmmmPRR->second.size()<<"  "<<inumcon<<"\n";
        if (inumcon>MaxNumCon) {
            MaxNumCon=inumcon;
            vMaxCon.clear();
            vMaxCon.push_back(inodeid);
        }
        else if (inumcon==MaxNumCon){
            vMaxCon.push_back(inodeid);
        }
    }
    
    if (vMaxCon.size()==0) {
        cout<<"\n\n Error, no access points \n\n";
        exit(12);
    }
    
    multimap<int, int> mmNumNei;
    for (int index=0; index<vMaxCon.size(); index++) {
        mmNumNei.insert(pair<int, int>(mNumNeighbor[vMaxCon.at(index)],vMaxCon.at(index)));
    }
    
    
    multimap<int,int>::reverse_iterator ritmnumn;
    int index=0;
    for (ritmnumn=mmNumNei.rbegin(); ritmnumn!=mmNumNei.rend(); ritmnumn++) {
        vAccessPoints.push_back(ritmnumn->second);
        index++;
        if (index==numpoints) {
            break;
        }
        
    }
    
    
    //cout<<"Access Points: \n";
    LogSourceRouting<<"Access Points: ";
    for (int indexap=0; indexap<vAccessPoints.size(); indexap++) {
        cout<<vAccessPoints.at(indexap)<<"\t";
        LogSourceRouting<<vAccessPoints.at(indexap)<<"\t";
    }
    LogSourceRouting<<"\n";
    
    
    
    for (int indexap=0; indexap<vAccessPoints.size(); indexap++) {
        mmTopology[iGateway][vAccessPoints.at(indexap)]=0;
        mmTopology[vAccessPoints.at(indexap)][iGateway]=0;
    }
    
}

void ReliableScheduler:: GetAccessPoints_Paths(int numpoints, bool preset, bool TestCase)
{
    if (preset) {
        vAccessPoints.clear();
        if (TestCase) {
            vAccessPoints.push_back(1);
            vAccessPoints.push_back(8);
            LogTopology<<"\nAccess Points: ";
            LogTopology<<vAccessPoints[0]<<" ";
            LogTopology<<vAccessPoints[1]<<" ";
            LogTopology<<"\n";
        }
        else{
            //int aAccessPoints [4]={129, 155, 168, 132};
            int aAccessPoints [4]={154,	155, 171, 170};
            //int aAccessPoints [4]={123,	124, 132, 133};
            cout<<numpoints<<" "<<sizeof(aAccessPoints)<<" "<<sizeof(*aAccessPoints)<<"\n";
            if (sizeof(aAccessPoints)/sizeof(*aAccessPoints)!=numpoints) {
                cout<<"Wrong number of access points\n\n";
                exit(12);
            }
            LogTopology<<"\nAccess Points: ";
            for (int index=0; index<numpoints; index++) {
                vAccessPoints.push_back(aAccessPoints[index]);
                LogTopology<<aAccessPoints[index]<<" ";
            }
        }
        
        LogTopology<<"\n";
        for (int indexap=0; indexap<vAccessPoints.size(); indexap++) {
            mmTopology[iGateway][vAccessPoints.at(indexap)]=0;
            mmTopology[vAccessPoints.at(indexap)][iGateway]=0;
        }
        
        for (int i=0; i<vAccessPoints.size()-1; i++) {
            for (int j=i+1; j<vAccessPoints.size(); j++) {
                mmTopology[vAccessPoints[i]][vAccessPoints[j]]=0;
                mmTopology[vAccessPoints[j]][vAccessPoints[i]]=0;
            }
        }
        return;
    }
    
    vAccessPoints.clear();
    LogTopology<<"\n\n Number of nodes: "<<mmPRR.size()<<"\n";
    
    
    map<int, vector<int> > mpCandidate;
    map<int, vector<int> > mpNumPaths;
    map<int, int> mpMinPaths;
    map<int, vector<int> >::iterator itCandiate;
    vector<int> Candidate;
    vector<int> pool;
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        pool.push_back(itnodes->first);
    }
    
    
    
    pickup_copy(0, numpoints, pool, mpCandidate, Candidate);
    
    cout<<"Choosing Access Points from "<<mpCandidate.size()<< " cases \n\n";
    LogTopology<<"Choosing Access Points\n\n";
    LogChannelDiversity<<"Choosing Access Points\n\n";
    for (itCandiate=mpCandidate.begin(); itCandiate!=mpCandidate.end(); itCandiate++) {
        Candidate=itCandiate->second;
        LogTopology.flush();
        LogChannelDiversity.flush();
        cout<<"\n"<<itCandiate->first<<" : ";
        LogTopology<<"\n"<<itCandiate->first<<" : ";
        LogChannelDiversity<<"\n"<<itCandiate->first<<" : ";
        map<int, map<int, int> > TempTopology;
        TempTopology=mmTopology;
        for (int index=0; index<Candidate.size(); index++) {
            //cout<<Candidate.at(index)<<" ";
            LogTopology<<Candidate.at(index)<<" ";
            LogChannelDiversity<<Candidate.at(index)<<" ";
            TempTopology[Candidate.at(index)][iGateway]=0;
            TempTopology[iGateway][Candidate.at(index)]=0;
        }
        LogTopology<<" : ";
        LogChannelDiversity<<" : ";
        int minPath=INFINITY;
        for (int index=0; index<pool.size(); index++) {
            bool exitroute;
            int maxflow;
            if (IsInVector(Candidate, pool.at(index))) {
                maxflow=100;
                exitroute=true;
            }
            else {
                map<int, map<int, int> > CurrentFlow;
                map<int, vector<int> > SourceRoute;
                maxflow=EdgeDisjointPaths(pool.at(index), iGateway, TempTopology, CurrentFlow);
                if (maxflow>0) {
                    exitroute=true;
                }
                else {
                    exitroute=false;
                }
                //exitroute=FindRoute(pool.at(index), iGateway, TempTopology, SourceRoute);
            }
            mpNumPaths[itCandiate->first].push_back(maxflow);
            LogTopology<<maxflow<<" ";
            LogChannelDiversity<<exitroute<<" ";
            if (maxflow<minPath) {
                minPath=maxflow;
            }
        }
        mpMinPaths[itCandiate->first]=minPath;
        LogTopology<<": "<<minPath;
        
    }
    
    map<int, int>::iterator itminpath;
    int minpath=0;
    int minindex=0;
    for (itminpath=mpMinPaths.begin(); itminpath!=mpMinPaths.end(); itminpath++) {
        if (minpath<itminpath->second) {
            minpath=itminpath->second;
            minindex=itminpath->first;
        }
    }
    
    Candidate=mpCandidate[minindex];
    for (int index=0; index<Candidate.size(); index++) {
        vAccessPoints.push_back(Candidate.at(index));
    }
    
    
    cout<<"Access Points: \n";
    LogSourceRouting<<"Access Points: ";
    for (int indexap=0; indexap<vAccessPoints.size(); indexap++) {
        cout<<vAccessPoints.at(indexap)<<"\t";
        LogSourceRouting<<vAccessPoints.at(indexap)<<"\t";
    }
    LogSourceRouting<<"\n";
    
    
    
    for (int indexap=0; indexap<vAccessPoints.size(); indexap++) {
        mmTopology[iGateway][vAccessPoints.at(indexap)]=0;
        mmTopology[vAccessPoints.at(indexap)][iGateway]=0;
    }
    
}

void ReliableScheduler::SetNodeBattery(bool TestCase)
{
    if (TestCase) {
        mpNodes[1]->SetBattery(BigBattery);
        mpNodes[2]->SetBattery(20);
        mpNodes[3]->SetBattery(300);
        mpNodes[4]->SetBattery(400);
        mpNodes[5]->SetBattery(200);
        mpNodes[6]->SetBattery(200);
        mpNodes[7]->SetBattery(200);
        mpNodes[8]->SetBattery(200);
        mpNodes[9]->SetBattery(20);
    }
    else{
        int id=1;
        for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
            //itnodes->second->rate=pow(2, rand() % 8 +1);
            //itnodes->second->rate=0;
            for (int index=0; index<vRoutes.size(); index++) {
                itnodes->second->PowerRate[vRoutes[index]]=0;
            }

            if (IsInVector(vAccessPoints, itnodes->first)) {
                itnodes->second->SetBattery(BigBattery);
            }
            else {

                if (itnodes->second->GetBattery()==0) {
                    //itnodes->second->SetBattery(100*(50+(itnodes->first%500)));
                    itnodes->second->SetBattery(100*(50+rand() %100 ));
                    //itnodes->second->SetBattery(500*(500-itnodes->first));
                    
                }
                
            }
            id++;
        }
    }
    
}

int ReliableScheduler::BreadthFirstSearch(map<int, map<int, int> > Capacity, map<int, map<int, int> > CurrentFlow, map<int, vector<int> > Neighbors, map<int, int> &Parents, map<int, int> &MaxFlow, int source, int destination)
{
    map<int, map<int, int> >::iterator itmpCapacity;
    for (itmpCapacity=Capacity.begin(); itmpCapacity!=Capacity.end(); itmpCapacity++) {
        if (itmpCapacity->first==source) {
            Parents[itmpCapacity->first]=-2;
            MaxFlow[itmpCapacity->first]=INFINITY;
        }
        else {
            Parents[itmpCapacity->first]=-1;
        }
    }
    
    queue<int> Q;
    Q.push(source);
    while (Q.size()>0) {
        int u=Q.front();
        Q.pop();
        for (int index=0; index<Neighbors[u].size(); index++) {
            int v=Neighbors[u].at(index);
            if (Capacity[u][v]-CurrentFlow[u][v]>0 && Parents[v]==-1) {
                Parents[v]=u;
                MaxFlow[v]=min(MaxFlow[u], Capacity[u][v]-CurrentFlow[u][v]);
                if (v!=destination) {
                    Q.push(v);
                }
                else {
                    return MaxFlow[destination];
                }
            }
        }
        
    }
    return 0;
}

int ReliableScheduler::EdmondsKarp(map<int, map<int, int> > Capacity, map<int, vector<int> > Neighbors, int source, int destination, map<int, map<int, int> > &CurrentFlow)
{
    CurrentFlow.clear();
    int maxflow=0;
    map<int, map<int, int> >::iterator itmpCapacity;
    map<int, int>::iterator itCapacity;
    for (itmpCapacity=Capacity.begin(); itmpCapacity!=Capacity.end(); itmpCapacity++) {
        for (itCapacity=itmpCapacity->second.begin(); itCapacity!=itmpCapacity->second.end(); itCapacity++) {
            CurrentFlow[itmpCapacity->first][itCapacity->first]=0;
        }
    }
    
    while (true) {
        map<int, int> Parents;
        map<int, int> MaxFlows;
        int avilableflow=BreadthFirstSearch(Capacity, CurrentFlow, Neighbors, Parents, MaxFlows, source, destination);
        if (avilableflow==0) {
            break;
        }
        maxflow+=avilableflow;
        int v=destination;
        while (v!=source) {
            int u=Parents[v];
            CurrentFlow[u][v]+=avilableflow;
            CurrentFlow[v][u]-=avilableflow;
            v=u;
        }
    }
    return maxflow;
}

int ReliableScheduler::EdgeDisjointPaths(int source, int destination, map<int, map<int, int> > Topology, map<int, map<int, int> > &CurrentFlow)
{
    map<int, map<int, int> > Capacity;
    map<int, vector<int> > Neighbors;
    map<int, map<int, int> >::iterator itmpTopology;
    map<int, int>::iterator itTopology;
    for (itmpTopology=Topology.begin(); itmpTopology!=Topology.end(); itmpTopology++) {
        for (itTopology=itmpTopology->second.begin(); itTopology!=itmpTopology->second.end(); itTopology++) {
            Neighbors[itmpTopology->first].push_back(itTopology->first);
            if (itmpTopology->first==iGateway || itTopology->first==iGateway) {
                Capacity[itmpTopology->first][itTopology->first]=INFINITY;
            }
            else {
                Capacity[itmpTopology->first][itTopology->first]=1;
            }
            
        }
    }
    int maxflow=EdmondsKarp(Capacity, Neighbors, source, destination, CurrentFlow);
    return maxflow;
    
}

void ReliableScheduler::CountEdgeDisjointPaths()
{
    
    LogEdgeDisjointPaths<<"\n\n Number of Edge Disjoint Paths from nodes to Gateway\n\n";
    map<int, int> NumDisjointPahts;
    map<int, int>::iterator itNumPaths;
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        cout<<"\n Check node "<<itnodes->first<<"\n";
        if (IsInVector(vAccessPoints, itnodes->first)) {
            NumDisjointPahts[itnodes->first]=INFINITY;
            continue;
        }
        map<int, map<int, int> > CurrentFlow;
        map<int, map<int, int> > RealFlow;
        map<int, map<int, int> >::iterator itmpCFlow;
        map<int, int>::iterator itCFlow;
        int maxflow;
        int source=itnodes->first;
        int destination;
        destination=iGateway;
        
        FlowLog<<source<<"\t"<<destination<<"\n";

        maxflow=EdgeDisjointPaths(source, destination, mmTopology, CurrentFlow);
        NumDisjointPahts[itnodes->first]=maxflow;
        LogEdgeDisjointPaths<<"\nNode "<<itnodes->first<<" has "<<maxflow<<" edge disjont Paths \n";
        FlowLog<<"\nNode "<<itnodes->first<<" has "<<maxflow<<" edge disjont Paths \n";
        
        for (itmpCFlow=CurrentFlow.begin(); itmpCFlow!=CurrentFlow.end(); itmpCFlow++) {
            for (itCFlow=itmpCFlow->second.begin(); itCFlow!=itmpCFlow->second.end(); itCFlow++) {
                if (itCFlow->second>0) {
                    LogEdgeDisjointPaths<<itmpCFlow->first<<"\t"<<itCFlow->first<<"\t"<<itCFlow->second<<"\n";
                    FlowLog<<itmpCFlow->first<<"\t"<<itCFlow->first<<"\t"<<itCFlow->second<<"\n";
                    RealFlow[itmpCFlow->first][itCFlow->first]=itCFlow->second;
                }
            }
        }
        
        queue<int> Qsource;
        map<int, vector<int> > mpPaths;
        map<int, vector<int> >::iterator itmpPahts;
        
        
        FlowLog<<"Source "<<source<<endl;
        for (itCFlow=RealFlow[source].begin(); itCFlow!=RealFlow[source].end(); itCFlow++) {
            Qsource.push(itCFlow->first);
            FlowLog<<itCFlow->first<<"\n";
            
        }
        RealFlow.erase(source);
        int pathindex=0;
        while (Qsource.size()>0) {
            int u=Qsource.front();
            Qsource.pop();
            mpPaths[pathindex].push_back(source);
            mpPaths[pathindex].push_back(u);
            FlowLog<<"\n"<<u<<"\t";
            while (u!=destination) {
                map<int, int >::iterator itrealflow;
                if (RealFlow[u].size()<1) {
                    FlowLog.flush();
                    cout<<"Missing path from "<<u<<" \n";
                    exit(12);
                }
                int v;
                bool existpath=false;
                for (itrealflow=RealFlow[u].begin(); itrealflow!=RealFlow[u].end(); itrealflow++) {
                    if (itrealflow->second>0) {
                        v=itrealflow->first;
                        itrealflow->second--;
                        existpath=true;
                        break;
                    }
                }
                if (existpath==false) {
                    cout<<"Missing path from "<<u<<" \n";
                    exit(12);
                }
                mpPaths[pathindex].push_back(v);
                u=v;
                FlowLog<<u<<"\t";
            }
            pathindex++;
        }
        
        LogEdgeDisjointPaths<<"\n Disjoint Paths: \n";
        map<int, vector<int> >::iterator itmpPaths;
        for (itmpPaths=mpPaths.begin(); itmpPaths!=mpPaths.end(); itmpPaths++) {
            LogEdgeDisjointPaths<<itmpPaths->first<<" : ";
            for (int index=0; index<itmpPaths->second.size(); index++) {
                LogEdgeDisjointPaths<<itmpPaths->second.at(index)<<"\t";
            }
            LogEdgeDisjointPaths<<"\n";
        }
        
        
        map<int, map<int, vector<int> > > mpUplinkRoute;
        map<int, map<int, vector<int> > > mpDownlinkRoute;
        map<int, map<int, vector<int> > >::iterator itmpRoutes;
        map<int, vector<int> >::iterator itroute;
        
        for (itmpPahts=mpPaths.begin(); itmpPahts!=mpPaths.end(); itmpPahts++) {
            for (int index=0; index<itmpPahts->second.size()-2; index++) {
                mpUplinkRoute[itmpPahts->first][index].push_back(itmpPahts->second.at(index));
                mpUplinkRoute[itmpPahts->first][index].push_back(itmpPahts->second.at(index+1));
            }
            int reverseindex=0;
            for (int index=itmpPahts->second.size()-2; index>0; index--) {
                mpDownlinkRoute[itmpPahts->first][reverseindex].push_back(itmpPahts->second.at(index));
                mpDownlinkRoute[itmpPahts->first][reverseindex].push_back(itmpPahts->second.at(index-1));
                reverseindex++;
            }
        }
        
        LogEdgeDisjointPaths<<"\n Disjoint Uplink Routes: \n";
        for (itmpRoutes=mpUplinkRoute.begin(); itmpRoutes!=mpUplinkRoute.end(); itmpRoutes++) {
            LogEdgeDisjointPaths<<"\n"<<itmpRoutes->first<<" :";
            for (itroute=itmpRoutes->second.begin(); itroute!=itmpRoutes->second.end(); itroute++) {
                LogEdgeDisjointPaths<<"("<<itroute->first<<" "<<itroute->second.at(0)<<" "<<itroute->second.at(1)<<") ";
            }
        }
        
        LogEdgeDisjointPaths<<"\n Disjoint Downlink Routes: \n";
        for (itmpRoutes=mpDownlinkRoute.begin(); itmpRoutes!=mpDownlinkRoute.end(); itmpRoutes++) {
            LogEdgeDisjointPaths<<"\n"<<itmpRoutes->first<<" :";
            for (itroute=itmpRoutes->second.begin(); itroute!=itmpRoutes->second.end(); itroute++) {
                LogEdgeDisjointPaths<<"("<<itroute->first<<" "<<itroute->second.at(0)<<" "<<itroute->second.at(1)<<") ";
            }
        }
        itnodes->second->mpDisjontPaths=mpPaths;
        itnodes->second->mpDownlinkDisjointRoutes=mpDownlinkRoute;
        itnodes->second->mpUplinkDisjointRoutes=mpUplinkRoute;
    }
    LogEdgeDisjointPaths<<"\n Number of disjont paths:\n";
    for (itNumPaths=NumDisjointPahts.begin(); itNumPaths!=NumDisjointPahts.end(); itNumPaths++) {
        LogEdgeDisjointPaths<<itNumPaths->first<<" "<<itNumPaths->second<<"\n";
    }

}

bool ReliableScheduler::CheckFaultTolerantRoutes_Uplink()
{
    map<int, int> mDist;
    map<int, int> mLasthop;
    map<int, int> mQ;
    map<int, int>::iterator itmq;
    map<int, int> mConnected;
    
    map<int, map<int, int > >::iterator itTopology;
    map<int, int>::iterator ittop;
    map<int, int>::iterator itinnertop;
    map<int, map<int, float> >::iterator itneighbors;
    


    
    mDist[iGateway]=0;
    mLasthop[iGateway]=0;
    //mQ.insert(pair<int, int>(iGateway,0));
    
    
    
    for (itTopology=mmTopology.begin(); itTopology!=mmTopology.end(); itTopology++) {
        if (itTopology->first!=iGateway) {
            mDist.insert(pair<int, int>(itTopology->first, INFINITY));
        }
    }
    
    LogFaultTolerantRoute<<"Total number of nodes :  "<<mDist.size()<<"\n";
    
    mConnected[iGateway]=0;
    for (ittop=mmTopology[iGateway].begin(); ittop!=mmTopology[iGateway].end(); ittop++) {
        int cnode=ittop->first;
        mLasthop[ittop->first]=iGateway;
        mDist[ittop->first]=1;
        mConnected[ittop->first]=0;
        LogFaultTolerantRoute<<ittop->first<<" added into connected set with "<<iGateway<<" as its precessor\n";
        for (itinnertop=mmTopology[cnode].begin(); itinnertop!=mmTopology[cnode].end(); itinnertop++) {
            if (mConnected.count(itinnertop->first)==0) {
                if (mQ.count(itinnertop->first)==0) {
                    mQ[itinnertop->first]=0;
                }
            }
        }
    }
    
    
    while (mQ.size()>0) {
        vector<int> Toberemove;
        for (itmq=mQ.begin(); itmq!=mQ.end(); itmq++) {
            int cnode=itmq->first;
            for (ittop=mmTopology[itmq->first].begin(); ittop!=mmTopology[itmq->first].end(); ittop++) {
                int cneighbor=ittop->first;
                if (mConnected.count(cneighbor)>0) {
                    map<int, map<int, int> > temptopo=mmTopology;
                    temptopo[cnode].erase(cneighbor);
                    temptopo[cneighbor].erase(cnode);
                    map<int, vector<int> > cpath;
                    //LogFaultTolerantRoute<<cnode<<" "<<cneighbor<<"\n";
                    LogFaultTolerantRoute.flush();
                    if (FindDijkstraRoute(cnode, iGateway, temptopo, cpath)==true) {
                        mLasthop[cnode]=cneighbor;
                        mpNodes[cnode]->mmFaultTolerantUplinkRoute[0][0].push_back(cnode);
                        mpNodes[cnode]->mmFaultTolerantUplinkRoute[0][0].push_back(cneighbor);
                        mpNodes[cnode]->mmFaultTolerantUplinkRoute[1]=cpath;
                        mConnected[cnode]=0;
                        mDist[cnode]=mDist[cneighbor]+1;
                        LogFaultTolerantRoute<<cnode<<" <-> "<<cneighbor<<" \n";
                        
                        map<int, vector<int> >::iterator itcpath;
                        for (itcpath=cpath.begin(); itcpath!=cpath.end(); itcpath++) {
                            LogFaultTolerantRoute<<itcpath->second.at(0)<<" -> "<<itcpath->second.at(1)<<", ";
                        }
                        LogFaultTolerantRoute<<"\n";
                        for (itinnertop=mmTopology[cnode].begin(); itinnertop!=mmTopology[cnode].end(); itinnertop++) {
                            if (mConnected.count(itinnertop->first)==0) {
                                mQ[itinnertop->first]=0;
                            }
                        }
                        Toberemove.push_back(cnode);
                        break;
                    }
                }
            }
        }
        if (Toberemove.size()>0) {
            for (int index=0; index<Toberemove.size(); index++) {
                mQ.erase(Toberemove.at(index));
            }
        }
        else {
            cout<<"\n Some node doesn't have fault tolerant route \n\n";
            break;
        }
        
        
        
    }
    
    LogFaultTolerantRoute<<"\n mQ:\n";
    for (itmq=mQ.begin(); itmq!=mQ.end(); itmq++) {
        LogFaultTolerantRoute<<itmq->first<<" ";
    }
    LogFaultTolerantRoute<<"\n mConnected:\n";
    for (itmq=mConnected.begin(); itmq!=mConnected.end(); itmq++) {
        LogFaultTolerantRoute<<itmq->first<<" ";
    }
    
    LogFaultTolerantRoute<<"\n\nNumber of connected nodes : "<<mConnected.size()<<"\n";
    
    bool allconnected=true;
    
    LogFaultTolerantRoute<<"\n mDistance:\n";
    for (itmq=mDist.begin(); itmq!=mDist.end(); itmq++) {
        LogFaultTolerantRoute<<itmq->first<<" "<<itmq->second<<"; ";
        if (itmq->second==INFINITY) {
            allconnected=false;
        }
    }
    return allconnected;
    
}

bool ReliableScheduler::CheckFaultTolerantRoutes_Downlink(int root)
{
    if (root==iGateway) {
        cout<<"Can't be the gateway!";
        exit(12);
    }
    if (IsInVector(vAccessPoints, root)==true) {
        return true;
    }
    
    map<int, int> mDist;
    map<int, int> mLasthop;
    map<int, int> mQ;
    map<int, int>::iterator itmq;
    map<int, int> mConnected;
    
    map<int, map<int, int > >::iterator itTopology;
    map<int, int>::iterator ittop;
    map<int, int>::iterator itinnertop;
    map<int, map<int, float> >::iterator itneighbors;
    
    
    
    
    mDist[root]=0;
    mLasthop[root]=0;
    //mQ.insert(pair<int, int>(iGateway,0));
    
    
    
    for (itTopology=mmTopology.begin(); itTopology!=mmTopology.end(); itTopology++) {
        if (itTopology->first!=root) {
            mDist.insert(pair<int, int>(itTopology->first, INFINITY));
        }
    }
    
    LogFaultTolerantRoute<<"Total number of nodes :  "<<mDist.size()<<"\n";
    
    mConnected[root]=0;
    for (ittop=mmTopology[root].begin(); ittop!=mmTopology[root].end(); ittop++) {
        mQ[ittop->first]=0;
    }
    
    
    while (mQ.size()>0) {
        vector<int> Toberemove;
        for (itmq=mQ.begin(); itmq!=mQ.end(); itmq++) {
            int cnode=itmq->first;
            for (ittop=mmTopology[itmq->first].begin(); ittop!=mmTopology[itmq->first].end(); ittop++) {
                int cneighbor=ittop->first;
                if (mConnected.count(cneighbor)>0) {
                    map<int, map<int, int> > temptopo=mmTopology;
                    temptopo[cnode].erase(cneighbor);
                    temptopo[cneighbor].erase(cnode);
                    map<int, vector<int> > cpath;
                    //LogFaultTolerantRoute<<cnode<<" "<<cneighbor<<"\n";
                    LogFaultTolerantRoute.flush();
                    if (FindDijkstraRoute(cnode, root, temptopo, cpath)==true) {
                        mLasthop[cnode]=cneighbor;
                        mpNodes[root]->mmFaultTolerantDownlinkRoute[cnode]=cpath;
                        mConnected[cnode]=0;
                        mDist[cnode]=mDist[cneighbor]+1;
                        LogFaultTolerantRoute<<cnode<<" <-> "<<cneighbor<<" \n";
                        
                        map<int, vector<int> >::iterator itcpath;
                        for (itcpath=cpath.begin(); itcpath!=cpath.end(); itcpath++) {
                            LogFaultTolerantRoute<<itcpath->second.at(0)<<" -> "<<itcpath->second.at(1)<<", ";
                        }
                        LogFaultTolerantRoute<<"\n";
                        for (itinnertop=mmTopology[cnode].begin(); itinnertop!=mmTopology[cnode].end(); itinnertop++) {
                            if (mConnected.count(itinnertop->first)==0) {
                                mQ[itinnertop->first]=0;
                            }
                        }
                        Toberemove.push_back(cnode);
                        break;
                    }
                }
            }
        }
        if (Toberemove.size()>0) {
            for (int index=0; index<Toberemove.size(); index++) {
                mQ.erase(Toberemove.at(index));
            }
        }
        else {
            if (mConnected.count(iGateway)==1) {
                return true;
            }
            else {
                return false;
            }
            break;
        }
        
        
        
    }
    
//    LogFaultTolerantRoute<<"\n mQ:\n";
//    for (itmq=mQ.begin(); itmq!=mQ.end(); itmq++) {
//        LogFaultTolerantRoute<<itmq->first<<" ";
//    }
//    LogFaultTolerantRoute<<"\n mConnected:\n";
//    for (itmq=mConnected.begin(); itmq!=mConnected.end(); itmq++) {
//        LogFaultTolerantRoute<<itmq->first<<" ";
//    }
//    
//    LogFaultTolerantRoute<<"\n\nNumber of connected nodes : "<<mConnected.size()<<"\n";
//    
//    LogFaultTolerantRoute<<"\n mDistance:\n";
//    for (itmq=mDist.begin(); itmq!=mDist.end(); itmq++) {
//        LogFaultTolerantRoute<<itmq->first<<" "<<itmq->second<<"; ";
//    }
    
    return true;
    
}

void ReliableScheduler::PrintFaultTolerantRoutes()
{
    LogFaultTolerantRoute.clear();
    map<int, map<int, vector<int> > >::iterator itFaultRoute;
    map<int, vector<int> >::iterator itFaultPath;
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        LogFaultTolerantRoute<<"\n\n Node "<<itnodes->first<<":\n";
        LogFaultTolerantRoute<<"Uplink:\n";
        for (itFaultRoute=itnodes->second->mmFaultTolerantUplinkRoute.begin(); itFaultRoute!=itnodes->second->mmFaultTolerantUplinkRoute.end(); itFaultRoute++) {
            LogFaultTolerantRoute<<"\n"<<itFaultRoute->first<<": ";
            for (itFaultPath=itFaultRoute->second.begin(); itFaultPath!=itFaultRoute->second.end(); itFaultPath++) {
                LogFaultTolerantRoute<<itFaultPath->second.at(0)<<" "<<itFaultPath->second.at(1)<<", ";
            }
        }
        LogFaultTolerantRoute<<"\n\nDownlink:\n";
        for (itFaultRoute=itnodes->second->mmFaultTolerantDownlinkRoute.begin(); itFaultRoute!=itnodes->second->mmFaultTolerantDownlinkRoute.end(); itFaultRoute++) {
            LogFaultTolerantRoute<<"\n"<<itFaultRoute->first<<": ";
            for (itFaultPath=itFaultRoute->second.begin(); itFaultPath!=itFaultRoute->second.end(); itFaultPath++) {
                LogFaultTolerantRoute<<itFaultPath->second.at(0)<<" "<<itFaultPath->second.at(1)<<", ";
            }
        }
    }

}




void ReliableScheduler::PrintGLPKPowerRouting()
{
    LogSourceRouting<<"\n\nGLPK Routing:\n\n";
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        if (IsInVector(vAccessPoints, itnodes->first)==true) {
            continue;
        }
        //LogSourceRouting<<"\n"<<itnodes->first<<" : ";
        map<int, map<int, vector<int> > >::iterator itgroute;
        map<int, vector<int> >::iterator itroute;
        
        LogSourceRouting<<"\n"<<itnodes->first<<" : "<<"Uplink Route: \n";
        for (itgroute=itnodes->second->mpDijkstraUplinkRoute.begin(); itgroute!=itnodes->second->mpDijkstraUplinkRoute.end(); itgroute++) {
            LogSourceRouting<<"\nRoute "<<itgroute->first<<" : ";
            for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
                //LogSourceRouting<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<" , ";
                LogSourceRouting<<"("<<itroute->first<<" : "<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<") , ";
                //LogSourceRouting<<itroute->second.at(0)<<" , ";
                
            }
            //LogSourceRouting<<itgroute->second.rbegin()->second.at(1)<<"\n";
        }
        
        LogSourceRouting<<"\n"<<itnodes->first<<" : "<<"GLPK Route: \n";
        for (itgroute=itnodes->second->mpGLPKUplinkRoute.begin(); itgroute!=itnodes->second->mpGLPKUplinkRoute.end(); itgroute++) {
            LogSourceRouting<<"\nRoute "<<itgroute->first<<" : ";
            for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
                //LogSourceRouting<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<" , ";
                LogSourceRouting<<"("<<itroute->first<<" : "<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<") , ";
                //LogSourceRouting<<itroute->second.at(0)<<" , ";
                
            }
            //LogSourceRouting<<itgroute->second.rbegin()->second.at(1)<<"\n";
        }
        
    }
}

void ReliableScheduler::PrintFlowSourceRouting()
{
    //LogFlowRouting<<"Access Points: \n";
    for (int index=0; index<vAccessPoints.size(); index++) {
        LogFlowRouting<<vAccessPoints.at(index)<<" ";
    }
    cout<<"Flow Source Routing!!\n\n";
    LogFlowRouting<<"\n\nSource Routings:\n\n";
    for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
        LogFlowRouting<<"Flow "<<itmpflow->first<<":";
        
        map<int, vector<int> >::iterator itroute;
        
        
        for (itroute=itmpflow->second->mpSourceRoute.begin(); itroute!=itmpflow->second->mpSourceRoute.end(); itroute++) {
            //LogSourceRouting<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<" , ";
            LogFlowRouting<<itroute->second.at(0)<<"->"<<itroute->second.at(1)<<", ";
            //LogSourceRouting<<itroute->second.at(0)<<" , ";
            
        }
        
        //PrintNodeSourceRouting(itmpflow->second->GetSource(), UP);
        //PrintNodeSourceRouting(itmpflow->second->GetDestination(), DOWN);
        LogFlowRouting<<"\n\n";
    }
}

void ReliableScheduler::PrintFlowGraphRouting()
{
    //LogFlowRouting<<"Access Points: ";
    for (int i=0; i<vAccessPoints.size(); i++) {
        LogFlowRouting<<vAccessPoints[i]<<" ";
    }
    LogFlowRouting<<"\n";
    for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
        LogFlowRouting<<"\nFlow "<<itmpflow->first<<":";
        for (int rindex=0; rindex<vRoutes.size(); rindex++) {
            for (int dindex=0; dindex<vDirects.size(); dindex++) {
                LogFlowRouting<<"\n"<<RouteName[vRoutes[rindex]]<<" "<<DirectionName[vDirects[dindex]]<<":";
                PrintGraphRouting(itmpflow->second->mpGraphRoute[vRoutes[rindex]][vDirects[dindex]]);
            }
        }
        LogFlowRouting<<"\n\n";
    }
}

void ReliableScheduler::PrintNodeSourceRouting(int nodeid, DIRECTIONTYPE direction)
{
    map<int, map<int, vector<int> > >::iterator itgroute;
    map<int, vector<int> >::iterator itroute;
    
    //LogSourceRouting<<"\n"<<itnodes->first<<" : "<<"Uplink Route: \n";
    if (direction==UP) {
        itgroute=mpNodes[nodeid]->mpDijkstraUplinkRoute.begin();
        for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
            //LogSourceRouting<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<" , ";
            LogFlowRouting<<itroute->second.at(0)<<"->"<<itroute->second.at(1)<<", ";
            //LogSourceRouting<<itroute->second.at(0)<<" , ";
            
        }
    }
    else if (direction==DOWN){
        itgroute=mpNodes[nodeid]->mpDijkstraDownlinkRoute.begin();
        for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
            //LogSourceRouting<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<" , ";
            LogFlowRouting<<itroute->second.at(0)<<"->"<<itroute->second.at(1)<<", ";
            //LogSourceRouting<<itroute->second.at(0)<<" , ";
            
        }
    }
    else {
        cout<<"Wrong direction!\n\n";
        exit(12);
    }
}

void ReliableScheduler::PrintGraphRouting(map<int, map<int, vector<int> > > GraphRoute)
{
    //map<int, map<int, vector<int> > > GraphRoute;
    map<int, map<int, vector<int> > >::iterator itgroute;
    map<int, vector<int> >::iterator itroute;
    //GraphRoute=mpNodes[nodeid]->getGraphRoute(route, direction);
    for (itgroute=GraphRoute.begin(); itgroute!=GraphRoute.end(); itgroute++) {
        LogFlowRouting<<"\nR "<<itgroute->first<<" : ";
        for (itroute=itgroute->second.begin(); itroute!=itgroute->second.end(); itroute++) {
            LogFlowRouting<<itroute->first<<" , "<<itroute->second.at(0)<<" -> "<<itroute->second.at(1)<<" ; ";
            
        }
    }
}

int ReliableScheduler::NumConnectedNode(int source)
{
    map<int, int> mConnected;
    map<int, int> mCandidate;
    map<int, int> mUnChecked;
    map<int, int>::iterator itcheck;
    
    if (mmPRR.count(source)==0) {
        cout<<"source is not in the topology\n";
        exit(12);
    }
    
    map<int, map<int, map<int, float> > >::iterator itprr;
    
    for (itprr=mmPRR.begin(); itprr!=mmPRR.end(); itprr++) {
        if (itprr->first!=source) {
            mUnChecked.insert(pair<int, int>(itprr->first,0));
        }
    }
    
    mCandidate.insert(pair<int, int>(source,0));
    map<int, map<int, float> >::iterator itneighbors;
    //cout<<"Source is:  " <<source<<"\n";
    
    
    while (mCandidate.size()>0) {
        itcheck=mCandidate.begin();
        int currentnode=itcheck->first;
        for (itneighbors=mmPRR[currentnode].begin(); itneighbors!=mmPRR[currentnode].end(); itneighbors++) {
            if (mConnected.count(itneighbors->first)==0) {
                mCandidate.insert(pair<int, int>(itneighbors->first,0));
                //cout<<"Add "<<itneighbors->first<<" to mCandidate \n";
            }
            
        }
        mConnected.insert(pair<int, int>(currentnode, 0));
        mCandidate.erase(currentnode);
    }
    
    return mConnected.size();
}


void ReliableScheduler:: ReadHanUplinkGraph()
{
	cout<<"\n Creating global Uplink Graph";
	GraphRouteReader.open("GraphRoutes/HanUplinkGraph.txt", ios::in);
	string delimeter("#####################################");
	if(GraphRouteReader.is_open())
	{
		while(!GraphRouteReader.eof())
		{
			int node, parent;
			GraphRouteReader>>node>>parent;
			mvUpLinkGraph[node].push_back(parent);
		}
		cout<<"\n Uplink Graph Done.";
		GraphRouteReader.close();
	}
	else
	{
		cout<<"\n Unable to open uplink.txt file...."<<endl;
		exit(1);
	}
}

//function reads node-->parent format downlink graph for each actuator
void ReliableScheduler:: ReadHanDownlinkGraph()
{
	cout<<"\n Creating Individual Downlink Graphs";
	GraphRouteReader.open("GraphRoutes/HanDownlinkGraph.txt", ios::in);
    if(GraphRouteReader.is_open())
	{
		int nodeID;
		while(!GraphRouteReader.eof())
		{
			string s;
			GraphRouteReader>>s;
            string delimeter("#####################################");
			if(s==delimeter)
			{
				GraphRouteReader>>s>>s>>s>>nodeID>>s;
                if (mpNodes.count(nodeID)==0) {
                    cout<<"No node "<<nodeID<<" exist \n\n";
                    exit(12);
                }
			}
			else
			{
				int node, parent;
				node=atoi(s.c_str()); GraphRouteReader>>parent;
				mpNodes[nodeID]->mvHanDownLinkGraph[node].push_back(parent);
			}
		}
		cout<<"\n Downlink Graphs Done.";
		GraphRouteReader.close();
	}
	else
	{
		cout<<"\n Unable to open downlink.txt file...."<<endl;
		exit(1);
	}
}

void ReliableScheduler::CreateHanGraphRouting(){
    //Uplink
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        map<int, vector<int> > HanGraph;
        HanGraph=mvUpLinkGraph;
        int destination=iGateway;
        int sender=itnodes->first;
        if(IsInVector(vAccessPoints, sender)) continue;
        int primary_hopcount=1;
        int backup_route=0;
        while (sender!=destination) // explore primary route
        {
            int receiver= HanGraph[sender][0];			// 1st parent
            if ((!IsInVector(vAccessPoints, sender) && sender!=iGateway) || (!IsInVector(vAccessPoints, receiver) && receiver!=iGateway)){
                itnodes->second->mpHanUplinkRoute[0][primary_hopcount].push_back(sender);
                itnodes->second->mpHanUplinkRoute[0][primary_hopcount].push_back(receiver);
                if (HanGraph[sender].size()>1) {
                    backup_route=primary_hopcount;
                    int retry_sender=sender; int retry_receiver;
                    retry_sender=sender;
                    retry_receiver=HanGraph[sender][1];
                    int backup_hopcount=1;
                    if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                        itnodes->second->mpHanUplinkRoute[backup_route][backup_hopcount].push_back(retry_sender);
                        itnodes->second->mpHanUplinkRoute[backup_route][backup_hopcount].push_back(retry_receiver);
                        backup_hopcount++;
                    }
                    
                    //retry_receiver=mvUpLinkGraph[sender][1];
                    retry_sender=retry_receiver;
                    while (retry_sender!=destination)
                    {
                        
                        if(HanGraph[retry_sender].size()<=1)	//if there is no second parent
                        {
                            retry_receiver= HanGraph[retry_sender][0];// send to the only parent
                            if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                                itnodes->second->mpHanUplinkRoute[backup_route][backup_hopcount].push_back(retry_sender);
                                itnodes->second->mpHanUplinkRoute[backup_route][backup_hopcount].push_back(retry_receiver);
                                backup_hopcount++;
                            }
                        }
                        else
                        {
                            retry_receiver= HanGraph[retry_sender][1];// send to the only parent
                            if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                                itnodes->second->mpHanUplinkRoute[backup_route][backup_hopcount].push_back(retry_sender);
                                itnodes->second->mpHanUplinkRoute[backup_route][backup_hopcount].push_back(retry_receiver);
                                backup_hopcount++;
                            }
                        }
                        retry_sender=retry_receiver;
                    }
                }
                primary_hopcount++;
            }
            
            
            
            
            
            sender=receiver; //next sender
            
        }
    }
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        map<int, vector<int> > HanGraph;
        HanGraph=itnodes->second->mvHanDownLinkGraph;
        int destination=itnodes->first;
        int sender=iGateway;
        if(IsInVector(vAccessPoints, itnodes->first)) continue;
        int backup_route=0;
        int primary_hopcount=1;
        while (sender!=destination) // explore primary route
        {
            int receiver= HanGraph[sender][0];			// 1st parent
            if ((!IsInVector(vAccessPoints, sender) && sender!=iGateway) || (!IsInVector(vAccessPoints, receiver) && receiver!=iGateway)){
                itnodes->second->mpHanDownlinkRoute[0][primary_hopcount].push_back(sender);
                itnodes->second->mpHanDownlinkRoute[0][primary_hopcount].push_back(receiver);
                if (HanGraph[sender].size()>1) {
                    backup_route=primary_hopcount;
                    int retry_sender=sender; int retry_receiver;
                    retry_sender=sender;
                    retry_receiver=HanGraph[sender][1];
                    int backup_hopcount=1;
                    if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                        itnodes->second->mpHanDownlinkRoute[backup_route][backup_hopcount].push_back(retry_sender);
                        itnodes->second->mpHanDownlinkRoute[backup_route][backup_hopcount].push_back(retry_receiver);
                        backup_hopcount++;
                    }
                    //retry_receiver=mvUpLinkGraph[sender][1];
                    retry_sender=retry_receiver;
                    while (retry_sender!=destination)
                    {
                        
                        if(HanGraph[retry_sender].size()<=1)	//if there is no second parent
                        {
                            retry_receiver= HanGraph[retry_sender][0];// send to the only parent
                            if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                                itnodes->second->mpHanDownlinkRoute[backup_route][backup_hopcount].push_back(retry_sender);
                                itnodes->second->mpHanDownlinkRoute[backup_route][backup_hopcount].push_back(retry_receiver);
                                backup_hopcount++;
                            }
                        }
                        else
                        {
                            retry_receiver= HanGraph[retry_sender][1];// send to the only parent
                            if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                                itnodes->second->mpHanDownlinkRoute[backup_route][backup_hopcount].push_back(retry_sender);
                                itnodes->second->mpHanDownlinkRoute[backup_route][backup_hopcount].push_back(retry_receiver);
                                backup_hopcount++;
                            }
                        }
                        retry_sender=retry_receiver;
                    }
                }
                primary_hopcount++;
            }
            
            
            
            
            sender=receiver; //next sender
            
        }
    }
}

void ReliableScheduler::FindHanGraphRoute(int source, int destination, vector<vector<int> > vvHanRoute, map<int, map<int, vector<int> > > &Groute)
{
    map<int, vector<int> > HanGraph;
    for (int i=0; i<vvHanRoute.size(); i++) {
        if (vvHanRoute[i].size()<2) {
            cout<<"\n\nWrong vvHanRoute size, only one node in the link \n\n";
            exit(55);
        }
        int sender;
        for (int j=0; j<vvHanRoute[i].size(); j++) {
            if (j==0) {
                sender=vvHanRoute[i][j];
            }
            else{
                HanGraph[sender].push_back(vvHanRoute[i][j]);
            }
            
        }
    }
    
    LogHanGraph<<"\n\n mvHanGraph: \n";
    map<int, vector<int> >::iterator ithangraph;
    for (ithangraph=HanGraph.begin(); ithangraph!=HanGraph.end(); ithangraph++) {
        LogHanGraph<<ithangraph->first<<": ";
        for (int i=0; i<ithangraph->second.size(); i++) {
            LogHanGraph<<ithangraph->second.at(i)<<" ";
        }
        LogHanGraph<<"\n";
    }
    LogHanGraph.flush();
    int sender=source;
    int primary_hopcount=1;
    int backup_route=0;
    while (sender!=destination) // explore primary route
    {
        int receiver= HanGraph[sender][0];			// 1st parent
        if ((!IsInVector(vAccessPoints, sender) && sender!=iGateway) || (!IsInVector(vAccessPoints, receiver) && receiver!=iGateway)){
            Groute[0][primary_hopcount].push_back(sender);
            Groute[0][primary_hopcount].push_back(receiver);
            if (HanGraph[sender].size()>1) {
                backup_route=primary_hopcount;
                int retry_sender=sender; int retry_receiver;
                retry_sender=sender;
                retry_receiver=HanGraph[sender][1];
                int backup_hopcount=1;
                if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                    Groute[backup_route][backup_hopcount].push_back(retry_sender);
                    Groute[backup_route][backup_hopcount].push_back(retry_receiver);
                    backup_hopcount++;
                }
                
                //retry_receiver=mvUpLinkGraph[sender][1];
                retry_sender=retry_receiver;
                while (retry_sender!=destination)
                {
                    
                    if(HanGraph[retry_sender].size()<=1)	//if there is no second parent
                    {
                        retry_receiver= HanGraph[retry_sender][0];// send to the only parent
                        if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                            Groute[backup_route][backup_hopcount].push_back(retry_sender);
                            Groute[backup_route][backup_hopcount].push_back(retry_receiver);
                            backup_hopcount++;
                        }
                    }
                    else
                    {
                        retry_receiver= HanGraph[retry_sender][1];// send to the only parent
                        if ((!IsInVector(vAccessPoints, retry_sender) && retry_sender!=iGateway) || (!IsInVector(vAccessPoints, retry_receiver) && retry_receiver!=iGateway)){
                            Groute[backup_route][backup_hopcount].push_back(retry_sender);
                            Groute[backup_route][backup_hopcount].push_back(retry_receiver);
                            backup_hopcount++;
                        }
                    }
                    retry_sender=retry_receiver;
                }
            }
            primary_hopcount++;
        }
        
        
        
        
        
        sender=receiver; //next sender
        
    }
    //Uplink
}

bool ReliableScheduler::GenerateHanGraphRoute(int source, int root,  vector<vector<int> > &vvHanRoute){
    
    map<int, int> mDist;
    map<int, int> mLasthop;
    map<int, int> mQ;
    map<int, int>::iterator itmq;
    map<int, float> mConnected;
    map<int, float>::iterator itcon;
    map<int, float>::iterator itif;
    map<int, int> mNeighbors;
    //int Bigdistance=500;
    
    map<int, map<int, int > >::iterator itTopology;
    map<int, int>::iterator ittop;
    map<int, int>::iterator itinertop;
    map<int, int>::iterator itneighbors;
    //int source;

    mConnected[root]=0;
    
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        itnodes->second->mConnectedNeighbors.clear();
        if (mConnected.count(itnodes->first)==0) {
            mQ.insert(pair<int, int>(itnodes->first,0));
        }
    }
    
    
    for (ittop=mmTopology[root].begin(); ittop!=mmTopology[root].end(); ittop++) {
        int cnode=ittop->first;
        mpNodes[ittop->first]->mConnectedNeighbors[root]=0;
        mConnected[ittop->first]=1;
        mQ.erase(ittop->first);
        if (mNeighbors.count(cnode)==1) {
            mNeighbors.erase(cnode);
        }
        if (ittop->first==source) {
            break;
        }
        for (itinertop=mmTopology[cnode].begin(); itinertop!=mmTopology[cnode].end(); itinertop++) {
            if (mConnected.count(itinertop->first)==0) {
                if (mNeighbors.count(itinertop->first)==0) {
                    mNeighbors[itinertop->first]=0;
                }
            }
            
        }
    }
    
    while (mQ.size()>0) {
        //LogHanGraph<<"mConnected: ";
        for (itcon=mConnected.begin(); itcon!=mConnected.end(); itcon++) {
            //LogHanGraph<<itcon->first<<" ";
        }
        //LogHanGraph<<"\n";
        //cout<<"\n\n mQ size: "<<mQ.size()<<" mConnected size "<<mConnected.size()<<" \n\n";
        //LogHanGraph<<"\n\n mQ size: "<<mQ.size()<<" mConnected size "<<mConnected.size()<<" \n\n";
        //Check neighbors of mNeibors and there hopcount
        if (mNeighbors.size()==0) {
            break;
        }
        for (itneighbors=mNeighbors.begin(); itneighbors!=mNeighbors.end(); itneighbors++) {
            int cnode=itneighbors->first;
            mpNodes[cnode]->mConnectedNeighbors.clear();
            for (ittop=mmTopology[cnode].begin(); ittop!=mmTopology[cnode].end(); ittop++) {
                if (mConnected.count(ittop->first)==1) {
                    mpNodes[cnode]->mConnectedNeighbors[ittop->first]=mConnected[ittop->first];
                }
            }
        }
        //pick up the candidate with smallest hopcount with at least two connections to the mConnected
        int nodeid=-1;
        multimap<float, int> mCandidates;
        multimap<float, int>::iterator itcand;
        for (itneighbors=mNeighbors.begin(); itneighbors!=mNeighbors.end(); itneighbors++) {
            int cnode=itneighbors->first;
            if (mpNodes[cnode]->mConnectedNeighbors.size()>=2) {
                multimap<float, int> mhop;
                multimap<float, int>::iterator itmhop;
                for (itif=mpNodes[cnode]->mConnectedNeighbors.begin(); itif!=mpNodes[cnode]->mConnectedNeighbors.end(); itif++) {
                    mhop.insert(pair<float, int>(itif->second,itif->first));
                }
                itmhop=mhop.begin();
                float count1=itmhop->first;
                float count2=(++itmhop)->first;
                float hpcount=(count1+count2)/2+1;
                mCandidates.insert(pair<float, int>(hpcount,cnode));
            }
        }
        if (mCandidates.size()>0) {
            itcand=mCandidates.begin();
            nodeid=itcand->second;
            mConnected.insert(pair<int, float>(itcand->second,itcand->first));
            if (nodeid==source) {
                break;
            }
            mQ.erase(nodeid);
            mNeighbors.erase(nodeid);
            for (itinertop=mmTopology[nodeid].begin(); itinertop!=mmTopology[nodeid].end(); itinertop++) {
                if (mConnected.count(itinertop->first)==0) {
                    if (mNeighbors.count(itinertop->first)==0) {
                        mNeighbors[itinertop->first]=0;
                    }
                }
                
            }
        }
        else {
            for (itneighbors=mNeighbors.begin(); itneighbors!=mNeighbors.end(); itneighbors++) {
                int cnode=itneighbors->first;
                if (mpNodes[cnode]->mConnectedNeighbors.size()>=1) {
                    itif=mpNodes[cnode]->mConnectedNeighbors.begin();
                    float hpcount=itif->second+1;
                    mCandidates.insert(pair<float, int>(hpcount,cnode));
                }
            }
            if (mCandidates.size()>0) {
                itcand=mCandidates.begin();
                nodeid=itcand->second;
                mConnected.insert(pair<int, float>(itcand->second,itcand->first));
                if (nodeid==source) {
                    break;
                }
                mQ.erase(nodeid);
                mNeighbors.erase(nodeid);
                for (itinertop=mmTopology[nodeid].begin(); itinertop!=mmTopology[nodeid].end(); itinertop++) {
                    if (mConnected.count(itinertop->first)==0) {
                        if (mNeighbors.count(itinertop->first)==0) {
                            mNeighbors[itinertop->first]=0;
                        }
                    }
                    
                }
            }
        }
    }
    for (itcon=mConnected.begin(); itcon!=mConnected.end(); itcon++) {
        if (itcon->first==root) {
            continue;
        }
        if (mpNodes[itcon->first]->mConnectedNeighbors.size()==1) {
            vector<int> temp;
            temp.push_back(itcon->first);
            temp.push_back(mpNodes[itcon->first]->mConnectedNeighbors.begin()->first);
            vvHanRoute.push_back(temp);
        }
        else {
            int cnode=itcon->first;
            multimap<float, int> mhop;
            multimap<float, int>::iterator itmhop;
            for (itif=mpNodes[cnode]->mConnectedNeighbors.begin(); itif!=mpNodes[cnode]->mConnectedNeighbors.end(); itif++) {
                mhop.insert(pair<float, int>(itif->second,itif->first));
            }
            itmhop=mhop.begin();
            vector<int> temp;
            temp.push_back(itcon->first);
            temp.push_back(itmhop->second);
            vvHanRoute.push_back(temp);
            temp.clear();
            itmhop++;
            temp.push_back(itcon->first);
            temp.push_back(itmhop->second);
            vvHanRoute.push_back(temp);
        }
    }
    
    return true;
}

void ReliableScheduler:: GenerateFlows(int numflows, bool whethersingleperiod, bool TestCase)
{
    if (TestCase) {
        numflows=1;
    }

	mpFlows.clear();
	cout<<"\n Creating flows in the network........";
	Log<<"\n **********************  Flows *********************";
	Log<<"\n ID    Sensor    Actuator    Period      DeadLine      Emergency      Criticality";
    
    FlowLog<<"\n **********************  Flows *********************";
	FlowLog<<"\n ID    Sensor    Actuator    Period      DeadLine      Emergency      Criticality	";
    
    int FlowID=0;
    map<int, vector<int> > mpSensorActuator;
    map<int, vector<int> >::iterator itmpsa;
    
    int numnodes=mpNodes.size();
    int numaccess=vAccessPoints.size();
    int numsa=numnodes-numaccess;
    
    if (numflows>numsa/2) {
        cout<<"Number of flows larger than number of node pairs\n\n";
        exit(13);
    }
    if (TestCase) {
        mpSensorActuator[1].push_back(5);
        mpSensorActuator[1].push_back(4);
    }
    else {
        vSensors.clear();
        vActuators.clear();
        int nodeindex=0;
        map<int, Node*>::reverse_iterator itrenode;
        
        multimap<int, int> mmHopCount;
        multimap<int, int>::iterator ithopcount;
        for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
            if (IsInVector(vAccessPoints, itnodes->first)) {
                continue;
            }
            mmHopCount.insert(pair<int, int>(itnodes->second->SourceRoute.size(),itnodes->first));
        }
        
//        for (ithopcount=mmHopCount.begin(); ithopcount!=mmHopCount.end(); ithopcount++) {
//            if (IsInVector(vAccessPoints, ithopcount->second)) {
//                continue;
//            }
//            if (nodeindex%2==0) {
//                vSensors.push_back(ithopcount->second);
//            }
//            else {
//                vActuators.push_back(ithopcount->second);
//            }
//            nodeindex++;
//        }
        
        for (itrenode=mpNodes.rbegin(); itrenode!=mpNodes.rend(); itrenode++) {
            if (IsInVector(vAccessPoints, itrenode->first)) {
                continue;
            }
//            if (itrenode->first==186 || itrenode->first==187) {
//                continue;
//            }
            if (nodeindex%2==0) {
                vSensors.push_back(itrenode->first);
            }
            else {
                vActuators.push_back(itrenode->first);
            }
            nodeindex++;
        }
        
        
        
        
        int numSApairs=min(vSensors.size(), vActuators.size());
        if (numflows>numSApairs) {
            cout<<"Number of flows larger than number of node pairs\n\n";
            exit(13);
        }
        
        bool tryallpairs=false;
        
        if (tryallpairs==false) {
            for (int index=1; index<=numflows; index++) {
                mpSensorActuator[index].push_back(vSensors.at(index));
                mpSensorActuator[index].push_back(vActuators.at(index));
            }
        }
        else {
            for (int index=1; index<=numSApairs; index++) {
                mpSensorActuator[index].push_back(vSensors.at(index));
                mpSensorActuator[index].push_back(vActuators.at(index));
            }
        }
    }
    
    for (itmpsa=mpSensorActuator.begin(); itmpsa!=mpSensorActuator.end(); itmpsa++) {
        
        Flow *flow; //int ID, int src, int dest, int period, int deadline, vector <int> path
		int source, destination;
		source= itmpsa->second.at(0);
        FlowID=itmpsa->first;
		destination=itmpsa->second.at(1);
		//int period=pow(2, exp1+rand()%(exp2-exp1+1));
        //int period=aPeriod[FlowID];
        //int period=120;
        int period;
        if (whethersingleperiod) {
            period=100*pow(2,5);
        }
        else {
            //int seed=rand() % exp2 +exp1;
            //period=pow(2, seed);
            //period=100*pow(2, seed);
            period=100*pow(2, (itmpsa->first+1)/4+2);
            //period=100*pow(2, itmpsa->first);
        }
        
        //mpNodes[source]->rate=(100*pow(2, 7)/period);
        //int period=100*(1+itmpsa->first);
        
        //FlowID++;
        CRITTYPE pcrit;
        bool Emergency;
        if (itmpsa->first%2==0) {
            Emergency=true;
            pcrit=HIGH;
            //period=regularperiod;
        }
        else {
            Emergency=false;
            pcrit=LOW;
            //period=regularperiod;
        }
        
        if (LCM<period) {
            LCM=period;
        }
		flow = new Flow(FlowID, source, destination, period, period, Emergency, pcrit, inumtrace);
		mpFlows.insert(pair<int, Flow*>(FlowID, flow ));
        
        
        
        char Etype[]={'R', 'E'};
		Log<<"\n  "<<FlowID<<"      "<<source<<"      "<<destination<<"          "<<period<<"          "<<period<<"         "<<Etype[Emergency]<<"      "<<vCritType[pcrit]<<"\n";
        FlowLog<<"\n  "<<FlowID<<"      "<<source<<"      "<<destination<<"          "<<period<<"          "<<period<<"         "<<Etype[Emergency]<<"      "<<vCritType[pcrit]<<"\n";
    }
	cout<<"\n All flows Created!";
    
}

void ReliableScheduler:: GenerateFlows_DisjointRoutes(int numflows, bool whethersingleperiod)
{
	mpFlows.clear();
	cout<<"\n Creating flows in the network........";
	Log<<"\n **********************  Flows *********************";
	Log<<"\n ID    Sensor    Actuator    Period      DeadLine      Emergency      Criticality";
    
    FlowLog<<"\n **********************  Flows *********************";
	FlowLog<<"\n ID    Sensor    Actuator    Period      DeadLine      Emergency      Criticality	";
    
    
    map<int, vector<int> > mpSensorActuator;
    map<int, vector<int> >::iterator itmpsa;
    
    vSensors.clear();
    vActuators.clear();
    
    int nodeindex=0;
    for (itnodes=mpNodes.begin(); itnodes!=mpNodes.end(); itnodes++) {
        if (itnodes->second->mpDisjontPaths.size()>1 && IsInVector(vAccessPoints, itnodes->first)==false) {
            if (nodeindex%2==0) {
                vSensors.push_back(itnodes->first);
            }
            else {
                vActuators.push_back(itnodes->first);
            }
            nodeindex++;
        }
    }
    int numSApairs=min(vSensors.size(), vActuators.size());
    
    if (numflows>numSApairs) {
        cout<<"Number of flows larger than number of node pairs\n\n";
        exit(13);
    }
    
    bool tryallpairs=false;
    
    if (tryallpairs==false) {
        for (int index=0; index<numflows; index++) {
            mpSensorActuator[index].push_back(vSensors.at(index));
            mpSensorActuator[index].push_back(vActuators.at(index));
        }
    }
    else {
        for (int index=0; index<numSApairs; index++) {
            mpSensorActuator[index].push_back(vSensors.at(index));
            mpSensorActuator[index].push_back(vActuators.at(index));
        }
    }
    
    for (itmpsa=mpSensorActuator.begin(); itmpsa!=mpSensorActuator.end(); itmpsa++) {
        
        
		int source, destination;
		source= itmpsa->second.at(0);
        
        
		destination=itmpsa->second.at(1);
		//int period=pow(2, exp1+rand()%(exp2-exp1+1));
        //int period=aPeriod[FlowID];
        //int period=120;
        int period;
        period=100*pow(2, itmpsa->first/2);
        
        //period=100*(1+itmpsa->first);
        
        //FlowID++;
        CRITTYPE pcrit;
        bool Emergency;
        if (itmpsa->first%2==0) {
            Emergency=true;
            pcrit=HIGH;
            //period=regularperiod;
        }
        else {
            Emergency=false;
            pcrit=LOW;
            //period=regularperiod;
        }
        
        if (LCM<period) {
            LCM=period;
        }
        
        map<int, int> mpNewFlows;
        int Masterid=itmpsa->first;
        mpNewFlows[0]=2*Masterid;
        mpNewFlows[1]=2*Masterid+1;
        
        map<int, int>::iterator itnewflow;
        for (itnewflow=mpNewFlows.begin(); itnewflow!=mpNewFlows.end(); itnewflow++) {
            Flow *flow; //int ID, int src, int dest, int period, int deadline, vector <int> path
            int FlowID=itnewflow->second;
            flow = new Flow(FlowID, source, destination, period, period, Emergency, pcrit, inumtrace);
            mpFlows.insert(pair<int, Flow*>(FlowID, flow ));
            flow->SetMasterID(Masterid);
            
            map<int, vector<int> > uplinkroute;
            map<int, vector<int> > downlinkroute;
            
            uplinkroute=mpNodes[source]->mpUplinkDisjointRoutes[itnewflow->first];
            downlinkroute=mpNodes[destination]->mpDownlinkDisjointRoutes[itnewflow->first];
            
            map<int, vector<int> > singleroute;
            map<int, vector<int> > sourseroute;
            map<int, vector<int> >::iterator itroute;
            
            singleroute=uplinkroute;
            
            
            int hopcout=0;
            for (itroute=singleroute.begin(); itroute!=singleroute.end(); itroute++) {
                sourseroute[hopcout]=itroute->second;
                hopcout++;
            }
            singleroute=downlinkroute;
            for (itroute=singleroute.begin(); itroute!=singleroute.end(); itroute++) {
                sourseroute[hopcout]=itroute->second;
                hopcout++;
            }
            
            flow->mpSourceRoute=sourseroute;
            flow->mpGraphRoute[DIJKSTRA][UP][0]=uplinkroute;
            flow->mpGraphRoute[DIJKSTRA][DOWN][0]=downlinkroute;
            
            
            char Etype[]={'R', 'E'};
            Log<<"\n  "<<FlowID<<"      "<<source<<"      "<<destination<<"          "<<period<<"          "<<period<<"         "<<Etype[Emergency]<<"      "<<vCritType[pcrit]<<"\n";
            FlowLog<<"\n  "<<FlowID<<"      "<<source<<"      "<<destination<<"          "<<period<<"          "<<period<<"         "<<Etype[Emergency]<<"      "<<vCritType[pcrit]<<"\n";
        }
        
        
    }
	cout<<"\n All flows Created!";
    
}


bool ReliableScheduler:: GenerateSchedule(bool TestPassed, APPROACH iapproach, ROUTETYPE iroute)
{
    cout<<"\n\n++++++++++++++++++++++++  "<<APPtype[iapproach]<<"  Simulation   ++++++++++++++++++++++++\n\n\n ";
    mmSchedule.clear();
    map<int, int>:: iterator hit,  pit= f.end();
    int superframe_length=1;
    for(pit=f.begin();  pit!=f.end(); pit++)			//start from the highest priority
    {
        int currentflow=pit->second; int currentperiod= mpFlows[currentflow]->GetPeriod();
        //cout<<"\n Scheduling Flow "<<currentflow;
        mpFlows[currentflow]->SetSimulationEndToEndDelay(0);	//reset for new data
        mpFlows[currentflow]->SetSIMSensingDelay(0);
        if(currentperiod>=superframe_length) superframe_length=currentperiod;
        int jobs_in_superframe= (superframe_length/ currentperiod); 	//schedule only upto superframe lengt, then repeat uptp LCM
        for(int i=0; i<jobs_in_superframe; i++)				//schedule every instance on  available time slots in the current superframe
        {
            if(schedule(currentflow, i, superframe_length, TestPassed, iapproach, iroute)==false)
            {
                if(TestPassed==true)
                {
                    cout<<"\n Fatal error! Test passed but simulaton failed:  "<<f.size();
                    
                    cout<<"\n flow:<<"<<currentflow<<":   simDelay"<<mpFlows[currentflow]->GetSimulationEndToEndDelay()<<"      deadline: "<<mpFlows[currentflow]->GetDeadLine()<<"\n  Analy: "<<mpFlows[currentflow]->GetAnalyticalEndToEndDelay();
                    
                    //exit(1);
                }
                return false;
            }
        }
    }
    return true;
    
    
}



bool ReliableScheduler:: schedule(int flow, int job, int framelength, bool TestPassed, APPROACH iapproach, ROUTETYPE iroute)// schedules one instance
{
	int period= mpFlows[flow]->GetPeriod();
	int releasetime = job*period+1;
	//int deadline= releasetime + mpFlows[flow]->GetDeadLine()  - 1;
	int deadline= mpFlows[flow]->GetDeadLine();		//relative
    int delay;
    
    
    map<int, map<int, vector<int> > > mpUpRoute=mpFlows[flow]->mpGraphRoute[iroute][UP];
    map<int, map<int, vector<int> > > mpDownRoute=mpFlows[flow]->mpGraphRoute[iroute][DOWN];;
    int delaysensingphase=SchedulePhase(flow, releasetime, framelength, mpUpRoute, SENSING, iapproach);
    if (delaysensingphase>mpFlows[flow]->GetSIMSensingDelay()) {
        mpFlows[flow]->SetSIMSensingDelay(delaysensingphase);
    }
    if(delaysensingphase>deadline)
    {
        cout<<"\n flow:<<"<<flow<<":   Delay in sensing phase: "<<delaysensingphase<<"      deadline: "<<deadline;
        cout<<"\n Total flows: "<<f.size();
        return false;
    }
    delay= delaysensingphase+SchedulePhase(flow, releasetime+delaysensingphase, framelength, mpDownRoute, CONTROL, iapproach);
    //delay=delaysensingphase;
    if(delay>	mpFlows[flow]->GetSimulationEndToEndDelay()){
        mpFlows[flow]->SetSimulationEndToEndDelay(delay);
    }
    
    
	
	if(delay>deadline)
	{
		cout<<"\n flow:<<"<<flow<<":   Delay"<<delay<<"      deadline: "<<deadline<<"\n  Sim: "<<mpFlows[flow]->GetSimulationEndToEndDelay();
        cout<<"\n Flow "<<flow<<" can not be scheduled !!!!!!!\n";
		cout<<"\n Total flows: "<<f.size();
		return false; 
	}
	return true;
}


int ReliableScheduler::SchedulePhase(int flow, int starttime, int framelength, map<int, map<int, vector<int> > > mpRoute, PHASETYPE phase,  APPROACH iapproach)
{
    if (iapproach==POLLING){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        access=DEDICATED;
        
        //cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, int> PrimaryPathScheduleHopCount;   //record when is the second dedicated trans scheduled [hopcount][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
            //cout<<"Route ID "<<itackpath->first<<"\n";
            if (itackpath->first==0) {
                slot1=starttime-1;
            }
            else {
                //cout<<itackpath->second.begin()->second[0]<<"\n";
                if (PrimaryPathSchedule.count(itackpath->second.begin()->second[0])>=1) {
                    slot1=PrimaryPathSchedule.at(itackpath->second.begin()->second[0]);
                }
                else{
                    slot1=PrimaryPathScheduleHopCount[0];
                }
                
            }
            
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                //cout<<"Slot 1: "<<slot1<<"\n";
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                hopcount=itacktrans->first;
//                if (itackpath->first<=1) {
//                    hopcount=itacktrans->first;
//                }
//                else {
//                    hopcount=itacktrans->first+itackpath->first-1;
//                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
                if (itackpath->first==0) {
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first);
                    PrimaryPathSchedule[sender]=slot1;
                    PrimaryPathScheduleHopCount[itacktrans->first]=slot1;
                }
            }
            
            
        }
        return delay;
    }
    else if (iapproach==MCDEDI){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        access=DEDICATED;
        
        //cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        if (mpFlows[flow]->GetCrit()==LOW) {
            slot1=starttime-1;				//dedicated
            map<int, map<int, vector<int> > >::iterator itackpath;
            map<int, vector<int> >::iterator itacktrans;
            itackpath=mpRoute.begin();
            slot1=starttime-1;
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first,  hopcount);// false meanse dedicated
                
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, hopcount);
            }
            return delay;
        }
        else if (mpFlows[flow]->GetCrit()==HIGH){
            for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
                //cout<<"Route ID "<<itackpath->first<<"\n";
                if (itackpath->first==0) {
                    slot1=starttime-1;
                }
                else {
                    slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
                }
                
                for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                    sender=itacktrans->second.at(0);
                    receiver=itacktrans->second.at(1);
                    //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                    //cout<<"Slot 1: "<<slot1<<"\n";
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    int hopcount;
                    if (itackpath->first<=1) {
                        hopcount=itacktrans->first+1;
                    }
                    else {
                        hopcount=itacktrans->first+itackpath->first;
                    }
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                    
                    if (itackpath->first==0) {
                        slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                        if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                        if(delay>deadline) 	return deadline+2;
                        ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                        PrimaryPathSchedule[sender]=slot1;
                    }
                }
                
                
            }
            return delay;
        }
        else {
            cout<<"\n\n Wrong Criticality Level!\n\n";
            exit(12);
        }
    }
    else if (iapproach==MCFLAT){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        //cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        if (mpFlows[flow]->GetCrit()==LOW) {
            access=DEDICATED;
            slot1=starttime-1;				//dedicated
            map<int, map<int, vector<int> > >::iterator itackpath;
            map<int, vector<int> >::iterator itacktrans;
            itackpath=mpRoute.begin();
            slot1=starttime-1;
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, hopcount);
            }
            return delay;
        }
        else if (mpFlows[flow]->GetCrit()==HIGH){
            for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
                //cout<<"Route ID "<<itackpath->first<<"\n";
                if (itackpath->first==0) {
                    slot1=starttime-1;
                    access=DEDICATED;
                }
                else {
                    slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
                    access=SHARED;
                }
                
                for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                    sender=itacktrans->second.at(0);
                    receiver=itacktrans->second.at(1);
                    //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                    //cout<<"Slot 1: "<<slot1<<"\n";
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    int hopcount;
                    if (itackpath->first<=1) {
                        hopcount=itacktrans->first+1;
                    }
                    else {
                        hopcount=itacktrans->first+itackpath->first;
                    }
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                    
                    if (itackpath->first==0) {
                        slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                        if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                        if(delay>deadline) 	return deadline+2;
                        ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                        PrimaryPathSchedule[sender]=slot1;
                    }
                }
            }
            return delay;
        }
        else {
            cout<<"\n\n Wrong Criticality Level!\n\n";
            exit(12);
        }
    }
    else if (iapproach==MCPRIO){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        //cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        if (mpFlows[flow]->GetCrit()==LOW) {
            access=SECONDRY;
            slot1=starttime-1;				//dedicated
            map<int, map<int, vector<int> > >::iterator itackpath;
            map<int, vector<int> >::iterator itacktrans;
            itackpath=mpRoute.begin();
            slot1=starttime-1;
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, hopcount);
            }
            return delay;
        }
        else if (mpFlows[flow]->GetCrit()==HIGH){
            for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
                //cout<<"Route ID "<<itackpath->first<<"\n";
                if (itackpath->first==0) {
                    slot1=starttime-1;
                    access=DEDICATED;
                }
                else {
                    slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
                    access=PRIMARY;
                }
                
                for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                    sender=itacktrans->second.at(0);
                    receiver=itacktrans->second.at(1);
                    //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                    //cout<<"Slot 1: "<<slot1<<"\n";
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    int hopcount;
                    if (itackpath->first<=1) {
                        hopcount=itacktrans->first+1;
                    }
                    else {
                        hopcount=itacktrans->first+itackpath->first;
                    }
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                    
                    if (itackpath->first==0) {
                        slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                        if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                        if(delay>deadline) 	return deadline+2;
                        ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                        PrimaryPathSchedule[sender]=slot1;
                    }
                }
            }
            return delay;
        }
        else {
            cout<<"\n\n Wrong Criticality Level!\n\n";
            exit(12);
        }
    }
    
    else if (iapproach==DIVIDE){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        access=DEDICATED;
        
        //cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
            //cout<<"Route ID "<<itackpath->first<<"\n";
            if (itackpath->first==0) {
                slot1=starttime-1;
            }
            else {
                slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
            }
            
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                //cout<<"Slot 1: "<<slot1<<"\n";
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                PrimaryPathSchedule[sender]=slot1;
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
            }
            
            
        }
        return delay;
    }
    else if (iapproach==SOURCE){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        access=DEDICATED;
        
        //cout<<"\nScheduling "<<Ptype[iphase]<< " part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        itackpath=mpRoute.begin();
        
        slot1=starttime-1;
        
        for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
            sender=itacktrans->second.at(0);
            receiver=itacktrans->second.at(1);
            //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
            slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
            //cout<<"Slot 1: "<<slot1<<"\n";
            if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
            if(delay>deadline) 	return deadline+2;
            int hopcount;
            if (itackpath->first<=1) {
                hopcount=itacktrans->first+1;
            }
            else {
                hopcount=itacktrans->first+itackpath->first;
            }
            ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
            
            if (itackpath->first==0) {
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                
            }
        }
        return delay;
    }
    else if (iapproach==BACKUP){
        PHASETYPE iphase=phase;
        ACCESSTYPE access;
        cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
            //cout<<"Route ID "<<itackpath->first<<"\n";
            if (itackpath->first==0) {
                slot1=starttime-1;
                access=DEDICATED;
            }
            else {
                slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
                access=SHARED;
            }
            
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                //cout<<"Slot 1: "<<slot1<<"\n";
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
                if (itackpath->first==0) {
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                    PrimaryPathSchedule[sender]=slot1;
                }
            }
            
            
        }
        return delay;
    }
    else if (iapproach==SHARBACKUP){
        PHASETYPE iphase=SENSING;
        ACCESSTYPE access;
        cout<<"\nScheduling sensing part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        map<int, map<int, vector<int> > > UplinkRoute=mpFlows[flow]->mpGraphRoute[DIJKSTRA][UP];
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        for (itackpath=UplinkRoute.begin(); itackpath!=UplinkRoute.end(); itackpath++) {
            //cout<<"Route ID "<<itackpath->first<<"\n";
            if (itackpath->first==0) {
                slot1=starttime-1;
                if (mpFlows[flow]->GetEmergency()==true) {
                    access=DEDICATED;
                }
                else {
                    access=SECONDRY;
                }
            }
            else {
                slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
                access=SHARED;
            }
            
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                //cout<<"Slot 1: "<<slot1<<"\n";
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
                if (itackpath->first==0) {
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                    PrimaryPathSchedule[sender]=slot1;
                }
            }
            
            
        }
        return delay;
        
        
    }
    else {
        PHASETYPE iphase=SENSING;
        ACCESSTYPE access;
        if (iapproach==SHARING ||ACKALL) {
            if (mpFlows[flow]->GetEmergency()==true) {
                access=PRIMARY;
            }
            else {
                access=SECONDRY;
            }
        }
        cout<<"\nScheduling"<<Ptype[iphase]<< "part of flow "<<flow<<"\n";
        
        int delay=0;
        int slot1, channel, sender, receiver;
        int deadline= mpFlows[flow]->GetDeadLine();		//relative
        //if(IsInVector(vAccessPoints, sender)) return 0;
        slot1=starttime-1;				//dedicated
        
        map<int, int> PrimaryPathSchedule;   //record when is the second dedicated trans scheduled [sender][time slot]
        map<int, map<int, vector<int> > >::iterator itackpath;
        map<int, vector<int> >::iterator itacktrans;
        for (itackpath=mpRoute.begin(); itackpath!=mpRoute.end(); itackpath++) {
            //cout<<"Route ID "<<itackpath->first<<"\n";
            if (itackpath->first==0) {
                slot1=starttime-1;
            }
            else {
                slot1=PrimaryPathSchedule.at(itackpath->second.at(0).at(0));
            }
            
            for (itacktrans=itackpath->second.begin(); itacktrans!=itackpath->second.end(); itacktrans++) {
                sender=itacktrans->second.at(0);
                receiver=itacktrans->second.at(1);
                //cout<<"Sender "<<sender<<" Receiver "<<receiver<<"\n";
                slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);
                //cout<<"Slot 1: "<<slot1<<"\n";
                if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                if(delay>deadline) 	return deadline+2;
                int hopcount;
                if (itackpath->first<=1) {
                    hopcount=itacktrans->first+1;
                }
                else {
                    hopcount=itacktrans->first+itackpath->first;
                }
                ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access, iphase, iapproach, itackpath->first, hopcount);// false meanse dedicated
                
                if (itackpath->first==0) {
                    slot1=FindFreeSlot(flow, slot1+1, sender, receiver, false, channel, access ,iapproach);   // 2nd dedicated slot; give true for shared slot, and false for dedicated
                    if(slot1 - starttime + 1>delay) delay=slot1 - starttime + 1;
                    if(delay>deadline) 	return deadline+2;
                    ScheduleLink(flow,  sender, receiver, slot1, channel, framelength, false, access , iphase, iapproach, itackpath->first, itacktrans->first+1);
                    PrimaryPathSchedule[sender]=slot1;
                }
            }
            
            
        }
        return delay;
    }
}



// find earliest free slot where this links can be scheduled
int ReliableScheduler:: FindFreeSlot(int flow, int starttime, int sender, int receiver, bool shared, int &channel, ACCESSTYPE iaccess, APPROACH iapproach)
{
    m=vChannels.size();
    if (iapproach==POLLING ||iapproach==SOURCE || iapproach==DIVIDE || iapproach==MCDEDI) {
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            else {
                
                map<int, vector<ScheduleEntry> >::iterator itmvs;
                for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                    UsedChannels.push_back(itmvs->first);
                    for (int index=0; index<itmvs->second.size(); index++) {
                        if (shared==false) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver) {
                                conflict=true;
                                break;
                            }
                        }
                    }
                }
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }			
            slot++; 
        }
    }
    else if (iapproach==BACKUP || iapproach==MCFLAT){
        
        if (iaccess!=DEDICATED && iaccess!=SHARED) {
            cout<<"\n\nWrong access type\n\n";
            exit(11);
        }
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            bool isharing=false;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            else {
                if (iaccess==DEDICATED) {
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                isharing=false;
                                conflict=true;
                                break;
                            }
                        }
                    }
                }
                else if (iaccess==SHARED){
                    isharing=true;
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    bool DedicatedConflict=false;
                    int SharedConflictCount=0;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        bool SharedConflict=false;
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                conflict=true;
                                if (itmvs->second.at(index).iAccess==DEDICATED) {
                                    DedicatedConflict=true;
                                    isharing=false;
                                    break;
                                }
                                else if (itmvs->second.at(index).iAccess==SHARED){
                                    SharedConflict=true;
                                    if (itmvs->second.at(index).iReceiver!=receiver) {
                                        isharing=false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (SharedConflict==true) {
                            SharedConflictCount++;
                        }
                    }
                    
                    if (conflict==true && isharing==true && SharedConflictCount<2) {
                        for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                            //UsedChannels.push_back(itmvs->first);
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                {
                                    if (itmvs->second.at(index).iAccess==SHARED && itmvs->second.at(index).iReceiver==receiver) {
                                        isharing=true;
                                        channel=itmvs->first;
                                        return slot;
                                        
                                    }
                                    else {
                                        isharing=false;
                                    }
                                    conflict=true;
                                    break;
                                }
                            }
                        }
                    }
                    
                    
                }
                
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }
            slot++;
        }
    }
    else if (iapproach==MCPRIO){
        
        if (iaccess!=DEDICATED && iaccess!=PRIMARY && iaccess!=SECONDRY) {
            cout<<"\n\nWrong access type\n\n";
            exit(11);
        }
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            bool isharing=true;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            else {
                if (iaccess==DEDICATED) {
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                isharing=false;
                                conflict=true;
                                break;
                            }
                        }
                    }
                }
                else if (iaccess==SECONDRY){
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    int primaryconflictcount=0;
                    bool SecondaryConflict=false;
                    bool DedicatedConflict=false;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                if (itmvs->second.size()>=2) {
                                    conflict=true;
                                    isharing=false;
                                    break;
                                }
                                if (itmvs->second.at(index).iAccess==PRIMARY) {
                                    primaryconflictcount++;
                                    conflict=true;
                                }
                                if (itmvs->second.at(index).iAccess==SECONDRY) {
                                    SecondaryConflict=true;
                                    isharing=false;
                                    conflict=true;
                                    break;
                                }
                                if (itmvs->second.at(index).iAccess==DEDICATED) {
                                    DedicatedConflict=true;
                                    isharing=false;
                                    conflict=true;
                                    break;
                                }
                                
                            }
                        }
                    }
                    if (conflict==true && isharing==true && primaryconflictcount<2) {
                        for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                            //UsedChannels.push_back(itmvs->first);
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                {
                                    if (itmvs->second.at(index).iAccess==PRIMARY) {
                                        isharing=true;
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                    else {
                                        cout<<"Something is wrong in the preivous for loop/n/n";
                                        exit(13);
                                        isharing=false;
                                    }
                                    conflict=true;
                                    break;
                                }
                            }
                        }
                    }
                    
                }
                else if (iaccess==PRIMARY){
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    int Secondaryconflictcount=0;
                    bool PrimaryConflict=false;
                    bool DedicatedConflict=false;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                if (itmvs->second.size()>=2) {
                                    conflict=true;
                                    isharing=false;
                                    break;
                                }
                                if (itmvs->second.at(index).iAccess==SECONDRY) {
                                    Secondaryconflictcount++;
                                    conflict=true;
                                }
                                if (itmvs->second.at(index).iAccess==PRIMARY) {
                                    PrimaryConflict=true;
                                    isharing=false;
                                    conflict=true;
                                    break;
                                }
                                if (itmvs->second.at(index).iAccess==DEDICATED) {
                                    DedicatedConflict=true;
                                    isharing=false;
                                    conflict=true;
                                    break;
                                }
                                
                            }
                        }
                    }
                    if (conflict==true && isharing==true && Secondaryconflictcount<2) {
                        for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                            //UsedChannels.push_back(itmvs->first);
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                {
                                    if (itmvs->second.at(index).iAccess==SECONDRY) {
                                        isharing=true;
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                    else {
                                        cout<<"Something is wrong in the preivous for loop/n/n";
                                        exit(13);
                                        isharing=false;
                                    }
                                    conflict=true;
                                    break;
                                }
                            }
                        }
                    }
                    
                }
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }
            slot++;
        }
    }
    else if (iapproach==SHARING || iapproach==ACKALL) {
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            bool isharing=false;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            else {
                
                map<int, vector<ScheduleEntry> >::iterator itmvs;
                for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                    UsedChannels.push_back(itmvs->first);
                    for (int index=0; index<itmvs->second.size(); index++) {
                        if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                        {
                            if (itmvs->second.at(index).iAccess!=iaccess && itmvs->second.size()<2) {
                                isharing=true;
                                channel=itmvs->first;
                                return slot;
                            }
                            else {
                                isharing=false;
                            }
                            conflict=true;
                            break;
                        }
                    }
                }
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }
            slot++; 
        }
    }
    else if (iapproach==SHARBACKUP){
        
        if (iaccess!=DEDICATED && iaccess!=SHARED && iaccess!=SECONDRY) {
            cout<<"\n\nWrong access type\n\n";
            exit(11);
        }
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            bool isharing=false;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            else {
                if (iaccess==DEDICATED) {
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                isharing=false;
                                conflict=true;
                                break;
                            }
                        }
                    }
                }
                else if (iaccess==SECONDRY){
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    int dedicatedconflictcount=0;
                    bool SecondaryConflict=false;
                    bool SharedConflict=false;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                if (itmvs->second.at(index).iAccess==DEDICATED) {
                                    dedicatedconflictcount++;
                                    conflict=true;
                                }
                                if (itmvs->second.at(index).iAccess==SECONDRY) {
                                    SecondaryConflict=true;
                                    isharing=false;
                                    conflict=true;
                                    break;
                                }
                                if (itmvs->second.at(index).iAccess==SHARED) {
                                    SecondaryConflict=true;
                                    isharing=false;
                                    conflict=true;
                                    break;
                                }
                                
                            }
                        }
                    }
                    if (SecondaryConflict==false && SharedConflict==false && dedicatedconflictcount<2) {
                        for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                            UsedChannels.push_back(itmvs->first);
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                {
                                    if (itmvs->second.at(index).iAccess==DEDICATED && itmvs->second.at(index).iReceiver==receiver && itmvs->second.at(index).iSender!=sender) {
                                        isharing=true;
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                    else {
                                        isharing=false;
                                    }
                                    conflict=true;
                                    break;
                                }
                            }
                        }
                    }
                    
                }
                else if (iaccess==SHARED){
                    bool DedicatedConflict=false;
                    int sharedconflictcount=0;
                    map<int, vector<ScheduleEntry> >::iterator itmvs;
                    for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                        UsedChannels.push_back(itmvs->first);
                        bool whethershared=false;
                        for (int index=0; index<itmvs->second.size(); index++) {
                            if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                            {
                                if (itmvs->second.at(index).iAccess==DEDICATED || itmvs->second.at(index).iAccess==SECONDRY) {
                                    isharing=false;
                                    DedicatedConflict=true;
                                    conflict=true;
                                    break;
                                }
                                if (itmvs->second.at(index).iAccess==SHARED) {
                                    whethershared=true;
                                    conflict=true;
                                }
                                
                            }
                        }
                        if (whethershared==true) {
                            sharedconflictcount++;
                        }
                    }
                    if (DedicatedConflict==false && sharedconflictcount<2) {
                        for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                            UsedChannels.push_back(itmvs->first);
                            
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                {
                                    if (itmvs->second.at(index).iReceiver==receiver) {
                                        if (itmvs->second.at(index).iFlowID==flow || (itmvs->second.at(index).iSender!=sender )){
                                            
                                            isharing=true;
                                            channel=itmvs->first;
                                            return slot;
                                        }
                                    }
                                    
                                }
                            }
                            
                            /*
                            bool sharesender=false;
                            
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                {
                                    if (itmvs->second.at(index).iFlowID!=flow && itmvs->second.at(index).iSender==sender){
                                        sharesender=true;
                                    }
                                }
                            }
                            if (sharesender==false) {
                                for (int index=0; index<itmvs->second.size(); index++) {
                                    if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                                    {
                                        if (itmvs->second.at(index).iAccess==SHARED && itmvs->second.at(index).iReceiver==receiver) {
                                            
                                            isharing=true;
                                            channel=itmvs->first;
                                            return slot;
                                            
                                            
                                        }
                                        conflict=true;
                                        break;
                                    }
                                }
                            }
                             */
                            
                            
                        }
                    }
                    
                }
                
                
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }
            slot++;
        }
    }
    else if (iapproach==ACKREV){
        
        //need to handle the difference between different access aproaches: Primary, Secondary and ACK
        //The sensing and control phase will follow the SHARING role above, so here only deals with ACK
        //In one time slot, ACK dominate Primary and Primary Dominate Secondary
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            
            else {
                map<int, vector<ScheduleEntry> >::iterator itmvs;
                for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                    bool currentchannelconflict=false;
                    UsedChannels.push_back(itmvs->first);
                    //instead of checking them one by one, should check them together
                    vector<ScheduleEntry> ChTrans;
                    ChTrans=itmvs->second;
                    for (int index=0; index<itmvs->second.size(); index++) {
                        if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                        {
                            conflict=true;
                            currentchannelconflict=true;
                        }
                    }
                    if (currentchannelconflict==true) {
                        if (itmvs->second.size()==3) {
                            continue;
                        }
                        else if (itmvs->second.size()==1) {
                            if (itmvs->second.at(0).iAccess==EACK) {
                                if (itmvs->second.at(0).iSender==sender && itmvs->second.at(0).iReceiver==receiver) {
                                    channel=itmvs->first;
                                    return slot;
                                }
                                else {
                                    continue;
                                }
                            }
                            else if (itmvs->second.at(0).iAccess==PRIMARY){
                                if (itmvs->second.at(0).iSender==sender || itmvs->second.at(0).iSender==receiver) {
                                    channel=itmvs->first;
                                    return slot;
                                }
                                else {
                                    continue;
                                }
                            }
                            else if (itmvs->second.at(0).iAccess==SECONDRY) {
                                channel=itmvs->first;
                                return slot;
                            }
                        }
                        else if (itmvs->second.size()==2) {
                            bool MeetOtherACK=false;
                            int indexPRI=-1;
                            int indexSEC=-1;
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iAccess==EACK) {
                                    MeetOtherACK=true;
                                    if (itmvs->second.at(index).iSender==sender && itmvs->second.at(index).iReceiver==receiver) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                    else {
                                        break;
                                    }
                                }
                                if (itmvs->second.at(index).iAccess==PRIMARY) {
                                    indexPRI=index;
                                }
                                if (itmvs->second.at(index).iAccess==SECONDRY) {
                                    indexSEC=index;
                                }
                            }
                            if (MeetOtherACK==false) {
                                int Psender=itmvs->second.at(indexPRI).iSender;
                                int Preceiver=itmvs->second.at(indexPRI).iReceiver;
                                int Ssender=itmvs->second.at(indexSEC).iSender;
                                int Sreceiver=itmvs->second.at(indexSEC).iReceiver;
                                
                                if (Psender==Ssender) {
                                    if (sender==Psender || receiver==Psender) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                                else if (Psender==Sreceiver){
                                    if (sender==Psender) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                                else if (Preceiver==Ssender){
                                    if (sender==Psender || (sender==Preceiver && receiver ==Psender)) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                                else if (Preceiver==Psender){
                                    if (sender==Psender || (sender==Preceiver && receiver ==Psender)) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                            }
                        }
                        //
                    }
                }
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }
            slot++;
        }
    }
    else if (iapproach==SHARACKREV){
        
        //need to handle the difference between different access aproaches: Primary, Secondary and ACK
        //The sensing and control phase will follow the SHARING role above, so here only deals with ACK
        //In one time slot, ACK dominate Primary and Primary Dominate Secondary
        int slot=starttime;
        while(1)  // loop for mmschedule[slot]
        {
            //first check there is no conflict
            int ch;
            bool conflict=false;
            vector<int> UsedChannels;
            if (mmSchedule.count(slot)==0) {
                conflict=false;
                ch=0;
            }
            else if (mmSchedule[slot].size()==0) {
                conflict=false;
                ch=0;
            }
            
            else {
                map<int, vector<ScheduleEntry> >::iterator itmvs;
                for (itmvs=mmSchedule[slot].begin(); itmvs!=mmSchedule[slot].end(); itmvs++) {
                    bool currentchannelconflict=false;
                    UsedChannels.push_back(itmvs->first);
                    //instead of checking them one by one, should check them together
                    vector<ScheduleEntry> ChTrans;
                    ChTrans=itmvs->second;
                    for (int index=0; index<itmvs->second.size(); index++) {
                        if (itmvs->second.at(index).iSender==sender|| itmvs->second.at(index).iReceiver==sender || itmvs->second.at(index).iSender==receiver || itmvs->second.at(index).iReceiver==receiver)
                        {
                            conflict=true;
                            currentchannelconflict=true;
                        }
                    }
                    if (currentchannelconflict==true) {
                        if (itmvs->second.size()==3) {
                            continue;
                        }
                        else if (itmvs->second.size()==1) {
                            if (itmvs->second.at(0).iAccess==EACK) {
                                if (itmvs->second.at(0).iSender==sender && itmvs->second.at(0).iReceiver==receiver) {
                                    channel=itmvs->first;
                                    return slot;
                                }
                                else {
                                    continue;
                                }
                            }
                            else if (itmvs->second.at(0).iAccess==PRIMARY){
                                if (itmvs->second.at(0).iSender==sender || itmvs->second.at(0).iSender==receiver) {
                                    channel=itmvs->first;
                                    return slot;
                                }
                                else {
                                    continue;
                                }
                            }
                            else if (itmvs->second.at(0).iAccess==SECONDRY) {
                                channel=itmvs->first;
                                return slot;
                            }
                        }
                        else if (itmvs->second.size()==2) {
                            bool MeetOtherACK=false;
                            int indexPRI=-1;
                            int indexSEC=-1;
                            for (int index=0; index<itmvs->second.size(); index++) {
                                if (itmvs->second.at(index).iAccess==EACK) {
                                    MeetOtherACK=true;
                                    if (itmvs->second.at(index).iSender==sender && itmvs->second.at(index).iReceiver==receiver) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                    else {
                                        break;
                                    }
                                }
                                if (itmvs->second.at(index).iAccess==PRIMARY) {
                                    indexPRI=index;
                                }
                                if (itmvs->second.at(index).iAccess==SECONDRY) {
                                    indexSEC=index;
                                }
                            }
                            if (MeetOtherACK==false) {
                                int Psender=itmvs->second.at(indexPRI).iSender;
                                int Preceiver=itmvs->second.at(indexPRI).iReceiver;
                                int Ssender=itmvs->second.at(indexSEC).iSender;
                                int Sreceiver=itmvs->second.at(indexSEC).iReceiver;
                                
                                if (Psender==Ssender) {
                                    if (sender==Psender || receiver==Psender) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                                else if (Psender==Sreceiver){
                                    if (sender==Psender) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                                else if (Preceiver==Ssender){
                                    if (sender==Psender || (sender==Preceiver && receiver ==Psender)) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                                else if (Preceiver==Psender){
                                    if (sender==Psender || (sender==Preceiver && receiver ==Psender)) {
                                        channel=itmvs->first;
                                        return slot;
                                    }
                                }
                            }
                        }
                        //
                    }
                }
            }
            if(!conflict)		//now see if channel is available
            {
                if (UsedChannels.size()<m) {
                    for (int channelindex=0; channelindex<m; channelindex++) {
                        if (IsInVector(UsedChannels, channelindex)==false) {
                            ch=channelindex;
                            channel=ch;
                            return slot;
                        }
                    }
                }
            }
            slot++;
        }
    }
    else {
        cout<<"\n\nWrong approach code\n\n";
        exit(1);
    }
			
}

bool ReliableScheduler::VerifySchedule(APPROACH iapproach)
{
    if (iapproach==POLLING || iapproach==SOURCE || iapproach==DIVIDE || iapproach==MCDEDI) {
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit;
        for(sit=mmSchedule.begin(); sit!=mmSchedule.end(); sit++)
        {
            map<int, vector<ScheduleEntry> >:: iterator cit;
            vector<int> InvolvedNodes;
            for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
            {
                if (cit->second.size()>1) {
                    cout<<"\n\n More than 1 trans on one channel in approach "<<APPtype[iapproach]<<"\n\n";
                    exit(12);
                    return false;
                }
                
                int sender=cit->second[0].iSender;
                int receiver=cit->second[0].iReceiver;
                
                if (IsInVector(InvolvedNodes, sender)==true) {
                    cout<<"\n\n Different trans share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                    exit(12);
                }
                else {
                    InvolvedNodes.push_back(sender);
                }
                
                if (IsInVector(InvolvedNodes, receiver)==true) {
                    cout<<"\n\n Different trans share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                    exit(12);
                }
                else {
                    InvolvedNodes.push_back(receiver);
                }
            }
        }
        return true;
    }
    else if (iapproach==BACKUP || iapproach==MCFLAT)
    {
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit;
        for(sit=mmSchedule.begin(); sit!=mmSchedule.end(); sit++)
        {
            map<int, vector<ScheduleEntry> >:: iterator cit;
            vector<int> InvolvedNodes;
            for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
            {
                if (cit->second.size()==1) {
                    if (cit->second[0].iAccess==SHARED) {
                        cit->second[0].iAccess=DEDICATED;
                    }
                    
                    int sender=cit->second[0].iSender;
                    int receiver=cit->second[0].iReceiver;
                    
                    if (IsInVector(InvolvedNodes, sender)==true) {
                        cout<<"\n\n Different trans share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                        exit(12);
                    }
                    else {
                        InvolvedNodes.push_back(sender);
                    }
                    
                    if (IsInVector(InvolvedNodes, receiver)==true) {
                        cout<<"\n\n Different trans share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                        exit(12);
                    }
                    else {
                        InvolvedNodes.push_back(receiver);
                    }
                }
                
                else {
                    vector<int> InterNodes;
                    int commenreceiver=cit->second[0].iReceiver;
                    for (int index=0; index<cit->second.size(); index++) {
                        ScheduleEntry ientry=cit->second.at(index);
                        if (cit->second[index].iAccess!=SHARED) {
                            cout<<"Wrong Access Type\n\n";
                            exit(12);
                        }
                        
                        int sender=cit->second[index].iSender;
                        int receiver=cit->second[index].iReceiver;
                        
                        if (receiver!=commenreceiver) {
                            cout<<"Shared links have different receiver!\n\n";
                            exit(12);
                        }
                        
                        if (IsInVector(InterNodes, sender)==false) {
                            InterNodes.push_back(sender);
                        }
                        
                        if (IsInVector(InterNodes, receiver)==false) {
                            InterNodes.push_back(receiver);
                        }
                        
                    }
                    
                    for (int indexinter=0; indexinter<InterNodes.size(); indexinter++) {
                        if (IsInVector(InvolvedNodes, InterNodes.at(indexinter))==true) {
                            cout<<"\n\n Trans in different channels share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                            exit(12);
                        }
                        else {
                            InvolvedNodes.push_back(InterNodes.at(indexinter));
                        }
                    }
                }
            }
        }
    }
    else if (iapproach==MCPRIO)
    {
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit;
        for(sit=mmSchedule.begin(); sit!=mmSchedule.end(); sit++)
        {
            map<int, vector<ScheduleEntry> >:: iterator cit;
            vector<int> InvolvedNodes;
            for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
            {
                if (cit->second.size()>2) {
                    cout<<"\n\n More than 2 trans in the same channel in "<<APPtype[iapproach]<<"\n\n";
                    cout<<"Time slot :"<<sit->first<<", Channel "<<cit->first<<"\n";
                    exit(12);
                }
                if (cit->second.size()==1) {
                    int sender=cit->second[0].iSender;
                    int receiver=cit->second[0].iReceiver;
                    
                    if (IsInVector(InvolvedNodes, sender)==true) {
                        cout<<"\n\n Different trans share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                        exit(12);
                    }
                    else {
                        InvolvedNodes.push_back(sender);
                    }
                    
                    if (IsInVector(InvolvedNodes, receiver)==true) {
                        cout<<"\n\n Different trans share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                        exit(12);
                    }
                    else {
                        InvolvedNodes.push_back(receiver);
                    }
                }
                
                else {
                    //cout<<"Priotized Sharing happens\n";
                    vector<int> InterNodes;
                    int sharenodecount=0;
                    for (int index=0; index<cit->second.size(); index++) {
                        ScheduleEntry ientry=cit->second.at(index);
                        if (cit->second[index].iAccess==DEDICATED) {
                            cout<<"Wrong Access Type\n\n";
                            exit(12);
                        }
                        
                        int sender=cit->second[index].iSender;
                        int receiver=cit->second[index].iReceiver;
                        
                        if (IsInVector(InterNodes, sender)==true) {
                            sharenodecount++;
                        }
                        else {
                            InterNodes.push_back(sender);
                        }
                        
                        if (IsInVector(InterNodes, receiver)==true) {
                            sharenodecount++;
                        }
                        else {
                            InterNodes.push_back(receiver);
                        }
                        
                    }
                    
                    if (sharenodecount==0) {
                        cout<<"\n\n Two trans in the same channel without sharing the same node in approach "<<APPtype[iapproach]<<"\n\n";
                        exit(12);
                    }
                    
                    for (int indexinter=0; indexinter<InterNodes.size(); indexinter++) {
                        if (IsInVector(InvolvedNodes, InterNodes.at(indexinter))==true) {
                            cout<<"\n\n Trans in different channels share the same node in approach "<<APPtype[iapproach]<<"\n\n";
                            exit(12);
                        }
                        else {
                            InvolvedNodes.push_back(InterNodes.at(indexinter));
                        }
                    }
                }
            }
        }
    }
    else {
        cout<<"\n\nWrong scheduling approach in VerifySchedule\n\n";
        exit(55);
    }
    return true;
}

void ReliableScheduler:: ScheduleLink(int flow, int sender, int receiver, int slot, int channel, int framelength, bool isShared, ACCESSTYPE iaccess, PHASETYPE iphase, APPROACH iapp, int routeid, int HopCount)
{
    int superframes_instances=LCM/framelength;
    //cout<<"LCM "<<LCM<<"  Supreframe numbers "<<superframes_instances<<"\n";
    for(int j=0; j<superframes_instances; j++)	//schedule every superframe instance in LCM
    {
        int slot_in_jth_frame=framelength*j+slot;
        //cout<<"\ntime slot "<<slot_in_jth_frame<<"\n";
        ScheduleEntry entry(flow,sender, receiver, isShared, iaccess, iphase, routeid, HopCount);
        mmSchedule[slot_in_jth_frame][channel].push_back(entry);
        //cout<<"Link is scheduled at "<<slot_in_jth_frame<<" time slot\n";
    }
    
}


 
bool ReliableScheduler::SuccessfulTransmission(int sender, int receiver)
{
    
    int schance=rand()% 100;
    if (schance>90) {
        return false;
    }
    else {
        return true;
    }
    
}



void ReliableScheduler::PrintSchedule(APPROACH iapproach, ROUTETYPE iroute)
{
    //LogSchedule<<"\n***************  Schedule for Scheduling approach "<<APPtype[iapproach]<<" on "<<PrintRouteType(iroute)<<" Route ******************\n\n";
    LogSchedule<<"\n***************  Schedule for "<<PrintRouteType(iroute)<<" Routing Approach ******************\n\n";
    LogTestbedSchedule<<"\n***************  Schedule for "<<APPtype[iapproach]<<" on "<<PrintRouteType(iroute)<<" ******************\n\n";
    LogWCPSSchedule<<"\n***************  Schedule for "<<APPtype[iapproach]<<" on "<<PrintRouteType(iroute)<<" ******************\n\n";
    LogSchedule<<"Time Slot, Channel, Flow ID, Sender, Receiver, RouteID, Hopcount, up/down  \n\n";
    LogTestbedSchedule<<"Time Slot, Channel, Flow ID, Sender, Receiver, RouteID, Hopcount, up/down  \n\n";
    map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
    for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
        itmpflows->second->mpSchedule.clear();
    }
    for(sit=mmSchedule.begin(); sit!=sitend; sit++)
    {
        
        map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
        LogWCPSSchedule<<"\n";
        
        
        for(cit=sit->second.begin(); cit!=citend; cit++)
        {
            for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
            {
                
                //LogWCPSSchedule<<"{"<<sit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<22+(cit->first-1+sit->first)%m<<", "<<cit->second[i].iAccess<<", "<<1-mpFlows[cit->second[i].iFlowID]->GetEmergency()<<", "<<cit->second[i].iFlowID<<", "<<mpFlows[cit->second[i].iFlowID]->GetSource()<<", "<<"0, "<<"0, "<<cit->second[i].iHopCount<<"},\t";
                if (sit->first<=mpFlows[cit->second[i].iFlowID]->GetPeriod()){
                    LogSchedule<<sit->first<<", "<<vChannels.at((cit->first-1+sit->first)%m)<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<cit->second[i].iRouteID<<", "<<cit->second[i].iHopCount<<", "<<cit->second[i].iPhase<<"\n";
                    LogTestbedSchedule<<sit->first<<", "<<vChannels.at((cit->first-1+sit->first)%m)<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<cit->second[i].iRouteID<<", "<<cit->second[i].iHopCount<<", "<<cit->second[i].iPhase<<"\n";
                }
                
                if (sit->first<=mpFlows[cit->second[i].iFlowID]->GetPeriod()) {
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][vChannels.at((cit->first-1+sit->first)%m)].push_back(cit->second[i]);
                }
                
            }
        }
    }
    

}

void ReliableScheduler::PrintFlowSchedule(APPROACH iapproach, ROUTETYPE iroute)
{
    //LogFlowSchedule<<"\n***************  Schedule for "<<APPtype[iapproach]<<" on "<<PrintRouteType(iroute)<<" ******************\n\n";
    LogFlowSchedule<<"\n***************  Schedule for "<<PrintRouteType(iroute)<<" Routing Approach ******************\n\n";
    LogTestbedFlowSchedule<<"\n***************  Schedule for "<<APPtype[iapproach]<<" on "<<PrintRouteType(iroute)<<" ******************\n\n";
    LogWCPSFlowSchedule<<"\n***************  Schedule for "<<APPtype[iapproach]<<" on "<<PrintRouteType(iroute)<<" ******************\n\n";
    LogFlowSchedule<<"Time slot, Channel, Flow ID, Sender, Receiver, RouteID, Hopcount, up/down  \n\n";
    LogTestbedFlowSchedule<<"Time slot, Channel, Flow ID, Sender, Receiver, RouteID, Hopcount, up/down \n\n";
    //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
    map<int, map<int, vector<ScheduleEntry> > > :: iterator sit;
    map<int, map<int, vector<ScheduleEntry> > > mpflowschedule;
    map<int, map<int, vector<ScheduleEntry> > > mpShortSchedule;
    for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
        LogWCPSFlowSchedule<<"\n\n //flow "<<itmpflows->second->GetSource()<<"\n";
        LogFlowSchedule<<"\n\n //flow "<<itmpflows->first<<"\n";
        LogTestbedFlowSchedule<<"\n\n //flow "<<itmpflows->first<<"\n";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit;
        mpflowschedule.clear();
        mpflowschedule=itmpflows->second->mpSchedule;
        for(sit=mpflowschedule.begin(); sit!=mpflowschedule.end(); sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            //LogFlowSchedule<<"\n"<<sit->first<<":\t\t";
            //LogWCPSFlowSchedule<<"\n";
            map<int, vector<ScheduleEntry> >:: iterator cit;
            
            for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
            {
                mpShortSchedule[sit->first][cit->first]=cit->second;
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    LogFlowSchedule<<sit->first<<", "<<cit->first<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<cit->second[i].iRouteID<<", "<<cit->second[i].iHopCount<<", "<<cit->second[i].iPhase<<"\n";
                    LogTestbedFlowSchedule<<sit->first<<", "<<cit->first<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<cit->second[i].iRouteID<<", "<<cit->second[i].iHopCount<<", "<<cit->second[i].iPhase<<"\n";
                    //mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    LogShortSchedule<<"Time slot, Channel, Flow ID, Sender Receiver \n\n";
    for (sit=mpShortSchedule.begin(); sit!=mpShortSchedule.end(); sit++) {
        map<int, vector<ScheduleEntry> >:: iterator cit;
        for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
        {
            mpShortSchedule[sit->first][cit->first]=cit->second;
            for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
            {
                
                LogShortSchedule<<sit->first<<", "<<cit->first<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"\n";
            }
        }
    }
    
    
}

void ReliableScheduler::CreateFlowRouteSchedule(ROUTETYPE iroute){
    map<int, Flow*>::iterator itflow;
    for (itflow=mpFlows.begin(); itflow!=mpFlows.end(); itflow++) {
        //LogWCPSFlowSchedule<<"\n\n //flow "<<itmpflows->second->GetSource()<<"\n";
        //cout<<"\n\n //flow "<<itmpflows->first<<"\n";
        //LogTestbedFlowSchedule<<"\n\n //flow "<<itmpflows->first<<"\n";
        map<int, map<int, vector<ScheduleEntry> > > mpUpSchedule; //[route id][hop count][schedule entry]
        map<int, map<int, vector<ScheduleEntry> > > mpDownSchedule; //[route id][hop count][schedule entry]
        map<int, map<int, vector<ScheduleEntry> > > mpflowschedule;
        map<int, map<int, vector<ScheduleEntry> > >:: iterator sit;
        mpflowschedule=itflow->second->mpSchedule;
        //cout<<mpflowschedule.size()<<"\n";
        for(sit=mpflowschedule.begin(); sit!=mpflowschedule.end(); sit++)
        {
            map<int, vector<ScheduleEntry> >:: iterator cit;
            for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
            {
                
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //cout<<sit->first<<", "<<cit->first<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<cit->second[i].iRouteID<<", "<<cit->second[i].iHopCount<<", "<<cit->second[i].iPhase<<"\n";
                    //LogTestbedFlowSchedule<<sit->first<<", "<<cit->first<<", "<<cit->second[i].iFlowID<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<", "<<cit->second[i].iRouteID<<", "<<cit->second[i].iHopCount<<", "<<cit->second[i].iPhase<<"\n";
                    //mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                    cit->second[i].iTimeSlot=sit->first;
                    cit->second[i].iChannel=cit->first;
                    DIRECTIONTYPE idirt;
                    if (cit->second[i].iPhase==SENSING) {
                        idirt=UP;
                    }
                    else{
                        idirt=DOWN;
                    }
                    //cout<<iroute<<" "<<idirt<<" "<<cit->second[i].iRouteID<<" "<<cit->second[i].iHopCount<<"\n";
                    //itmpflow->second->mpRouteSchedule
                    itflow->second->mpRouteSchedule[iroute][idirt][cit->second[i].iRouteID][cit->second[i].iHopCount].push_back(cit->second[i]);
                    
                }
                //cout<<cit->first<<"\n";
            }
            //cout<<sit->first<<"\n";
        }
        //cout<<"Right here";

        //cout<<"\nNext flow \n";
    }
    cout<<"Finish CreateFlowRouteSchedule\n";
}

void ReliableScheduler::PrintWCPSSchedule(APPROACH iapproach)
{
    if (iapproach==POLLING) {
        LogWCPSSchedule<<"\n***************  Schedule for Periodic Server ******************\n\n";
        LogWCPSSchedule<<"TimeSlot Channel  FlowType  FlowID  Sender Receiver \n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            //LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    //LogSchedule<<sit->first<<" "<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    LogWCPSSchedule<<sit->first<<" "<<cit->first<<" "<<mpFlows[cit->second[i].iFlowID]->GetEmergency()<<" "<<0<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    
    if (iapproach==SHARING) {
        LogWCPSSchedule<<"\n***************  Schedule for Periodic Server with Sharing ******************\n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            //LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    //LogSchedule<<"("<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<Atype[cit->second[i].iAccess]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    //LogSchedule<<sit->first<<" "<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<Atype[cit->second[i].iAccess]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    LogWCPSSchedule<<sit->first<<" "<<cit->first<<" "<<mpFlows[cit->second[i].iFlowID]->GetEmergency()<<" "<<cit->second[i].iAccess<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    
    if (iapproach==ACKALL) {
        LogWCPSSchedule<<"\n***************  Schedule for Polling Server with Sharing with Scheduled ACK ******************\n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            //LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    LogWCPSSchedule<<sit->first<<" "<<cit->first<<" "<<mpFlows[cit->second[i].iFlowID]->GetEmergency()<<" "<<cit->second[i].iAccess<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    
    if (iapproach==ACKREV) {
        LogWCPSSchedule<<"\n***************  Schedule for Polling Server with Sharing with Adaptive ACK ******************\n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            //LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    LogWCPSSchedule<<sit->first<<" "<<cit->first<<" "<<mpFlows[cit->second[i].iFlowID]->GetEmergency()<<" "<<cit->second[i].iAccess<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
}

void ReliableScheduler::PrintFlowDelay(APPROACH iapproach)
{
    //LogDelay<<"Flow \t Delay_Poly \t Delay_RTAS \t Delay_TMC \t Sim_Delay \t Delay_Basic \t Delay_EDF_RTAS \t Delay_Precise \t Delay_Master \t DeliveryRatio \n\n";
    if (iapproach==SOURCE) {
        for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
            //LogDelay<<itmpflow->first<<"\t"<<itmpflow->second->GetAnaDelay_FP_Poly()<<"\t"<<itmpflow->second->GetAnaDelay_FP_RTAS()<<"\t"<<itmpflow->second->GetAnaDelay_FP_TMC()<<"\t"<<itmpflow->second->GetSimulationEndToEndDelay()<<"\t"<<itmpflow->second->GetAnaDelay_EDF_Basic()<<"\t"<<itmpflow->second->GetAnaDelay_EDF_RTAS()<<"\t"<<itmpflow->second->GetAnaDelay_EDF_Precise()<<"\t"<<itmpflow->second->GetAnaDelay_EDF_Master()<<"\t"<<itmpflow->second->GetDeliverRatio()<<"\n";
            LogDelay<<itmpflow->first<<"\t"<<itmpflow->second->GetAnaDelay_EDF_Basic()<<"\t"<<itmpflow->second->GetAnaDelay_EDF_RTAS()<<"\t"<<itmpflow->second->GetAnaDelay_FP_Poly()<<"\t"<<itmpflow->second->GetAnaDelay_FP_RTAS()<<"\t"<<itmpflow->second->GetAnaDelay_FP_TMC()<<"\t"<<itmpflow->second->GetSimulationEndToEndDelay()<<"\n";
            //LogDelay<<itmpflow->first<<"\t"<<itmpflow->second->GetAnaDelay_FP_Poly()<<"\t"<<itmpflow->second->GetAnaDelay_FP_RTAS()<<"\t"<<itmpflow->second->GetAnaDelay_FP_TMC()<<"\t"<<itmpflow->second->GetSimulationEndToEndDelay()<<"\n";
            
        }
    }
    else {
        cout<<"Invalid approach in PrintFlowDelay\n";
        exit(12);
    }
}

void ReliableScheduler::PrintTransmissions(APPROACH iapproach)
{
    if (iapproach==POLLING || iapproach==SHARING ||iapproach==ACKALL) {
        LogTransmissions<<"\n***************  Transmissions for Periodic Server ******************\n\n";
        LogTransmissions<<"TimeSlot Channel  FlowType  Access Type  FlowID  Sender Receiver Success \n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, ScheduleEntry> >::iterator sit;
        
        for(sit=mmTransmissions.begin(); sit!=mmTransmissions.end(); sit++)
        {
            LogTransmissions<<"\n"<<sit->first<<": ";
            //LogTransmissions<<"\n"<<sit->first<<":\t\t";
            map<int, ScheduleEntry >:: iterator cit;
            for(cit=sit->second.begin(); cit!=sit->second.end(); cit++)
            {
                LogTransmissions<<"("<<cit->first<<" "<<Etype[mpFlows[cit->second.iFlowID]->GetEmergency()]<<" "<<Atype[cit->second.iAccess]<<" "<<cit->second.iFlowID<<" "<<cit->second.iSender<<" "<<cit->second.iReceiver<<" "<<cit->second.iSuccessfulTransmission<<")\t";
                mpFlows[cit->second.iFlowID]->mpTransmissions[sit->first][cit->first]=cit->second;
            }
        }
        LogTransmissions<<"\n\n";
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            LogTransmissions<<"Flow "<<itmpflows->first<<" Delivered "<<itmpflows->second->GetDeliveredPackets()<<" out of "<<itmpflows->second->GetNumPackets()<<" Packets\n";
        }
    }
    
    /*
    if (iapproach==SHARING) {
        LogSchedule<<"\n***************  Schedule for Periodic Server with Sharing ******************\n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    //LogSchedule<<"("<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<Atype[cit->second[i].iAccess]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    LogSchedule<<"("<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<Atype[cit->second[i].iAccess]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    //LogSchedule<<sit->first<<" "<<cit->first<<" "<<mpFlows[cit->second[i].iFlowID]->GetEmergency()<<" "<<cit->second[i].iAccess<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    
    if (iapproach==ACKALL) {
        LogSchedule<<"\n***************  Schedule for Polling Server with Sharing with ACKALL ******************\n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    LogSchedule<<"("<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<Atype[cit->second[i].iAccess]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    
    if (iapproach==ACKREV) {
        LogSchedule<<"\n***************  Schedule for Polling Server with Sharing with ACKREV ******************\n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit, sitend= mmSchedule.end();
        for (itmpflows=mpFlows.begin(); itmpflows!=mpFlows.end(); itmpflows++) {
            itmpflows->second->mpSchedule.clear();
        }
        for(sit=mmSchedule.begin(); sit!=sitend; sit++)
        {
            //Schedule<<"\n Time Slot "<<sit->first<<": ";
            LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    LogSchedule<<"("<<cit->first<<" "<<Etype[mpFlows[cit->second[i].iFlowID]->GetEmergency()]<<" "<<Atype[cit->second[i].iAccess]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
    
    if (iapproach==SITTING) {
        LogSchedule<<"\n***************  Schedule for SITTING ******************\n\n";
        LogSchedule<<"TimeSlot Channel  FlowType  FlowID  Sender Receiver \n\n";
        //LogSchedule<<"\n***************  Schedule for "<<n<<" Flows of run "<<iRunCount<<" ******************";
        map<int, map<int, vector<ScheduleEntry> > > :: iterator sit;
        for(sit=mmSchedule.begin(); sit!=mmSchedule.end(); sit++)
        {
            Schedule<<"\n Time Slot "<<sit->first<<": ";
            LogSchedule<<"\n"<<sit->first<<":\t\t";
            map<int, vector<ScheduleEntry> >:: iterator cit, citend= sit->second.end();
            for(cit=sit->second.begin(); cit!=citend; cit++)
            {
                for(int i=0; i<cit->second.size(); i++)// entered as this form: <FlowID, ChannelOffset, Sender, Receiver>
                {
                    //Schedule<<"<"<<cit->second[i].iFlowID<<", "<<cit->first<<", "<<cit->second[i].iSender<<", "<<cit->second[i].iReceiver<<"], ";
                    //LogSchedule<<"("<<cit->first<<" "<<cit->second[i].iFlowID<<" "<<Etype<<" "<<PType[cit->second[i].iPhase]<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    LogSchedule<<"("<<cit->first<<" "<<Ptype[mpCodesignFlows[cit->second[i].iFlowID]->GetPhase()]<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<")\t";
                    //LogSchedule<<sit->first<<" "<<cit->first<<" "<<mpFlows[cit->second[i].iFlowID]->GetEmergency()<<" "<<0<<" "<<cit->second[i].iFlowID<<" "<<cit->second[i].iSender<<" "<<cit->second[i].iReceiver<<"\n";
                    //mpFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                    mpCodesignFlows[cit->second[i].iFlowID]->mpSchedule[sit->first][cit->first].push_back(cit->second[i]);
                }
            }
        }
    }
     */
}

void ReliableScheduler::PrintDelay(map<int, map<int, int> > mpDelay)
{
    map<int, map<int, int> >::iterator itflowdelay;
    map<int, int>::iterator itpacketdelay;
    //LogDelay<<"Size: "<<mpDelay.size();
//    string printaddress;
//    printaddress="/Users/Chengjie/Dropbox/Research/Publication/WirelessControl/code/ReliableScheduler/Outputs/LogDelay";
//    printaddress=printaddress
    
    for (itflowdelay=mpDelay.begin(); itflowdelay!=mpDelay.end(); itflowdelay++) {
        
        
        LogDelivery<<"\nFlow "<<itflowdelay->first<<": "<<mpFlows[itflowdelay->first]->GetDeliveredPackets()<<"/"<<mpFlows[itflowdelay->first]->GetNumPackets()<<"\n";
        LogDelivery<<"\n"<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\t";
        //LogDelay<<"\nFlow "<<itflowdelay->first<<": "<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\n";
        //LogDelay<<"\n"<<itflowdelay->first<<"\t";
        LogDelay<<"\n\n";
        for (itpacketdelay=itflowdelay->second.begin(); itpacketdelay!=itflowdelay->second.end(); itpacketdelay++) {
            if (itpacketdelay->first<mpDelay.begin()->second.size()-10) {
                LogDelay<<itpacketdelay->second<<"\t";
            }
            
        }
    }
    LogDelay<<"\n";
}


void ReliableScheduler::ReadTraces(APPROACH iapproach)
{
    if (iapproach!=SOURCE) {
        cout<<"invalid approach\n\n!";
        exit(12);
    }
    map<int, map<int, map<int, vector<int> > > > mpLinkQuality;
    map<int, map<int, map<int, vector<int> > > >::iterator itmmmLinkQuality;
    map<int, map<int, vector<int> > >::iterator itmmlinkquality;
    map<int, vector<int> >::iterator itmlinkquality;
    vector<float> vLinkQuality;
    vector<float> vLinkQuality_Channel; //seperate with different channels
    cout<<"start to read traces\n";
    if (TraceReader.is_open())
    {
        cout<<"Trace reader is working \n";
        while(!TraceReader.eof())
        {
            int slot, channel, flowid, sender, receiver;
            vector<int> link;
            vector<int> vtrans;
            for (int index=0; index<5; index++) {
                int temp;
                TraceReader>>temp;
                link.push_back(temp);
            }
            
            int numsuccesstrans=0;
            
            for (int index=0; index<100; index++) {
                int temp;
                TraceReader>>temp;
                vtrans.push_back(temp);
                numsuccesstrans+=temp;
            }
            slot=link.at(0);
            channel=link.at(1);
            flowid=link.at(2);
            sender=link.at(3);
            receiver=link.at(4);
            
            bool oldlink=false;
            if (mpLinkQuality.count(sender)>0) {
                if (mpLinkQuality[sender].count(receiver)>0) {
                    if (mpLinkQuality[sender][receiver].count(channel)>0) {
                        oldlink=true;
                    }
                }
            }
            if (oldlink==false) {
                mpLinkQuality[sender][receiver][channel].push_back(vtrans.size());
                mpLinkQuality[sender][receiver][channel].push_back(numsuccesstrans);
            }
            else {
                mpLinkQuality[sender][receiver][channel].at(0)+=vtrans.size();
                mpLinkQuality[sender][receiver][channel].at(1)+=numsuccesstrans;
            }
            
            
            if (mpFlows[flowid]->mpSchedule[slot][channel].size()!=1) {
                cout<<"Flow "<<flowid<<" has "<< mpFlows[flowid]->mpSchedule[slot][channel].size() <<" scheduled links at time slot "<<slot<<" on channel "<<channel<<"!\n\n";
                exit(12);
            }
            if (mpFlows[flowid]->mpSchedule[slot][channel].at(0).iSender==sender && mpFlows[flowid]->mpSchedule[slot][channel].at(0).iReceiver==receiver) {
                mpFlows[flowid]->mpLinkTrans[slot][channel]=vtrans;
            }
            
            
            //cout<<"a new line added\n";
        }
        cout<<"\n Topology read  Done.";
        
        
    }
    else cout << "Unable to open file";
    
    for (itmpflow=mpFlows.begin(); itmpflow!=mpFlows.end(); itmpflow++) {
        LogFlowSchedule<<"\n\nFlow "<<itmpflow->first<<"\n";
        FlowLog<<"\n\nFlow "<<itmpflow->first<<"\n";
        map<int, map<int, vector<int> > > mplinktrans;
        map<int, map<int, vector<int> > >::iterator itmplinktrans;
        map<int, vector<int> >::iterator itlink;
        map<int, vector<int> > TransRounds;
        vector<int> vSlot;
        vector<int> vDelivery;
        vector<int> vDelay;
        mplinktrans=itmpflow->second->mpLinkTrans;
        for (itmplinktrans=mplinktrans.begin(); itmplinktrans!=mplinktrans.end(); itmplinktrans++) {
            vSlot.push_back(itmplinktrans->first);
            for (itlink=itmplinktrans->second.begin(); itlink!=itmplinktrans->second.end(); itlink++) {
                LogFlowSchedule<<itmplinktrans->first<<", "<<itlink->first<<", ";
                for (int index=0; index<itlink->second.size(); index++) {
                    TransRounds[index].push_back(itlink->second.at(index));
                    LogFlowSchedule<<itlink->second.at(index)<<" ";
                }
            }
            LogFlowSchedule<<"\n";
        }
        FlowLog<<"TransRounds:\n\n";
        for (itlink=TransRounds.begin(); itlink!=TransRounds.end(); itlink++) {
            FlowLog<<"\n"<<itlink->first<<" : ";
            for (int index=0; index<itlink->second.size(); index++) {
                FlowLog<<itlink->second.at(index)<<" ";
            }
            
            int delivery=1;
            int delay=0;
            for (int index=0; index<itlink->second.size()-1; index+=2) {
                if (itlink->second.at(index)+itlink->second.at(index+1)==0) {
                    delivery=0;
                    break;
                }
            }
            vDelivery.push_back(delivery);
            if (delivery==0) {
                delay=0;
            }
            else {
                if (itlink->second.at(itlink->second.size()-2)==1) {
                    delay=vSlot[vSlot.size()-2];
                }
                else {
                    delay=vSlot.back();
                }
            }
            vDelay.push_back(delay);
            
        }
        itmpflow->second->vDelivery=vDelivery;
        itmpflow->second->vDelay=vDelay;
        
        LogFlowSchedule<<"\n Vector of delivery:\n";
        int numDelivered=0;
        for (int index=0; index<vDelivery.size(); index++) {
            numDelivered+=vDelivery.at(index);
            LogFlowSchedule<<vDelivery.at(index)<<" ";
        }
        float ratio=float(numDelivered)/float(vDelivery.size());
        itmpflow->second->SetDeliverRatio(ratio);
        LogFlowSchedule<<"\n Delivery Ratio: "<<ratio<<"\n";
        LogDeliveryRatio<<itmpflow->second->GetDeliverRatio()<<" ";
        
        //cout<<ratio<<"\t";
        LogFlowSchedule<<"\n Vector of delay:\n";
        for (int index=0; index<vDelay.size(); index++) {
            LogFlowSchedule<<vDelay.at(index)<<" ";
        }
        
    }
    for (itmmmLinkQuality=mpLinkQuality.begin(); itmmmLinkQuality!=mpLinkQuality.end(); itmmmLinkQuality++) {
        for (itmmlinkquality=itmmmLinkQuality->second.begin(); itmmlinkquality!=itmmmLinkQuality->second.end(); itmmlinkquality++) {
            int numstrans=0;
            int numtrans=0;
            float alllinkq;
            for (itmlinkquality=itmmlinkquality->second.begin(); itmlinkquality!=itmmlinkquality->second.end(); itmlinkquality++) {
                float linkq=float(itmlinkquality->second.at(1))/float(itmlinkquality->second.at(0));
                vLinkQuality_Channel.push_back(linkq);
                numstrans+=itmlinkquality->second.at(1);
                numtrans+=itmlinkquality->second.at(0);
            }
            alllinkq=float(numstrans)/float(numtrans);
            vLinkQuality.push_back(alllinkq);
        }
    }
//    for (itmplinkquality=mpLinkQuality.begin(); itmplinkquality!=mpLinkQuality.end(); itmplinkquality++) {
//        for (itlinkquality=itmplinkquality->second.begin(); itlinkquality!=itmplinkquality->second.end(); itlinkquality++) {
//            int numstrans=0;
//            for (int index=0; index<itlinkquality->second.size(); index++) {
//                numstrans+=itlinkquality->second.at(index);
//            }
//            float linkq=float(numstrans)/float(itlinkquality->second.size());
//            vLinkQuality.push_back(linkq);
//        }
//    }
    for (int index=0; index<vLinkQuality_Channel.size(); index++) {
        LogLinkQuality<<vLinkQuality_Channel.at(index)<<" ";
    }
    //LogLinkQuality<<"\n\n";
    for (int index=0; index<vLinkQuality.size(); index++) {
        //LogLinkQuality<<vLinkQuality.at(index)<<" ";
    }
}

void ReliableScheduler::PrintFlowTrace(APPROACH iapproach)
{
    if (iapproach!=SOURCE) {
        cout<<"invalid approach\n\n!";
        exit(12);
    }
    LogDeliveryRatio<<"\n";
    map<int, float> mpDeliveryRatio;
    map<int, vector<int> > mpWorstCaseDelay;
    map<int, vector<int> > mpTestDelay;
    
    for (int flowindex=0; flowindex<mpFlows.size(); flowindex+=2) {
        vector<int> vDeliveryDouble;
        vector<int> vDeliveryFirst;
        vector<int> vDeliverySecond;
        vector<int> vDelayFirst;
        vector<int> vDelaySecond;
        int numsuctrans=0;
        int flowindex_delay;
        
        
        vDeliveryFirst=mpFlows[flowindex]->vDelivery;
        vDeliverySecond=mpFlows[flowindex+1]->vDelivery;
        vDelayFirst=mpFlows[flowindex]->vDelay;
        vDelaySecond=mpFlows[flowindex+1]->vDelay;
        
        for (int index=0; index<vDeliveryFirst.size(); index++) {
            if (vDeliveryFirst.at(index)+vDeliverySecond.at(index)>0) {
                vDeliveryDouble.push_back(1);
                numsuctrans++;
            }
            else {
                vDeliveryDouble.push_back(0);
            }
        }
        float ratio=float(numsuctrans)/float(vDeliveryDouble.size());
        mpDeliveryRatio[flowindex]=ratio;
        LogDeliveryRatio<<ratio<<" ";
        
        
        
        
        
        int maxTestDelay=0;
        int minTestDelay=INFINITY;
        for (int index=0; index<vDelayFirst.size(); index++) {
            if (vDelayFirst.at(index)>0) {
                mpTestDelay[flowindex].push_back(vDelayFirst.at(index));
                LogTestDelayVector<<vDelayFirst.at(index)<<" ";
                if (vDelayFirst.at(index)>maxTestDelay) {
                    maxTestDelay=vDelayFirst.at(index);
                }
                if (vDelayFirst.at(index)<minTestDelay) {
                    minTestDelay=vDelayFirst.at(index);
                }
            }
        }
        
        for (int index=0; index<vDelaySecond.size(); index++) {
            if (vDelaySecond.at(index)>0) {
                mpTestDelay[flowindex].push_back(vDelaySecond.at(index));
                LogTestDelayVector<<vDelaySecond.at(index)<<" ";
                if (vDelaySecond.at(index)>maxTestDelay) {
                    maxTestDelay=vDelaySecond.at(index);
                }
                if (vDelaySecond.at(index)<minTestDelay) {
                    minTestDelay=vDelaySecond.at(index);
                }
            }
        }
        LogTestDelayVector<<"\n";
        
        if (mpFlows[flowindex]->GetSimulationEndToEndDelay()>mpFlows[flowindex+1]->GetSimulationEndToEndDelay()) {
            flowindex_delay=flowindex;
        }
        else {
            flowindex_delay=flowindex+1;
        }
        
        int EDF_Basic=mpFlows[flowindex_delay]->GetAnaDelay_EDF_Basic();
        int EDF_Precise=mpFlows[flowindex_delay]->GetAnaDelay_EDF_Precise();
        //int EDF_RTAS=mpFlows[flowindex_delay]->GetAnaDelay_EDF_Master();
        int Basic_delay=mpFlows[flowindex_delay]->GetAnaDelay_FP_Poly();
        int Improved_delay=mpFlows[flowindex_delay]->GetAnaDelay_FP_TMC();
        int Sim_delay=mpFlows[flowindex_delay]->GetSimulationEndToEndDelay();
        

        
        mpWorstCaseDelay[flowindex].push_back(Basic_delay);
        mpWorstCaseDelay[flowindex].push_back(Improved_delay);
        mpWorstCaseDelay[flowindex].push_back(Sim_delay);
        mpWorstCaseDelay[flowindex].push_back(maxTestDelay);
        
        
        LogDelay<<minTestDelay<<" "<<maxTestDelay<<" "<<Sim_delay<<" "<<Improved_delay<<" "<<EDF_Precise<<" "<<Basic_delay<<" "<<EDF_Basic<<"\n";
        //LogDelay<<EDF_Basic<<" "<<EDF_Precise<<" "<<Basic_delay<<" "<<Improved_delay<<" "<<Sim_delay<<" "<<minTestDelay<<" "<<maxTestDelay<<"\n";
        
    }
    
}



void ReliableScheduler::PrintFirstPacketDelay(map<int, map<int, vector<int> > > mpFirstDelay)
{
    map<int, map<int, vector<int> > >::iterator itflowdelay;
    map<int, vector<int> >::iterator itpacketdelay;
    //LogDelay<<"Size: "<<mpDelay.size();
    //    string printaddress;
    //    printaddress="/Users/Chengjie/Dropbox/Research/Publication/WirelessControl/code/ReliableScheduler/Outputs/LogDelay";
    //    printaddress=printaddress
    
    for (itflowdelay=mpFirstDelay.begin(); itflowdelay!=mpFirstDelay.end(); itflowdelay++) {
        
        
        //LogDelivery<<"\nFlow "<<itflowdelay->first<<": "<<mpFlows[itflowdelay->first]->GetDeliveredPackets()<<"/"<<mpFlows[itflowdelay->first]->GetNumPackets()<<"\n";
        //LogDelivery<<"\n"<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\t";
        //LogDelay<<"\nFlow "<<itflowdelay->first<<": "<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\n";
        //LogDelay<<"\n"<<itflowdelay->first<<"\t";
        LogFirstDelay<<"\n\n";
        for (itpacketdelay=itflowdelay->second.begin(); itpacketdelay!=itflowdelay->second.end(); itpacketdelay++) {
            LogFirstDelay<<itpacketdelay->second.at(0)<<"\t";
        }
    }
    LogFirstDelay<<"\n";
    
    for (itflowdelay=mpFirstDelay.begin(); itflowdelay!=mpFirstDelay.end(); itflowdelay++) {
        
        
        //LogDelivery<<"\nFlow "<<itflowdelay->first<<": "<<mpFlows[itflowdelay->first]->GetDeliveredPackets()<<"/"<<mpFlows[itflowdelay->first]->GetNumPackets()<<"\n";
        //LogDelivery<<"\n"<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\t";
        //LogDelay<<"\nFlow "<<itflowdelay->first<<": "<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\n";
        //LogDelay<<"\n"<<itflowdelay->first<<"\t";
        LogFirstDelay<<"\n\n";
        for (itpacketdelay=itflowdelay->second.begin(); itpacketdelay!=itflowdelay->second.end(); itpacketdelay++) {
            LogFirstDelay<<itpacketdelay->second.at(1)-itpacketdelay->second.at(0)<<"\t";
        }
    }
    LogFirstDelay<<"\n";
    
    for (itflowdelay=mpFirstDelay.begin(); itflowdelay!=mpFirstDelay.end(); itflowdelay++) {
        
        
        //LogDelivery<<"\nFlow "<<itflowdelay->first<<": "<<mpFlows[itflowdelay->first]->GetDeliveredPackets()<<"/"<<mpFlows[itflowdelay->first]->GetNumPackets()<<"\n";
        //LogDelivery<<"\n"<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\t";
        //LogDelay<<"\nFlow "<<itflowdelay->first<<": "<<float(mpFlows[itflowdelay->first]->GetDeliveredPackets())/float(mpFlows[itflowdelay->first]->GetNumPackets())<<"\n";
        //LogDelay<<"\n"<<itflowdelay->first<<"\t";
        LogFirstDelay<<"\n\n";
        for (itpacketdelay=itflowdelay->second.begin(); itpacketdelay!=itflowdelay->second.end(); itpacketdelay++) {
            LogFirstDelay<<itpacketdelay->second.at(1)<<"\t";
        }
    }
    LogFirstDelay<<"\n";
}


void ReliableScheduler:: PerformanceRecord()
{
	cout<<"\n=======================================================================================================================================";
	cout<<"\n ---------------------------------Performance-------------------------------------------";
	cout<<"\n Periods ranges: [2^"<<exp1<<", 2^"<<exp2<<"] "<< " total channels:"<<m<<" total runs: "<<iRuns;	
	cout<<" \n TotalFlows = ["; 
	for(n=10; n<=iTotalFlows; n+=5){cout<<n<<" \t"; }
	cout<<" ];";
	cout<<" \n DB_Simulation = ["; 
	for(n=10; n<=iTotalFlows; n+=5){cout<<AcceptanceCount[n][DB_SIM]<<"\t"; }
	cout<<" ];";
	cout<<" \n DB_Acceptance = ["; 
	for(n=10; n<=iTotalFlows; n+=5){cout<<AcceptanceCount[n][DB]<<"\t"; }
	cout<<" ];";	
	cout<<" \n DM_Simulation = ["; 
	for(n=10; n<=iTotalFlows; n+=5){cout<<AcceptanceCount[n][DM_SIM]<<"\t"; }
	cout<<" ];";
	cout<<" \n DM_Acceptance = ["; 
	for(n=10; n<=iTotalFlows; n+=5){cout<<AcceptanceCount[n][DM]<<"\t"; }
	cout<<" ];";
	cout<<"\n=======================================================================================================================================";
	
	// write to logs
	LogAllRuns<<"\n=======================================================================================================================================";
	LogAllRuns<<"\n ---------------------------------Performance-------------------------------------------";
	LogAllRuns<<"\n Periods ranges: [2^"<<exp1<<", 2^"<<exp2<<"] "<< " total channels:"<<m<<" total runs: "<<iRuns;
	
	LogAllRuns<<" \n TotalFlows = ["; 
	for(n=10; n<=iTotalFlows; n+=5){LogAllRuns<<n<<"\t"; }
	LogAllRuns<<" ];";
	LogAllRuns<<" \n DB_Simulation = ["; 
	for(n=10; n<=iTotalFlows; n+=5){LogAllRuns<<AcceptanceCount[n][DB_SIM]<<"\t"; }
	LogAllRuns<<" ];";
	LogAllRuns<<" \n DB_Acceptance = ["; 
	for(n=10; n<=iTotalFlows; n+=5){LogAllRuns<<AcceptanceCount[n][DB]<<"\t"; }
	LogAllRuns<<" ];";	
	LogAllRuns<<" \n DM_Simulation = ["; 
	for(n=10; n<=iTotalFlows; n+=5){LogAllRuns<<AcceptanceCount[n][DM_SIM]<<"\t"; }
	LogAllRuns<<" ];";
	LogAllRuns<<" \n DM_Acceptance = ["; 
	for(n=10; n<=iTotalFlows; n+=5){LogAllRuns<<AcceptanceCount[n][DM]<<"\t"; }
	LogAllRuns<<" ];";
	LogAllRuns<<"\n=======================================================================================================================================";	
}



void ReliableScheduler::AssignRMPriority(map<int, int> &f)
{
    multimap<int, int> mPeriodToFlow;
    map <int, Flow*> ::iterator  zit;
    //int count=1; LCM=1;
    for(zit=mpFlows.begin(); zit!=mpFlows.end(); zit++)
    {
        int period= zit->second->GetPeriod();
        mPeriodToFlow.insert(pair<int, int>(period,  zit->first));
    }
    int priority=0;
    multimap<int, int> :: iterator  it;
    for(it=mPeriodToFlow.begin(); it!=mPeriodToFlow.end(); it++)
    {
        priority++;
        f[priority]=it->second;
        cout<<"\n"<<it->second<<"\t Priority \t"<<priority;
        //mpFlows[it->second]->setPriority(priority);
    }
}








void ReliableScheduler::RandomSimulator()
{
    
    typedef enum
    {
        FAILURE,  //Polling server
        FIRST,  //Polling server with slot sharing
        SECOND ,    //Polling server with slot sharing with scheduled ACK
        THIRD,  //Polling server with slot sharing with immediate one time ACK on the reverse route
    } CONTROLDELIVERYTYPE;
    
    typedef enum
    {
        FAIL,  //Polling server
        SUCCESS,  //Polling server with slot sharing
    } SENSEDELIVERYTYPE;
    
//    if (NetworkOutput.size()!=4) {
//        cout<<"Wrong input \n\n";
//        exit(1);
//    }
    CONTROLDELIVERYTYPE icdeliver;
    SENSEDELIVERYTYPE isdeliver;
    int stry1;
    int stry2;
    int stry3;
    
    map<int, int> mCount;
    mCount[FAILURE]=0;
    mCount[FIRST]=0;
    mCount[SECOND]=0;
    mCount[THIRD]=0;
    
    vector<int> vPeriods;
    vPeriods.push_back(100);
    vPeriods.push_back(50);
    vPeriods.push_back(33);
    map<int, int>::iterator itiii;
    for (int index1=0; index1<vPeriods.size(); index1++) {
        mCount.clear();
        for (int index=0; index<(240000/vPeriods[index1]); index++) {
            
            stry1=rand() % 1000;
            if (stry1>997) {
                isdeliver=FAIL;
                icdeliver=FAILURE;
                mCount[FAILURE]++;
            }
            else {
                stry1=rand() % 1000;
                stry2=rand() % 1000;
                if (stry1>900 && stry2>900) {
                    stry1=rand() % 1000;
                    stry2=rand() % 1000;
                    stry3=rand() % 1000;
                    if (stry1<=900 && stry2<=900 && stry3<=900) {
                        icdeliver=SECOND;
                        mCount[SECOND]++;
                    }
                    else {
                        icdeliver=FAILURE;
                        mCount[FAILURE]++;
                    }
                }
                else {
                    stry1=rand() % 1000;
                    stry2=rand() % 1000;
                    if (stry1>900 && stry2>900){
                        stry1=rand() % 1000;
                        stry2=rand() % 1000;
                        if (stry1<=900 && stry2<=900) {
                            icdeliver=THIRD;
                            mCount[THIRD]++;
                        }
                        else {
                            icdeliver=FAILURE;
                            mCount[FAILURE]++;
                        }
                    }
                    else {
                        stry1=rand() % 1000;
                        stry2=rand() % 1000;
                        if (stry1<=900 || stry2<=900){
                            icdeliver=FIRST;
                            mCount[FIRST]++;
                        }
                        else {
                            icdeliver=FAILURE;
                            mCount[FAILURE]++;
                        }
                        
                    }
                }
            }
            
        }
        for (itiii=mCount.begin(); itiii!=mCount.end(); itiii++) {
            LogRand<<"\n "<<itiii->first<<" "<<itiii->second;
        }
        LogRand<<"\n\n\n";
    }
    
    for (int index=0; index<vPeriods.size(); index++) {
        LogRand<<"\n";
        LogRand<<240000/vPeriods[index]<<"\t";
    }
    
    
    
    
    LogRand.close();
}




bool ReliableScheduler:: IsInVector(vector<int> v, int  value)
{
	for(int i=0; i<v.size(); i++)
	{
		if(value==v[i])
			return true;
	}
	return false; 
	
}

ReliableScheduler:: ~ReliableScheduler()
{
}


 