/*
 *  ReliableScheduler.h
 *  ReliableScheduler
 *
 *  
 *
 */

#ifndef __RELIABLESCHEDULER
#define	__RELIABLESCHEDULER


#include <vector>
#include <list>
#include <string>
#include <map>
#include <queue>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <math.h>
#include <algorithm>
//#include "Actuator.h"
#include "Flow.h"
#include "Node.h"
#include "ScheduleEntry.h"
//#include "SearchTreeNode.h"
#include "conio.h"
//#include "EmergencyEntry.h"
//#include "glpk.h"


using namespace std;


class ReliableScheduler						//main class
{
    bool whethersingleperiod;
    float PRRBound;
	int iGateway;							//Gateway is wired connected to access points; 
	int iRuns;								//total runs
	int iRunCount;
	int m;									//m=total channels
    int iTotalChannels;                     //same as m
	int iTotalFlows;						//total flows
	int numflows_for_display;
    int inumflows;                          //number of flows
    int n;                                  //number of flows, identical to inumflows
    int numAccessPoints;
    float BigBattery;
    float SABattery; //battery capacity for sensor or actuator
    float GLPKratio;
    int inumtrace;
    int inumlinks;// num of links in testbed test;
    map<ROUTETYPE, int> mpLifetime;
    map<int, int> mpHanTime;
    vector<vector<int> > HanUplinkGraph;
    
	map<int, vector<int> > mvUpLinkGraph;	//Han's uplink graph: key=sensor or router; vector[0]=primary parent; vector[1]=secondary parent
	vector<int> vSensors;					//vector of sensors
	vector <int> vActuators;				//vector of actuators
	vector<int> vAccessPoints;				//vector of access points
    vector<int> vChannels;
    vector<int> combination;
    map<int, vector<int> > mpChannels;
    
    map<int, map<int, map<int, float > > > mmPRR;   //key1, node id; key2, node id; key3, channel; value, prr
    map<int, map<int, map<int, vector<int> > > > mmPRRTrace;   //[sender] [receiver] [channel] [vector of traces]
    map<int, map<int, map<int, int > > > mmPRRIndex;   //[sender] [receiver] [channel] [index of current trace]
    map<int, map<int, map<int, int > > > mmPRRReal;   //[sender] [receiver] [channel] [wheather real]
    map<int, map<int, map<int, float> > >::iterator itmmmPRR;
    map<int, map<int, float> >::iterator itmmPRR;
    map<int, float>::iterator itmPRR;
    map<int, map<int, int> > mmTopology;
    
    map<int, vector<int> > mvNeighbor;      //key: node id, value: neighbors of this node
    map<int, map<int, int> > mmHopCount;    //key1, node id; key2, node id; value hop count
    multimap<int, int> mMaxHop;         //key: max hopcount; value: node id;
    
	//map<int, Actuator*> mpActuators;		//ke=actuator node ID
    map<int, Node*> mpNodes;
    map<int, Node*>::iterator itnodes;
	map<int, Flow*> mpFlows;				// map for flows: Flow ID  is the key
    map<int, Flow*>::iterator itmpflow;
    map<int, Flow*> mpCodesignFlows;
    //map<int, EmergencyEntry*> mpEmergency;
    //map<int, EmergencyEntry*>::iterator itmpem;
    map<int, Flow*>::iterator itmpflows;
    map<int, vector<int> >::iterator itmpgraph;
    map<int, map<int, int> >::iterator itmroute;
    map<int, map<int, vector<int> > >::iterator itmpackroute;
    //map<int, map<int, int> >::iterator itmpgraphroute;
	map<int, map<int, int> > mmSmalldelta;	//ke1: lowerpriority flow, key2=higher priority flow; value is small delta
	map<int, map<int, int> > mmSmalldelta2;	//ke1: lowerpriority flow, key2=higher priority flow; value is small delta

	map<int, map<int, int> > mmDelta;	//ke1: lowerpriority flow, key2=higher priority flow; value is small delta
    
    map<int, map<int, int>  > mmLpFlowHpFlowConflict;							//[lpflow][hp flow]=conflict
    map<int, map<int, int>  > mmLpFlowHpFlowOneTxMaxDelay;
    map<int, map<int, int>  > mmLpFlowHpFlowFirstTxMaxDelay;                    // map of delay by first tx by higher priority flow[lpflow][hpflow]=maxdelay
    
	int deadlinedivisor;
    int regularperiod;
    int emergencyperiod;
    int SIMStart;
    int SIMEnd;
    int EMStart;
    int EMEnd;
    int LOSTDELAY;
    
    int PowerTrans;
    int PowerReceive;
    int PowerWait;
    
    
    vector<char> Etype;
    map<ACCESSTYPE, char> Atype;
    vector<char> Ptype;
    map<APPROACH, string> APPtype;
    map<ROUTETYPE, string> RouteName;
    map<DIRECTIONTYPE, string> DirectionName;
    vector<char> Btype;
    vector<char> vCritType;
    vector<bool> vPass;
    
    vector<APPROACH> vAPPROACH;
    vector<ROUTETYPE> vRoutes;
    vector<DIRECTIONTYPE> vDirects;

	int LCM;								//Hyperperiod
	int exp1, exp2;							//2^exp1~2^exp2  for period range
    ACCESSTYPE iaccesstype;
    APPROACH iapproach;
    
    bool stop;
	map<int, int> f;										// Key: priority value=flowID
	map<int, int> DM_f;										// Key: priority value=flowID
    
	map<int, map<int, vector<ScheduleEntry> > > mmSchedule;		// schedule:  [time][chennl]= vector of transmissions scheduled on that slot and channel
    map<int, map<int, ScheduleEntry > > mmTransmissions;		// schedule:  [time][chennl]= vector of transmissions scheduled on that slot and channel
    map<int, map<int, vector<ScheduleEntry> > > :: iterator itschedule;
    map<int, vector<ScheduleEntry> >::iterator itslotschedule;
    map<int, map<int, vector<ScheduleEntry> > > PSSchedule;		// schedule:  [time][chennl]= vector of transmissions scheduled on that slot and channel
    
    map<int, map<int, vector<ScheduleEntry> > > ASPSSchedule;		// schedule:  [time][chennl]= vector of transmissions scheduled on that slot and channel
	
	map<int, map<int, int> > AcceptanceCount;					// key1= number of flows   key2: algorithm
    
	
	// I/O variables
    ifstream TopologyReader;
	ifstream GraphRouteReader;
    ifstream TraceReader;
    ifstream GLPKReader;
    ifstream TestbedTraceReader;
    ifstream HanTimeReader;
	ofstream Log;
    ofstream FlowLog;
	ofstream LogAllRuns;					//Log performance
	ofstream Schedule;
    ofstream LogSchedule;
    ofstream LogTestbedSchedule;
    ofstream LogWCPSSchedule;
    ofstream LogFlowSchedule;
    ofstream LogShortSchedule;
    ofstream LogTestbedFlowSchedule;
    ofstream LogWCPSFlowSchedule;
    ofstream LogDebug;
    ofstream LogACC;
    ofstream LogWCDelay;
    ofstream LogTransmissions;
    ofstream LogDelay;
    ofstream LogDelivery;
    ofstream LogFirstDelay;
    ofstream LogTopology;
    ofstream LogChannelDiversity;
    ofstream LogLinks;
    ofstream LogSourceRouting;
    ofstream LogFlowRouting;
    ofstream LogRESP;
    ofstream LogRand;
    ofstream LogEdgeDisjointPaths;
    ofstream LogDeliveryRatio;
    ofstream LogLinkQuality;
    ofstream LogTestDelayVector;
    ofstream LogFaultTolerantRoute;
    ofstream LogGLPKData;
    ofstream LogPowerRate;
    ofstream LogGraphSchedule;
    ofstream LogTestbedGraphTrace;
    ofstream LogTransmissionTraces;
    ofstream LogEnergySimulation;
    ofstream LogEnergy;
    ofstream LogTime;
    ofstream LogLifetimeEXP;
    ofstream LogLifetimeRatio;  // The ratio to the DIJ algorithm
    ofstream LogLifetimeSim;  // lifetime in simulation
    ofstream LogLifetimeSimRatio;  // lifetime in simulation
    ofstream LogHanGraph;
	
	//functions
    
    //set up input files and output files
    void setIOFiles();
    void closeIOFiles();
    //topology and routing
    
    void ReadTraces(APPROACH iapproach);
    void CollectTopology(bool TestCase);
    void pickup(int offset, int k);
    void pickup_copy(int offset, int k, vector<int> pool, map<int, vector<int> > &mpConbination, vector<int> &Candidate);
    void SelectChannel(bool preset, bool TestCase);
    void SelectLinks();
    void CheckLinkConnectivity();
    int NumConnectedNode(int source);
    void GetAccessPoints_Neighbors(int numpoints, bool preset);
    void GetAccessPoints_Paths(int numpoints, bool preset, bool TestCase);
    void SetNodeBattery(bool TestCase);
    
    //routing
    int Shortestpath(int source);
    int BreadthFirstSearch(map<int, map<int, int> > Capacity, map<int, map<int, int> > CurrentFlow, map<int, vector<int> > Neighbors, map<int, int> &Parents, map<int, int> &MaxFlow, int source, int destination);
    
    int EdmondsKarp(map<int, map<int, int> > Capacity, map<int, vector<int> > Neighbors, int source, int destination, map<int, map<int, int> > &CurrentFlow);
    int EdgeDisjointPaths(int source, int destination, map<int, map<int, int> > mmTopology, map<int, map<int, int> > &CurrentFlow);
    void CountEdgeDisjointPaths();
    bool CheckFaultTolerantRoutes_Uplink();
    bool CheckFaultTolerantRoutes_Downlink(int nodeid);
    void PrintFaultTolerantRoutes();
    bool GenerateBFSTopology(map<int, map<int, float> > mmTraffic, float threshold, set<int> &sNode, map<int, vector<int> > &mvtop); //return false if mvtop is empty
    bool FindBFSRoute(int source, int dest, set<int> sNode, map<int, vector<int> > mvtop, map<int, vector<int> > &SourceRoute);
    
    void PrintNodes();
    string PrintRouteType(ROUTETYPE iroute);
    
    //Generic Route Functions
    void CreateAllGraphRouting();
    void VerifyAllGraphRouting();
    void PrintGraphRouteSchedule();
    bool SendOnePacket(int flowid, ROUTETYPE iroute);
    bool SendOnePacketSourceRoute(int flowid, ROUTETYPE iroute);
    void CollectTestbedGraphTrace();
    void PrintTestbedGraphTrace();
    int ReadTestbedGraphTrace(int sender, int receiver, int channel);
    void PrintTransmissionTraces();
    void SourceRoutingSimulation();
    void CollectHanRoutingTime();
    
    //Han Route Functions
    
    void ReadHanUplinkGraph();
    void ReadHanDownlinkGraph();
    void CreateHanGraphRouting();
    bool GenerateHanGraphRoute(int source, int root, vector<vector<int> > &vvHanRoute);
    void FindHanGraphRoute(int source, int destination, vector<vector<int> > vvHanRoute,  map<int, map<int, vector<int> > > &Groute);
    
    //Glpk Route Functions
    void PrintMIPData();
    void PrintGLPKData_base();
    void PrintGLPKData_second();
    void PrintGLPKData_backup();
    void ReadGLPKResult_base();
    void ReadGLPKResult_primary();
    void ReadGLPKResult_backup();
    void ReadGLPKResult_second();
    void ReadMIPGLPK();
    void SetFlowGLPKRoute();
    void LPGLPK();
    void MIPGLPK();
    
    //Dijkstra Route Functions
    void CreateDijkstraSourceRouting();
    void CreateDijkstraGraphRouting();
    void CreateFlowDijkstraGraphRouting();
    bool FindDijkstraGraphRoute(int source, int dest, map<int, map<int, int> > mmTopology, map<int, map<int, vector<int> > > &GraphRoute);
    bool FindDijkstraRoute(int source, int dest, map<int, map<int, int> > mmTopology, map<int, vector<int> > &SourceRoute);
    
    //MinLoad Route Functions
    void CreateMinLoadGraphRouting();
    bool FindMinLoadGraphRoute(int flowid, DIRECTIONTYPE dirt, map<int, map<int, int> > mmTopology, map<int, map<int, vector<int> > > &GraphRoute);
    bool FindMinLoadRoute(int source, int dest, map<int, map<int, int> > mmTopology, map<int, vector<int> > &SourceRoute, int rate, bool Primary);
    
    void CreateMinLoadGraphRouting_Iterative();
    void DestroyMinLoadGraphRoute(int flowid); //destroy the minload graph routes for flow flowid and update coreponding load paramenters for each node
    //bool FindMinLoadGraphRoute_Iterative(int flowid, DIRECTIONTYPE dirt, map<int, map<int, int> > mmTopology, map<int, map<int, vector<int> > > &GraphRoute);
    //bool FindMinLoadRoute_Iterative(int source, int dest, map<int, map<int, int> > mmTopology, map<int, vector<int> > &SourceRoute, int rate, bool Primary);
    

    void CalculatePowerRate();
    
	void GenerateFlows(int numflows, bool whethersingleperiod, bool Testcase);
    void GenerateFlows_DisjointRoutes(int numflows, bool whethersingleperiod);
    
    //Schedulability analysis
    
    void GetWCDelay(APPROACH iapproach, map<int, int> &mpEDelay, map<int, int> &mpRDelay);
    int  GetWCEMDelay(APPROACH);
    int  GetWCREDelay(APPROACH);
	void Determinedelta(int lpflow, int hpflow);	// determine small delta for lpflow with hp flow
	int  DetermineDelta(int lpflow, int hpflow);	// determine big Delta for lpflow with hp flow
	int  DetermineDeltaSum(int lpflow, int hpflow);	// determine pessimistic  Delta total of all paths  for lpflow with hp flow
    
	void CalculateAlldelta();
	void CalculateLiAllFlows();
    
    bool SchedulabilityTest_EDF_RTAS();
    bool SchedulabilityTest_EDF_Master();
    bool SchedulabilityTest_EDF_Precise();
    bool SchedulabilityTest_EDF_Basic();
    bool SchedulabilityTest_FP_Poly();
    bool SchedulabilityTest_FP_RTAS();
    bool SchedulabilityTest_FP_TMC();
    
    int DelayByHigherPriority(int lower, int higher);
    int DeterminePartialConflict(int lpflow, int hpflow, int conflen);
    int  MaxOneLowTxDelayByHigherPriority(int lower, int higher);
    // Network functions
    int MaxOneLowTxDelayByFirstTxsHigherPriority(int lower, int higher);
	
	int  CarryIn( int Ck, int x, int Ci, int Ti, int Ri);
	int  NoCarryIn( int Ck, int x, int Ci, int Ti, int Ri);
	bool SchedulabilityTest();									//this is our sched test
	int PolynomialDelayBound(int iFlow, int, int &);
	int DelayBound(map<int, int> temp_f, int currentlevel) ;
	int  LiSensing(int flow, int starttime);
	int  LiControl(int flow, int starttime);
	void  DetermineLi();
    int InfiniteChannelTest(int priority, map<int, int> pf);
	bool SchedulabilityTest_S_UB(map<int, int> pf, int l, int k);
	bool SchedulabilityTest_S_LB(map<int, int> pf, int l, int k);
	//bool Search(SearchTreeNode *nd);
    
	//Priority assignment functions
	void AssignDMPriority();
	void AssignDBPriority();									//Based on delay bound
    void AssignRMPriority(map<int, int> &f);
	
	
	//performance recorder functions
	void PerformanceRecord();
    void PrintSourceRouting();
    void PrintGraphRouting();
    void PrintGLPKPowerRouting();
    void PrintFlowSourceRouting();
    void PrintFlowGraphRouting();
    void PrintFlowDelay(APPROACH iapproach);
    void PrintNodeSourceRouting(int nodeid, DIRECTIONTYPE direction);
    void PrintGraphRouting(map<int, map<int, vector<int> > > GraphRoute);
    void PrintSchedule(APPROACH iapproach, ROUTETYPE iroute);
    void PrintTransmissions(APPROACH iapproach);
    void PrintDelay(map<int, map<int, int> > mpDelay);
    void PrintFirstPacketDelay(map<int, map<int, vector<int> > > mpFirstDelay);
    void PrintWCPSSchedule(APPROACH iapproach);
    void PrintFlowSchedule(APPROACH iapproach, ROUTETYPE iroute);
    void PrintFlowTrace(APPROACH iapproach);
    void PlotResults();
	
	//Scheduling
	void AssignPriority();
	bool GenerateSchedule(bool testpassed, APPROACH iapproach, ROUTETYPE iroute);
    bool VerifySchedule(APPROACH iapproach);
    void Simulation(APPROACH iapporach, map<int, map<int, int > > &mpDelay, map<int, map<int, vector<int> > > &mpFirstDelay, int starttime, int endtime);  // [flowid][packetid]<WhetherSend, WhetherDelivery, Delay>
    void EnergySimulation();  // [flowid][packetid]<WhetherSend, WhetherDelivery, Delay>
    bool OneSlotTransmit(int flow, int timeslot, APPROACH iapproach);
    bool SuccessfulTransmission(int sender, int receiver);
    
	bool schedule(int flow, int job, int framelength, bool TestPassed, APPROACH iapproach, ROUTETYPE iroute);
	
    int SchedulePhase(int flow, int starttime, int framelength, map<int, map<int, vector<int> > > mpRoute, PHASETYPE iphase,  APPROACH iapproach);
    
	void ScheduleLink(int flow, int sender, int receiver, int slot, int channel, int framelength, bool isShared, ACCESSTYPE iaccess, PHASETYPE iphase, APPROACH iapproach, int routid, int HopCount);
	int FindFreeSlot(int flow, int starttime, int sender, int receiver, bool shared, int &channel, ACCESSTYPE iaccess, APPROACH iapproach);
	int PolynomialDelayBound(map<int, bool> UnassignedFlows, int iFlow, int &);  //returns polynomial delay of iFlow consider the hp flows are in 
	
    void RandomSimulator();
    void CreateFlowRouteSchedule(ROUTETYPE iroute);
    
    //void SchedulePhase(vector<APPROACH> vAPP, vector<bool> &vPass);
    //void AssignRMPriority(map<int, int> &f);
	
	
	
	
	
	//enumeration
	enum {DM, DM_SIM, DB, DB_SIM};
	
	//helper functions
	bool IsInVector(vector<int> v, int  value); 


	
public:
	ReliableScheduler(int numflows, int numruns);
	~ReliableScheduler();	
	
}; 


#endif
